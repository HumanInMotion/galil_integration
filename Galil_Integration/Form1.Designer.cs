﻿namespace Galil_Integration
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Main_Tab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LAnkle_Gbox = new System.Windows.Forms.GroupBox();
            this.LAnkle_Tbox = new System.Windows.Forms.TextBox();
            this.LAnkle_Stop_Button = new System.Windows.Forms.Button();
            this.LAnkle_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.LAnkle_Goto_Button = new System.Windows.Forms.Button();
            this.LAnkle_Neutral_Button = new System.Windows.Forms.Button();
            this.LAnkle_Init_Button = new System.Windows.Forms.Button();
            this.LHRear_Gbox = new System.Windows.Forms.GroupBox();
            this.LHRear_Tbox = new System.Windows.Forms.TextBox();
            this.LHRear_Stop_Button = new System.Windows.Forms.Button();
            this.LHRear_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.LHRear_Goto_Button = new System.Windows.Forms.Button();
            this.LHRear_Neutral_Button = new System.Windows.Forms.Button();
            this.LHRear_Init_Button = new System.Windows.Forms.Button();
            this.LHFront_Gbox = new System.Windows.Forms.GroupBox();
            this.LHFront_Tbox = new System.Windows.Forms.TextBox();
            this.LHFront_Stop_Button = new System.Windows.Forms.Button();
            this.LHFront_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.LHFront_Goto_Button = new System.Windows.Forms.Button();
            this.LHFront_Neutral_Button = new System.Windows.Forms.Button();
            this.LHFront_Init_Button = new System.Windows.Forms.Button();
            this.LHCenter_Gbox = new System.Windows.Forms.GroupBox();
            this.LHCenter_Tbox = new System.Windows.Forms.TextBox();
            this.LHCenter_Stop_Button = new System.Windows.Forms.Button();
            this.LHCenter_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.LHCenter_Goto_Button = new System.Windows.Forms.Button();
            this.LHCenter_Neutral_Button = new System.Windows.Forms.Button();
            this.LHCenter_Init_Button = new System.Windows.Forms.Button();
            this.LKnee_Gbox = new System.Windows.Forms.GroupBox();
            this.LKnee_Tbox = new System.Windows.Forms.TextBox();
            this.LKnee_Stop_Button = new System.Windows.Forms.Button();
            this.LKnee_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.LKnee_Goto_Button = new System.Windows.Forms.Button();
            this.LKnee_Neutral_Button = new System.Windows.Forms.Button();
            this.LKnee_Init_Button = new System.Windows.Forms.Button();
            this.RAnkle_Gbox = new System.Windows.Forms.GroupBox();
            this.RAnkle_Tbox = new System.Windows.Forms.TextBox();
            this.RAnkle_Stop_Button = new System.Windows.Forms.Button();
            this.RAnkle_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.RAnkle_Goto_Button = new System.Windows.Forms.Button();
            this.RAnkle_Neutral_Button = new System.Windows.Forms.Button();
            this.RAnkle_Init_Button = new System.Windows.Forms.Button();
            this.RHRear_Gbox = new System.Windows.Forms.GroupBox();
            this.RHRear_Tbox = new System.Windows.Forms.TextBox();
            this.RHRear_Stop_Button = new System.Windows.Forms.Button();
            this.RHRear_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.RHRear_Goto_Button = new System.Windows.Forms.Button();
            this.RHRear_Neutral_Button = new System.Windows.Forms.Button();
            this.RHRear_Init_Button = new System.Windows.Forms.Button();
            this.RHFront_Gbox = new System.Windows.Forms.GroupBox();
            this.RHFront_Tbox = new System.Windows.Forms.TextBox();
            this.RHFront_Stop_Button = new System.Windows.Forms.Button();
            this.RHFront_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.RHFront_Goto_Button = new System.Windows.Forms.Button();
            this.RHFront_Neutral_Button = new System.Windows.Forms.Button();
            this.RHFront_Init_Button = new System.Windows.Forms.Button();
            this.RHCenter_Gbox = new System.Windows.Forms.GroupBox();
            this.RHCenter_Tbox = new System.Windows.Forms.TextBox();
            this.RHCenter_Stop_Button = new System.Windows.Forms.Button();
            this.RHCenter_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.RHCenter_Goto_Button = new System.Windows.Forms.Button();
            this.RHCenter_Neutral_Button = new System.Windows.Forms.Button();
            this.RHCenter_Init_Button = new System.Windows.Forms.Button();
            this.Terminal_Gbox = new System.Windows.Forms.GroupBox();
            this.Terminal_Send_Button = new System.Windows.Forms.Button();
            this.Terminal_Device_CBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Terminal_Command_CBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Terminal_Tbox = new System.Windows.Forms.TextBox();
            this.RKnee_Gbox = new System.Windows.Forms.GroupBox();
            this.RKnee_Tbox = new System.Windows.Forms.TextBox();
            this.RKnee_Stop_Button = new System.Windows.Forms.Button();
            this.RKnee_Goto_Numeric = new System.Windows.Forms.NumericUpDown();
            this.RKnee_Goto_Button = new System.Windows.Forms.Button();
            this.RKnee_Neutral_Button = new System.Windows.Forms.Button();
            this.RKnee_Init_Button = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Disconnect_All_Button = new System.Windows.Forms.Button();
            this.Connect_All_Button = new System.Windows.Forms.Button();
            this.Disconnect_Button_4axis_Motor = new System.Windows.Forms.Button();
            this.Connection_Status_Label_4axis_Motor = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Connect_Button_4axis_Motor = new System.Windows.Forms.Button();
            this.Disconnect_Button_4axis_Biss = new System.Windows.Forms.Button();
            this.Connection_Status_Label_4axis_Biss = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Connect_Button_4axis_Biss = new System.Windows.Forms.Button();
            this.Disconnect_Button_8axis = new System.Windows.Forms.Button();
            this.Connection_Status_Label_8axis = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Connect_Button_8axis = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Motion_Progress_Label = new System.Windows.Forms.Label();
            this.Motion_Progress_Bar = new System.Windows.Forms.ProgressBar();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Loop_Button = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.STB_Tbox = new System.Windows.Forms.TextBox();
            this.STB_Button = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.STE_Tbox = new System.Windows.Forms.TextBox();
            this.STE_Button = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.PVT_Refresh_Button = new System.Windows.Forms.Button();
            this.PVT_Executed_Tbox_LA = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_LK = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_LHR = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_LHC = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_LHF = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_RA = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_RK = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_RHR = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_RHC = new System.Windows.Forms.TextBox();
            this.PVT_Executed_Tbox_RHF = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_LA = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_LK = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_LHR = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_LHC = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_LHF = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_RA = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_RK = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_RHR = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_RHC = new System.Windows.Forms.TextBox();
            this.PVT_Space_Tbox_RHF = new System.Windows.Forms.TextBox();
            this.Motion_ModeGbox = new System.Windows.Forms.GroupBox();
            this.NWK_Radiobutton = new System.Windows.Forms.RadioButton();
            this.GWK_Radiobutton = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Record_Enable_Checkbox = new System.Windows.Forms.CheckBox();
            this.PVT_Record_Gbox = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.PVT_Record_Rate_Numeric = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.Record_Velocity_Radiobutton = new System.Windows.Forms.RadioButton();
            this.Record_Torque_Radiobutton = new System.Windows.Forms.RadioButton();
            this.Record_Angle_Radiobutton = new System.Windows.Forms.RadioButton();
            this.PVT_Save_Button = new System.Windows.Forms.Button();
            this.PVT_Pause_Pbox = new System.Windows.Forms.PictureBox();
            this.PVT_Record_Pbox = new System.Windows.Forms.PictureBox();
            this.Record_Len_Tbox = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.PVT_Filepath_Tbox_Startposition = new System.Windows.Forms.TextBox();
            this.PVT_Browse_Button_Start_Position = new System.Windows.Forms.Button();
            this.PVT_Run_Button = new System.Windows.Forms.Button();
            this.PVT_Download_Button = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.PVT_Filepath_Tbox_4axis = new System.Windows.Forms.TextBox();
            this.PVT_Browse_Button_4axis = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.PVT_Filepath_Tbox_8axis = new System.Windows.Forms.TextBox();
            this.PVT_Browse_Button_8axis = new System.Windows.Forms.Button();
            this.Start_Position_Gbox = new System.Windows.Forms.GroupBox();
            this.Tick_LA_Pbox = new System.Windows.Forms.PictureBox();
            this.Tick_RK_Pbox = new System.Windows.Forms.PictureBox();
            this.Tick_RA_Pbox = new System.Windows.Forms.PictureBox();
            this.Tick_RHF_Pbox = new System.Windows.Forms.PictureBox();
            this.Tick_RHR_Pbox = new System.Windows.Forms.PictureBox();
            this.Warning_RK_Pbox = new System.Windows.Forms.PictureBox();
            this.Tick_RHC_Pbox = new System.Windows.Forms.PictureBox();
            this.Warning_RA_Pbox = new System.Windows.Forms.PictureBox();
            this.Warning_RHF_Pbox = new System.Windows.Forms.PictureBox();
            this.Tick_LHC_Pbox = new System.Windows.Forms.PictureBox();
            this.Tick_LK_Pbox = new System.Windows.Forms.PictureBox();
            this.Tick_LHR_Pbox = new System.Windows.Forms.PictureBox();
            this.ST_Check_Button = new System.Windows.Forms.Button();
            this.ST_Go_Button = new System.Windows.Forms.Button();
            this.Tick_LHF_Pbox = new System.Windows.Forms.PictureBox();
            this.ST_RA_Tbox = new System.Windows.Forms.TextBox();
            this.ST_RK_Tbox = new System.Windows.Forms.TextBox();
            this.ST_RHR_Tbox = new System.Windows.Forms.TextBox();
            this.Warning_LA_Pbox = new System.Windows.Forms.PictureBox();
            this.ST_RHC_Tbox = new System.Windows.Forms.TextBox();
            this.ST_RHF_Tbox = new System.Windows.Forms.TextBox();
            this.Warning_LK_Pbox = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Warning_LHR_Pbox = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Warning_LHC_Pbox = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ST_LA_Tbox = new System.Windows.Forms.TextBox();
            this.Warning_LHF_Pbox = new System.Windows.Forms.PictureBox();
            this.ST_LK_Tbox = new System.Windows.Forms.TextBox();
            this.ST_LHR_Tbox = new System.Windows.Forms.TextBox();
            this.ST_LHC_Tbox = new System.Windows.Forms.TextBox();
            this.ST_LHF_Tbox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Warning_RHC_Pbox = new System.Windows.Forms.PictureBox();
            this.Warning_RHR_Pbox = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.Stop_ALL_Button = new System.Windows.Forms.Button();
            this.Basetick = new System.Windows.Forms.Timer(this.components);
            this.Clear_Button = new System.Windows.Forms.Button();
            this.RHF_A_Tbox = new System.Windows.Forms.TextBox();
            this.RHC_A_Tbox = new System.Windows.Forms.TextBox();
            this.RHR_A_Tbox = new System.Windows.Forms.TextBox();
            this.RK_A_Tbox = new System.Windows.Forms.TextBox();
            this.RA_A_Tbox = new System.Windows.Forms.TextBox();
            this.LHF_A_Tbox = new System.Windows.Forms.TextBox();
            this.LHC_A_Tbox = new System.Windows.Forms.TextBox();
            this.LHR_A_Tbox = new System.Windows.Forms.TextBox();
            this.LK_A_Tbox = new System.Windows.Forms.TextBox();
            this.LA_A_Tbox = new System.Windows.Forms.TextBox();
            this.Refresh_Button = new System.Windows.Forms.Button();
            this.Reset_All_Button = new System.Windows.Forms.Button();
            this.LA_T_Tbox = new System.Windows.Forms.TextBox();
            this.LK_T_Tbox = new System.Windows.Forms.TextBox();
            this.LHR_T_Tbox = new System.Windows.Forms.TextBox();
            this.LHC_T_Tbox = new System.Windows.Forms.TextBox();
            this.LHF_T_Tbox = new System.Windows.Forms.TextBox();
            this.RA_T_Tbox = new System.Windows.Forms.TextBox();
            this.RK_T_Tbox = new System.Windows.Forms.TextBox();
            this.RHR_T_Tbox = new System.Windows.Forms.TextBox();
            this.RHC_T_Tbox = new System.Windows.Forms.TextBox();
            this.RHF_T_Tbox = new System.Windows.Forms.TextBox();
            this.Error_Gbox_8axis = new System.Windows.Forms.GroupBox();
            this.Error_ELO_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_Peakcurrent_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_Hall_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_UndervoltageEH_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_OvertemperetureEH_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_OvervoltageEH_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_OvercurrentEH_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_UndervoltageAD_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_OvertemperetureAD_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_OvervoltageAD_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_OvercurrentAD_Checkbox_8axis = new System.Windows.Forms.CheckBox();
            this.Error_Gbox_4axis = new System.Windows.Forms.GroupBox();
            this.Error_ELO_Checkbox_4axis = new System.Windows.Forms.CheckBox();
            this.Error_Peakcurrent_Checkbox_4axis = new System.Windows.Forms.CheckBox();
            this.Error_Hall_Checkbox_4axis = new System.Windows.Forms.CheckBox();
            this.Error_UndervoltageAD_Checkbox_4axis = new System.Windows.Forms.CheckBox();
            this.Error_OvertemperetureAD_Checkbox_4axis = new System.Windows.Forms.CheckBox();
            this.Error_OvervoltageAD_Checkbox_4axis = new System.Windows.Forms.CheckBox();
            this.Error_OvercurrentAD_Checkbox_4axis = new System.Windows.Forms.CheckBox();
            this.Main_Tab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.LAnkle_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LAnkle_Goto_Numeric)).BeginInit();
            this.LHRear_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LHRear_Goto_Numeric)).BeginInit();
            this.LHFront_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LHFront_Goto_Numeric)).BeginInit();
            this.LHCenter_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LHCenter_Goto_Numeric)).BeginInit();
            this.LKnee_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LKnee_Goto_Numeric)).BeginInit();
            this.RAnkle_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RAnkle_Goto_Numeric)).BeginInit();
            this.RHRear_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RHRear_Goto_Numeric)).BeginInit();
            this.RHFront_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RHFront_Goto_Numeric)).BeginInit();
            this.RHCenter_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RHCenter_Goto_Numeric)).BeginInit();
            this.Terminal_Gbox.SuspendLayout();
            this.RKnee_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RKnee_Goto_Numeric)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.Motion_ModeGbox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.PVT_Record_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PVT_Record_Rate_Numeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PVT_Pause_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PVT_Record_Pbox)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Start_Position_Gbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LA_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RK_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RA_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RHF_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RHR_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RK_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RHC_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RA_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RHF_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LHC_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LK_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LHR_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LHF_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LA_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LK_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LHR_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LHC_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LHF_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RHC_Pbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RHR_Pbox)).BeginInit();
            this.Error_Gbox_8axis.SuspendLayout();
            this.Error_Gbox_4axis.SuspendLayout();
            this.SuspendLayout();
            // 
            // Main_Tab
            // 
            this.Main_Tab.Controls.Add(this.tabPage1);
            this.Main_Tab.Controls.Add(this.tabPage2);
            this.Main_Tab.Controls.Add(this.tabPage3);
            this.Main_Tab.Location = new System.Drawing.Point(2, 0);
            this.Main_Tab.Name = "Main_Tab";
            this.Main_Tab.SelectedIndex = 0;
            this.Main_Tab.Size = new System.Drawing.Size(1033, 758);
            this.Main_Tab.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LAnkle_Gbox);
            this.tabPage1.Controls.Add(this.LHRear_Gbox);
            this.tabPage1.Controls.Add(this.LHFront_Gbox);
            this.tabPage1.Controls.Add(this.LHCenter_Gbox);
            this.tabPage1.Controls.Add(this.LKnee_Gbox);
            this.tabPage1.Controls.Add(this.RAnkle_Gbox);
            this.tabPage1.Controls.Add(this.RHRear_Gbox);
            this.tabPage1.Controls.Add(this.RHFront_Gbox);
            this.tabPage1.Controls.Add(this.RHCenter_Gbox);
            this.tabPage1.Controls.Add(this.Terminal_Gbox);
            this.tabPage1.Controls.Add(this.RKnee_Gbox);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1025, 732);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Init";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LAnkle_Gbox
            // 
            this.LAnkle_Gbox.Controls.Add(this.LAnkle_Tbox);
            this.LAnkle_Gbox.Controls.Add(this.LAnkle_Stop_Button);
            this.LAnkle_Gbox.Controls.Add(this.LAnkle_Goto_Numeric);
            this.LAnkle_Gbox.Controls.Add(this.LAnkle_Goto_Button);
            this.LAnkle_Gbox.Controls.Add(this.LAnkle_Neutral_Button);
            this.LAnkle_Gbox.Controls.Add(this.LAnkle_Init_Button);
            this.LAnkle_Gbox.Location = new System.Drawing.Point(431, 590);
            this.LAnkle_Gbox.Name = "LAnkle_Gbox";
            this.LAnkle_Gbox.Size = new System.Drawing.Size(265, 114);
            this.LAnkle_Gbox.TabIndex = 12;
            this.LAnkle_Gbox.TabStop = false;
            this.LAnkle_Gbox.Text = "Left Ankle";
            // 
            // LAnkle_Tbox
            // 
            this.LAnkle_Tbox.Location = new System.Drawing.Point(6, 19);
            this.LAnkle_Tbox.Name = "LAnkle_Tbox";
            this.LAnkle_Tbox.ReadOnly = true;
            this.LAnkle_Tbox.Size = new System.Drawing.Size(242, 20);
            this.LAnkle_Tbox.TabIndex = 5;
            // 
            // LAnkle_Stop_Button
            // 
            this.LAnkle_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.LAnkle_Stop_Button.Name = "LAnkle_Stop_Button";
            this.LAnkle_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.LAnkle_Stop_Button.TabIndex = 4;
            this.LAnkle_Stop_Button.Text = "Stop";
            this.LAnkle_Stop_Button.UseVisualStyleBackColor = true;
            this.LAnkle_Stop_Button.Click += new System.EventHandler(this.LAnkle_Stop_Button_Click);
            // 
            // LAnkle_Goto_Numeric
            // 
            this.LAnkle_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.LAnkle_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.LAnkle_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.LAnkle_Goto_Numeric.Name = "LAnkle_Goto_Numeric";
            this.LAnkle_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.LAnkle_Goto_Numeric.TabIndex = 3;
            // 
            // LAnkle_Goto_Button
            // 
            this.LAnkle_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.LAnkle_Goto_Button.Name = "LAnkle_Goto_Button";
            this.LAnkle_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.LAnkle_Goto_Button.TabIndex = 2;
            this.LAnkle_Goto_Button.Text = "Goto";
            this.LAnkle_Goto_Button.UseVisualStyleBackColor = true;
            this.LAnkle_Goto_Button.Click += new System.EventHandler(this.LAnkle_Goto_Button_Click);
            // 
            // LAnkle_Neutral_Button
            // 
            this.LAnkle_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.LAnkle_Neutral_Button.Name = "LAnkle_Neutral_Button";
            this.LAnkle_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.LAnkle_Neutral_Button.TabIndex = 1;
            this.LAnkle_Neutral_Button.Text = "HomeP";
            this.LAnkle_Neutral_Button.UseVisualStyleBackColor = true;
            this.LAnkle_Neutral_Button.Click += new System.EventHandler(this.LAnkle_Neutral_Button_Click);
            // 
            // LAnkle_Init_Button
            // 
            this.LAnkle_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.LAnkle_Init_Button.Name = "LAnkle_Init_Button";
            this.LAnkle_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.LAnkle_Init_Button.TabIndex = 0;
            this.LAnkle_Init_Button.Text = "Init";
            this.LAnkle_Init_Button.UseVisualStyleBackColor = true;
            this.LAnkle_Init_Button.Click += new System.EventHandler(this.LAnkle_Init_Button_Click);
            // 
            // LHRear_Gbox
            // 
            this.LHRear_Gbox.Controls.Add(this.LHRear_Tbox);
            this.LHRear_Gbox.Controls.Add(this.LHRear_Stop_Button);
            this.LHRear_Gbox.Controls.Add(this.LHRear_Goto_Numeric);
            this.LHRear_Gbox.Controls.Add(this.LHRear_Goto_Button);
            this.LHRear_Gbox.Controls.Add(this.LHRear_Neutral_Button);
            this.LHRear_Gbox.Controls.Add(this.LHRear_Init_Button);
            this.LHRear_Gbox.Location = new System.Drawing.Point(431, 249);
            this.LHRear_Gbox.Name = "LHRear_Gbox";
            this.LHRear_Gbox.Size = new System.Drawing.Size(265, 114);
            this.LHRear_Gbox.TabIndex = 11;
            this.LHRear_Gbox.TabStop = false;
            this.LHRear_Gbox.Text = "Left Hip Rear";
            // 
            // LHRear_Tbox
            // 
            this.LHRear_Tbox.Location = new System.Drawing.Point(6, 19);
            this.LHRear_Tbox.Name = "LHRear_Tbox";
            this.LHRear_Tbox.ReadOnly = true;
            this.LHRear_Tbox.Size = new System.Drawing.Size(242, 20);
            this.LHRear_Tbox.TabIndex = 5;
            // 
            // LHRear_Stop_Button
            // 
            this.LHRear_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.LHRear_Stop_Button.Name = "LHRear_Stop_Button";
            this.LHRear_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.LHRear_Stop_Button.TabIndex = 4;
            this.LHRear_Stop_Button.Text = "Stop";
            this.LHRear_Stop_Button.UseVisualStyleBackColor = true;
            this.LHRear_Stop_Button.Click += new System.EventHandler(this.LHRear_Stop_Button_Click);
            // 
            // LHRear_Goto_Numeric
            // 
            this.LHRear_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.LHRear_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.LHRear_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.LHRear_Goto_Numeric.Name = "LHRear_Goto_Numeric";
            this.LHRear_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.LHRear_Goto_Numeric.TabIndex = 3;
            // 
            // LHRear_Goto_Button
            // 
            this.LHRear_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.LHRear_Goto_Button.Name = "LHRear_Goto_Button";
            this.LHRear_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.LHRear_Goto_Button.TabIndex = 2;
            this.LHRear_Goto_Button.Text = "Goto";
            this.LHRear_Goto_Button.UseVisualStyleBackColor = true;
            this.LHRear_Goto_Button.Click += new System.EventHandler(this.LHRear_Goto_Button_Click);
            // 
            // LHRear_Neutral_Button
            // 
            this.LHRear_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.LHRear_Neutral_Button.Name = "LHRear_Neutral_Button";
            this.LHRear_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.LHRear_Neutral_Button.TabIndex = 1;
            this.LHRear_Neutral_Button.Text = "HomeP";
            this.LHRear_Neutral_Button.UseVisualStyleBackColor = true;
            this.LHRear_Neutral_Button.Click += new System.EventHandler(this.LHRear_Neutral_Button_Click);
            // 
            // LHRear_Init_Button
            // 
            this.LHRear_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.LHRear_Init_Button.Name = "LHRear_Init_Button";
            this.LHRear_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.LHRear_Init_Button.TabIndex = 0;
            this.LHRear_Init_Button.Text = "Init";
            this.LHRear_Init_Button.UseVisualStyleBackColor = true;
            this.LHRear_Init_Button.Click += new System.EventHandler(this.LHRear_Init_Button_Click);
            // 
            // LHFront_Gbox
            // 
            this.LHFront_Gbox.Controls.Add(this.LHFront_Tbox);
            this.LHFront_Gbox.Controls.Add(this.LHFront_Stop_Button);
            this.LHFront_Gbox.Controls.Add(this.LHFront_Goto_Numeric);
            this.LHFront_Gbox.Controls.Add(this.LHFront_Goto_Button);
            this.LHFront_Gbox.Controls.Add(this.LHFront_Neutral_Button);
            this.LHFront_Gbox.Controls.Add(this.LHFront_Init_Button);
            this.LHFront_Gbox.Location = new System.Drawing.Point(431, 11);
            this.LHFront_Gbox.Name = "LHFront_Gbox";
            this.LHFront_Gbox.Size = new System.Drawing.Size(265, 114);
            this.LHFront_Gbox.TabIndex = 10;
            this.LHFront_Gbox.TabStop = false;
            this.LHFront_Gbox.Text = "Left Hip Front";
            // 
            // LHFront_Tbox
            // 
            this.LHFront_Tbox.Location = new System.Drawing.Point(6, 19);
            this.LHFront_Tbox.Name = "LHFront_Tbox";
            this.LHFront_Tbox.ReadOnly = true;
            this.LHFront_Tbox.Size = new System.Drawing.Size(242, 20);
            this.LHFront_Tbox.TabIndex = 5;
            // 
            // LHFront_Stop_Button
            // 
            this.LHFront_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.LHFront_Stop_Button.Name = "LHFront_Stop_Button";
            this.LHFront_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.LHFront_Stop_Button.TabIndex = 4;
            this.LHFront_Stop_Button.Text = "Stop";
            this.LHFront_Stop_Button.UseVisualStyleBackColor = true;
            this.LHFront_Stop_Button.Click += new System.EventHandler(this.LHFront_Stop_Button_Click);
            // 
            // LHFront_Goto_Numeric
            // 
            this.LHFront_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.LHFront_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.LHFront_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.LHFront_Goto_Numeric.Name = "LHFront_Goto_Numeric";
            this.LHFront_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.LHFront_Goto_Numeric.TabIndex = 3;
            // 
            // LHFront_Goto_Button
            // 
            this.LHFront_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.LHFront_Goto_Button.Name = "LHFront_Goto_Button";
            this.LHFront_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.LHFront_Goto_Button.TabIndex = 2;
            this.LHFront_Goto_Button.Text = "Goto";
            this.LHFront_Goto_Button.UseVisualStyleBackColor = true;
            this.LHFront_Goto_Button.Click += new System.EventHandler(this.LHFront_Goto_Button_Click);
            // 
            // LHFront_Neutral_Button
            // 
            this.LHFront_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.LHFront_Neutral_Button.Name = "LHFront_Neutral_Button";
            this.LHFront_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.LHFront_Neutral_Button.TabIndex = 1;
            this.LHFront_Neutral_Button.Text = "HomeP";
            this.LHFront_Neutral_Button.UseVisualStyleBackColor = true;
            this.LHFront_Neutral_Button.Click += new System.EventHandler(this.LHFront_Neutral_Button_Click);
            // 
            // LHFront_Init_Button
            // 
            this.LHFront_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.LHFront_Init_Button.Name = "LHFront_Init_Button";
            this.LHFront_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.LHFront_Init_Button.TabIndex = 0;
            this.LHFront_Init_Button.Text = "Init";
            this.LHFront_Init_Button.UseVisualStyleBackColor = true;
            this.LHFront_Init_Button.Click += new System.EventHandler(this.LHFront_Init_Button_Click);
            // 
            // LHCenter_Gbox
            // 
            this.LHCenter_Gbox.Controls.Add(this.LHCenter_Tbox);
            this.LHCenter_Gbox.Controls.Add(this.LHCenter_Stop_Button);
            this.LHCenter_Gbox.Controls.Add(this.LHCenter_Goto_Numeric);
            this.LHCenter_Gbox.Controls.Add(this.LHCenter_Goto_Button);
            this.LHCenter_Gbox.Controls.Add(this.LHCenter_Neutral_Button);
            this.LHCenter_Gbox.Controls.Add(this.LHCenter_Init_Button);
            this.LHCenter_Gbox.Location = new System.Drawing.Point(431, 130);
            this.LHCenter_Gbox.Name = "LHCenter_Gbox";
            this.LHCenter_Gbox.Size = new System.Drawing.Size(265, 114);
            this.LHCenter_Gbox.TabIndex = 9;
            this.LHCenter_Gbox.TabStop = false;
            this.LHCenter_Gbox.Text = "Left Hip Center";
            // 
            // LHCenter_Tbox
            // 
            this.LHCenter_Tbox.Location = new System.Drawing.Point(6, 19);
            this.LHCenter_Tbox.Name = "LHCenter_Tbox";
            this.LHCenter_Tbox.ReadOnly = true;
            this.LHCenter_Tbox.Size = new System.Drawing.Size(242, 20);
            this.LHCenter_Tbox.TabIndex = 5;
            // 
            // LHCenter_Stop_Button
            // 
            this.LHCenter_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.LHCenter_Stop_Button.Name = "LHCenter_Stop_Button";
            this.LHCenter_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.LHCenter_Stop_Button.TabIndex = 4;
            this.LHCenter_Stop_Button.Text = "Stop";
            this.LHCenter_Stop_Button.UseVisualStyleBackColor = true;
            this.LHCenter_Stop_Button.Click += new System.EventHandler(this.LHCenter_Stop_Button_Click);
            // 
            // LHCenter_Goto_Numeric
            // 
            this.LHCenter_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.LHCenter_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.LHCenter_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.LHCenter_Goto_Numeric.Name = "LHCenter_Goto_Numeric";
            this.LHCenter_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.LHCenter_Goto_Numeric.TabIndex = 3;
            // 
            // LHCenter_Goto_Button
            // 
            this.LHCenter_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.LHCenter_Goto_Button.Name = "LHCenter_Goto_Button";
            this.LHCenter_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.LHCenter_Goto_Button.TabIndex = 2;
            this.LHCenter_Goto_Button.Text = "Goto";
            this.LHCenter_Goto_Button.UseVisualStyleBackColor = true;
            this.LHCenter_Goto_Button.Click += new System.EventHandler(this.LHCenter_Goto_Button_Click);
            // 
            // LHCenter_Neutral_Button
            // 
            this.LHCenter_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.LHCenter_Neutral_Button.Name = "LHCenter_Neutral_Button";
            this.LHCenter_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.LHCenter_Neutral_Button.TabIndex = 1;
            this.LHCenter_Neutral_Button.Text = "HomeP";
            this.LHCenter_Neutral_Button.UseVisualStyleBackColor = true;
            this.LHCenter_Neutral_Button.Click += new System.EventHandler(this.LHCenter_Neutral_Button_Click);
            // 
            // LHCenter_Init_Button
            // 
            this.LHCenter_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.LHCenter_Init_Button.Name = "LHCenter_Init_Button";
            this.LHCenter_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.LHCenter_Init_Button.TabIndex = 0;
            this.LHCenter_Init_Button.Text = "Init";
            this.LHCenter_Init_Button.UseVisualStyleBackColor = true;
            this.LHCenter_Init_Button.Click += new System.EventHandler(this.LHCenter_Init_Button_Click);
            // 
            // LKnee_Gbox
            // 
            this.LKnee_Gbox.Controls.Add(this.LKnee_Tbox);
            this.LKnee_Gbox.Controls.Add(this.LKnee_Stop_Button);
            this.LKnee_Gbox.Controls.Add(this.LKnee_Goto_Numeric);
            this.LKnee_Gbox.Controls.Add(this.LKnee_Goto_Button);
            this.LKnee_Gbox.Controls.Add(this.LKnee_Neutral_Button);
            this.LKnee_Gbox.Controls.Add(this.LKnee_Init_Button);
            this.LKnee_Gbox.Location = new System.Drawing.Point(431, 452);
            this.LKnee_Gbox.Name = "LKnee_Gbox";
            this.LKnee_Gbox.Size = new System.Drawing.Size(265, 114);
            this.LKnee_Gbox.TabIndex = 8;
            this.LKnee_Gbox.TabStop = false;
            this.LKnee_Gbox.Text = "Left Knee";
            // 
            // LKnee_Tbox
            // 
            this.LKnee_Tbox.Location = new System.Drawing.Point(6, 19);
            this.LKnee_Tbox.Name = "LKnee_Tbox";
            this.LKnee_Tbox.ReadOnly = true;
            this.LKnee_Tbox.Size = new System.Drawing.Size(242, 20);
            this.LKnee_Tbox.TabIndex = 5;
            // 
            // LKnee_Stop_Button
            // 
            this.LKnee_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.LKnee_Stop_Button.Name = "LKnee_Stop_Button";
            this.LKnee_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.LKnee_Stop_Button.TabIndex = 4;
            this.LKnee_Stop_Button.Text = "Stop";
            this.LKnee_Stop_Button.UseVisualStyleBackColor = true;
            this.LKnee_Stop_Button.Click += new System.EventHandler(this.LKnee_Stop_Button_Click);
            // 
            // LKnee_Goto_Numeric
            // 
            this.LKnee_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.LKnee_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.LKnee_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.LKnee_Goto_Numeric.Name = "LKnee_Goto_Numeric";
            this.LKnee_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.LKnee_Goto_Numeric.TabIndex = 3;
            // 
            // LKnee_Goto_Button
            // 
            this.LKnee_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.LKnee_Goto_Button.Name = "LKnee_Goto_Button";
            this.LKnee_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.LKnee_Goto_Button.TabIndex = 2;
            this.LKnee_Goto_Button.Text = "Goto";
            this.LKnee_Goto_Button.UseVisualStyleBackColor = true;
            this.LKnee_Goto_Button.Click += new System.EventHandler(this.LKnee_Goto_Button_Click);
            // 
            // LKnee_Neutral_Button
            // 
            this.LKnee_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.LKnee_Neutral_Button.Name = "LKnee_Neutral_Button";
            this.LKnee_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.LKnee_Neutral_Button.TabIndex = 1;
            this.LKnee_Neutral_Button.Text = "HomeP";
            this.LKnee_Neutral_Button.UseVisualStyleBackColor = true;
            this.LKnee_Neutral_Button.Click += new System.EventHandler(this.LKnee_Neutral_Button_Click);
            // 
            // LKnee_Init_Button
            // 
            this.LKnee_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.LKnee_Init_Button.Name = "LKnee_Init_Button";
            this.LKnee_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.LKnee_Init_Button.TabIndex = 0;
            this.LKnee_Init_Button.Text = "Init";
            this.LKnee_Init_Button.UseVisualStyleBackColor = true;
            this.LKnee_Init_Button.Click += new System.EventHandler(this.LKnee_Init_Button_Click);
            // 
            // RAnkle_Gbox
            // 
            this.RAnkle_Gbox.Controls.Add(this.RAnkle_Tbox);
            this.RAnkle_Gbox.Controls.Add(this.RAnkle_Stop_Button);
            this.RAnkle_Gbox.Controls.Add(this.RAnkle_Goto_Numeric);
            this.RAnkle_Gbox.Controls.Add(this.RAnkle_Goto_Button);
            this.RAnkle_Gbox.Controls.Add(this.RAnkle_Neutral_Button);
            this.RAnkle_Gbox.Controls.Add(this.RAnkle_Init_Button);
            this.RAnkle_Gbox.Location = new System.Drawing.Point(754, 590);
            this.RAnkle_Gbox.Name = "RAnkle_Gbox";
            this.RAnkle_Gbox.Size = new System.Drawing.Size(265, 114);
            this.RAnkle_Gbox.TabIndex = 7;
            this.RAnkle_Gbox.TabStop = false;
            this.RAnkle_Gbox.Text = "Right Ankle";
            // 
            // RAnkle_Tbox
            // 
            this.RAnkle_Tbox.Location = new System.Drawing.Point(6, 19);
            this.RAnkle_Tbox.Name = "RAnkle_Tbox";
            this.RAnkle_Tbox.ReadOnly = true;
            this.RAnkle_Tbox.Size = new System.Drawing.Size(242, 20);
            this.RAnkle_Tbox.TabIndex = 5;
            // 
            // RAnkle_Stop_Button
            // 
            this.RAnkle_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.RAnkle_Stop_Button.Name = "RAnkle_Stop_Button";
            this.RAnkle_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.RAnkle_Stop_Button.TabIndex = 4;
            this.RAnkle_Stop_Button.Text = "Stop";
            this.RAnkle_Stop_Button.UseVisualStyleBackColor = true;
            this.RAnkle_Stop_Button.Click += new System.EventHandler(this.RAnkle_Stop_Button_Click);
            // 
            // RAnkle_Goto_Numeric
            // 
            this.RAnkle_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.RAnkle_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.RAnkle_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.RAnkle_Goto_Numeric.Name = "RAnkle_Goto_Numeric";
            this.RAnkle_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.RAnkle_Goto_Numeric.TabIndex = 3;
            // 
            // RAnkle_Goto_Button
            // 
            this.RAnkle_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.RAnkle_Goto_Button.Name = "RAnkle_Goto_Button";
            this.RAnkle_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.RAnkle_Goto_Button.TabIndex = 2;
            this.RAnkle_Goto_Button.Text = "Goto";
            this.RAnkle_Goto_Button.UseVisualStyleBackColor = true;
            this.RAnkle_Goto_Button.Click += new System.EventHandler(this.RAnkle_Goto_Button_Click);
            // 
            // RAnkle_Neutral_Button
            // 
            this.RAnkle_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.RAnkle_Neutral_Button.Name = "RAnkle_Neutral_Button";
            this.RAnkle_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.RAnkle_Neutral_Button.TabIndex = 1;
            this.RAnkle_Neutral_Button.Text = "HomeP";
            this.RAnkle_Neutral_Button.UseVisualStyleBackColor = true;
            this.RAnkle_Neutral_Button.Click += new System.EventHandler(this.RAnkle_Neutral_Button_Click);
            // 
            // RAnkle_Init_Button
            // 
            this.RAnkle_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.RAnkle_Init_Button.Name = "RAnkle_Init_Button";
            this.RAnkle_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.RAnkle_Init_Button.TabIndex = 0;
            this.RAnkle_Init_Button.Text = "Init";
            this.RAnkle_Init_Button.UseVisualStyleBackColor = true;
            this.RAnkle_Init_Button.Click += new System.EventHandler(this.RAnkle_Init_Button_Click);
            // 
            // RHRear_Gbox
            // 
            this.RHRear_Gbox.Controls.Add(this.RHRear_Tbox);
            this.RHRear_Gbox.Controls.Add(this.RHRear_Stop_Button);
            this.RHRear_Gbox.Controls.Add(this.RHRear_Goto_Numeric);
            this.RHRear_Gbox.Controls.Add(this.RHRear_Goto_Button);
            this.RHRear_Gbox.Controls.Add(this.RHRear_Neutral_Button);
            this.RHRear_Gbox.Controls.Add(this.RHRear_Init_Button);
            this.RHRear_Gbox.Location = new System.Drawing.Point(754, 249);
            this.RHRear_Gbox.Name = "RHRear_Gbox";
            this.RHRear_Gbox.Size = new System.Drawing.Size(265, 114);
            this.RHRear_Gbox.TabIndex = 6;
            this.RHRear_Gbox.TabStop = false;
            this.RHRear_Gbox.Text = "Right Hip Rear";
            // 
            // RHRear_Tbox
            // 
            this.RHRear_Tbox.Location = new System.Drawing.Point(6, 19);
            this.RHRear_Tbox.Name = "RHRear_Tbox";
            this.RHRear_Tbox.ReadOnly = true;
            this.RHRear_Tbox.Size = new System.Drawing.Size(242, 20);
            this.RHRear_Tbox.TabIndex = 5;
            // 
            // RHRear_Stop_Button
            // 
            this.RHRear_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.RHRear_Stop_Button.Name = "RHRear_Stop_Button";
            this.RHRear_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.RHRear_Stop_Button.TabIndex = 4;
            this.RHRear_Stop_Button.Text = "Stop";
            this.RHRear_Stop_Button.UseVisualStyleBackColor = true;
            this.RHRear_Stop_Button.Click += new System.EventHandler(this.RHRear_Stop_Button_Click);
            // 
            // RHRear_Goto_Numeric
            // 
            this.RHRear_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.RHRear_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.RHRear_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.RHRear_Goto_Numeric.Name = "RHRear_Goto_Numeric";
            this.RHRear_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.RHRear_Goto_Numeric.TabIndex = 3;
            // 
            // RHRear_Goto_Button
            // 
            this.RHRear_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.RHRear_Goto_Button.Name = "RHRear_Goto_Button";
            this.RHRear_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.RHRear_Goto_Button.TabIndex = 2;
            this.RHRear_Goto_Button.Text = "Goto";
            this.RHRear_Goto_Button.UseVisualStyleBackColor = true;
            this.RHRear_Goto_Button.Click += new System.EventHandler(this.RHRear_Goto_Button_Click);
            // 
            // RHRear_Neutral_Button
            // 
            this.RHRear_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.RHRear_Neutral_Button.Name = "RHRear_Neutral_Button";
            this.RHRear_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.RHRear_Neutral_Button.TabIndex = 1;
            this.RHRear_Neutral_Button.Text = "HomeP";
            this.RHRear_Neutral_Button.UseVisualStyleBackColor = true;
            this.RHRear_Neutral_Button.Click += new System.EventHandler(this.RHRear_Neutral_Button_Click);
            // 
            // RHRear_Init_Button
            // 
            this.RHRear_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.RHRear_Init_Button.Name = "RHRear_Init_Button";
            this.RHRear_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.RHRear_Init_Button.TabIndex = 0;
            this.RHRear_Init_Button.Text = "Init";
            this.RHRear_Init_Button.UseVisualStyleBackColor = true;
            this.RHRear_Init_Button.Click += new System.EventHandler(this.RHRear_Init_Button_Click);
            // 
            // RHFront_Gbox
            // 
            this.RHFront_Gbox.Controls.Add(this.RHFront_Tbox);
            this.RHFront_Gbox.Controls.Add(this.RHFront_Stop_Button);
            this.RHFront_Gbox.Controls.Add(this.RHFront_Goto_Numeric);
            this.RHFront_Gbox.Controls.Add(this.RHFront_Goto_Button);
            this.RHFront_Gbox.Controls.Add(this.RHFront_Neutral_Button);
            this.RHFront_Gbox.Controls.Add(this.RHFront_Init_Button);
            this.RHFront_Gbox.Location = new System.Drawing.Point(754, 11);
            this.RHFront_Gbox.Name = "RHFront_Gbox";
            this.RHFront_Gbox.Size = new System.Drawing.Size(265, 114);
            this.RHFront_Gbox.TabIndex = 5;
            this.RHFront_Gbox.TabStop = false;
            this.RHFront_Gbox.Text = "Right Hip Front";
            // 
            // RHFront_Tbox
            // 
            this.RHFront_Tbox.Location = new System.Drawing.Point(6, 19);
            this.RHFront_Tbox.Name = "RHFront_Tbox";
            this.RHFront_Tbox.ReadOnly = true;
            this.RHFront_Tbox.Size = new System.Drawing.Size(242, 20);
            this.RHFront_Tbox.TabIndex = 5;
            // 
            // RHFront_Stop_Button
            // 
            this.RHFront_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.RHFront_Stop_Button.Name = "RHFront_Stop_Button";
            this.RHFront_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.RHFront_Stop_Button.TabIndex = 4;
            this.RHFront_Stop_Button.Text = "Stop";
            this.RHFront_Stop_Button.UseVisualStyleBackColor = true;
            this.RHFront_Stop_Button.Click += new System.EventHandler(this.RHFront_Stop_Button_Click);
            // 
            // RHFront_Goto_Numeric
            // 
            this.RHFront_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.RHFront_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.RHFront_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.RHFront_Goto_Numeric.Name = "RHFront_Goto_Numeric";
            this.RHFront_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.RHFront_Goto_Numeric.TabIndex = 3;
            // 
            // RHFront_Goto_Button
            // 
            this.RHFront_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.RHFront_Goto_Button.Name = "RHFront_Goto_Button";
            this.RHFront_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.RHFront_Goto_Button.TabIndex = 2;
            this.RHFront_Goto_Button.Text = "Goto";
            this.RHFront_Goto_Button.UseVisualStyleBackColor = true;
            this.RHFront_Goto_Button.Click += new System.EventHandler(this.RHFront_Goto_Button_Click);
            // 
            // RHFront_Neutral_Button
            // 
            this.RHFront_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.RHFront_Neutral_Button.Name = "RHFront_Neutral_Button";
            this.RHFront_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.RHFront_Neutral_Button.TabIndex = 1;
            this.RHFront_Neutral_Button.Text = "HomeP";
            this.RHFront_Neutral_Button.UseVisualStyleBackColor = true;
            this.RHFront_Neutral_Button.Click += new System.EventHandler(this.RHFront_Neutral_Button_Click);
            // 
            // RHFront_Init_Button
            // 
            this.RHFront_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.RHFront_Init_Button.Name = "RHFront_Init_Button";
            this.RHFront_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.RHFront_Init_Button.TabIndex = 0;
            this.RHFront_Init_Button.Text = "Init";
            this.RHFront_Init_Button.UseVisualStyleBackColor = true;
            this.RHFront_Init_Button.Click += new System.EventHandler(this.RHFront_Init_Button_Click);
            // 
            // RHCenter_Gbox
            // 
            this.RHCenter_Gbox.Controls.Add(this.RHCenter_Tbox);
            this.RHCenter_Gbox.Controls.Add(this.RHCenter_Stop_Button);
            this.RHCenter_Gbox.Controls.Add(this.RHCenter_Goto_Numeric);
            this.RHCenter_Gbox.Controls.Add(this.RHCenter_Goto_Button);
            this.RHCenter_Gbox.Controls.Add(this.RHCenter_Neutral_Button);
            this.RHCenter_Gbox.Controls.Add(this.RHCenter_Init_Button);
            this.RHCenter_Gbox.Location = new System.Drawing.Point(754, 130);
            this.RHCenter_Gbox.Name = "RHCenter_Gbox";
            this.RHCenter_Gbox.Size = new System.Drawing.Size(265, 114);
            this.RHCenter_Gbox.TabIndex = 4;
            this.RHCenter_Gbox.TabStop = false;
            this.RHCenter_Gbox.Text = "Right Hip Center";
            // 
            // RHCenter_Tbox
            // 
            this.RHCenter_Tbox.Location = new System.Drawing.Point(6, 19);
            this.RHCenter_Tbox.Name = "RHCenter_Tbox";
            this.RHCenter_Tbox.ReadOnly = true;
            this.RHCenter_Tbox.Size = new System.Drawing.Size(242, 20);
            this.RHCenter_Tbox.TabIndex = 5;
            // 
            // RHCenter_Stop_Button
            // 
            this.RHCenter_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.RHCenter_Stop_Button.Name = "RHCenter_Stop_Button";
            this.RHCenter_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.RHCenter_Stop_Button.TabIndex = 4;
            this.RHCenter_Stop_Button.Text = "Stop";
            this.RHCenter_Stop_Button.UseVisualStyleBackColor = true;
            this.RHCenter_Stop_Button.Click += new System.EventHandler(this.RHCenter_Stop_Button_Click);
            // 
            // RHCenter_Goto_Numeric
            // 
            this.RHCenter_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.RHCenter_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.RHCenter_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.RHCenter_Goto_Numeric.Name = "RHCenter_Goto_Numeric";
            this.RHCenter_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.RHCenter_Goto_Numeric.TabIndex = 3;
            // 
            // RHCenter_Goto_Button
            // 
            this.RHCenter_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.RHCenter_Goto_Button.Name = "RHCenter_Goto_Button";
            this.RHCenter_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.RHCenter_Goto_Button.TabIndex = 2;
            this.RHCenter_Goto_Button.Text = "Goto";
            this.RHCenter_Goto_Button.UseVisualStyleBackColor = true;
            this.RHCenter_Goto_Button.Click += new System.EventHandler(this.RHCenter_Goto_Button_Click);
            // 
            // RHCenter_Neutral_Button
            // 
            this.RHCenter_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.RHCenter_Neutral_Button.Name = "RHCenter_Neutral_Button";
            this.RHCenter_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.RHCenter_Neutral_Button.TabIndex = 1;
            this.RHCenter_Neutral_Button.Text = "HomeP";
            this.RHCenter_Neutral_Button.UseVisualStyleBackColor = true;
            this.RHCenter_Neutral_Button.Click += new System.EventHandler(this.RHCenter_Neutral_Button_Click);
            // 
            // RHCenter_Init_Button
            // 
            this.RHCenter_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.RHCenter_Init_Button.Name = "RHCenter_Init_Button";
            this.RHCenter_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.RHCenter_Init_Button.TabIndex = 0;
            this.RHCenter_Init_Button.Text = "Init";
            this.RHCenter_Init_Button.UseVisualStyleBackColor = true;
            this.RHCenter_Init_Button.Click += new System.EventHandler(this.RHCenter_Init_Button_Click);
            // 
            // Terminal_Gbox
            // 
            this.Terminal_Gbox.Controls.Add(this.Terminal_Send_Button);
            this.Terminal_Gbox.Controls.Add(this.Terminal_Device_CBox);
            this.Terminal_Gbox.Controls.Add(this.label3);
            this.Terminal_Gbox.Controls.Add(this.Terminal_Command_CBox);
            this.Terminal_Gbox.Controls.Add(this.label2);
            this.Terminal_Gbox.Controls.Add(this.Terminal_Tbox);
            this.Terminal_Gbox.Location = new System.Drawing.Point(6, 175);
            this.Terminal_Gbox.Name = "Terminal_Gbox";
            this.Terminal_Gbox.Size = new System.Drawing.Size(290, 153);
            this.Terminal_Gbox.TabIndex = 2;
            this.Terminal_Gbox.TabStop = false;
            this.Terminal_Gbox.Text = "Terminal";
            // 
            // Terminal_Send_Button
            // 
            this.Terminal_Send_Button.Location = new System.Drawing.Point(18, 114);
            this.Terminal_Send_Button.Name = "Terminal_Send_Button";
            this.Terminal_Send_Button.Size = new System.Drawing.Size(75, 23);
            this.Terminal_Send_Button.TabIndex = 11;
            this.Terminal_Send_Button.Text = "Send";
            this.Terminal_Send_Button.UseVisualStyleBackColor = true;
            this.Terminal_Send_Button.Click += new System.EventHandler(this.Terminal_Send_Button_Click);
            // 
            // Terminal_Device_CBox
            // 
            this.Terminal_Device_CBox.FormattingEnabled = true;
            this.Terminal_Device_CBox.Items.AddRange(new object[] {
            "8 Axis",
            "4 Axis Biss",
            "4 Axis Motor"});
            this.Terminal_Device_CBox.Location = new System.Drawing.Point(178, 47);
            this.Terminal_Device_CBox.Name = "Terminal_Device_CBox";
            this.Terminal_Device_CBox.Size = new System.Drawing.Size(106, 21);
            this.Terminal_Device_CBox.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(131, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Device";
            // 
            // Terminal_Command_CBox
            // 
            this.Terminal_Command_CBox.FormattingEnabled = true;
            this.Terminal_Command_CBox.Items.AddRange(new object[] {
            "RS",
            "ST",
            "TD",
            "TP",
            "TT",
            "QR",
            "QR A"});
            this.Terminal_Command_CBox.Location = new System.Drawing.Point(79, 46);
            this.Terminal_Command_CBox.Name = "Terminal_Command_CBox";
            this.Terminal_Command_CBox.Size = new System.Drawing.Size(41, 21);
            this.Terminal_Command_CBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Command";
            // 
            // Terminal_Tbox
            // 
            this.Terminal_Tbox.Location = new System.Drawing.Point(18, 19);
            this.Terminal_Tbox.Name = "Terminal_Tbox";
            this.Terminal_Tbox.ReadOnly = true;
            this.Terminal_Tbox.Size = new System.Drawing.Size(242, 20);
            this.Terminal_Tbox.TabIndex = 6;
            // 
            // RKnee_Gbox
            // 
            this.RKnee_Gbox.Controls.Add(this.RKnee_Tbox);
            this.RKnee_Gbox.Controls.Add(this.RKnee_Stop_Button);
            this.RKnee_Gbox.Controls.Add(this.RKnee_Goto_Numeric);
            this.RKnee_Gbox.Controls.Add(this.RKnee_Goto_Button);
            this.RKnee_Gbox.Controls.Add(this.RKnee_Neutral_Button);
            this.RKnee_Gbox.Controls.Add(this.RKnee_Init_Button);
            this.RKnee_Gbox.Location = new System.Drawing.Point(754, 452);
            this.RKnee_Gbox.Name = "RKnee_Gbox";
            this.RKnee_Gbox.Size = new System.Drawing.Size(265, 114);
            this.RKnee_Gbox.TabIndex = 1;
            this.RKnee_Gbox.TabStop = false;
            this.RKnee_Gbox.Text = "Right Knee";
            // 
            // RKnee_Tbox
            // 
            this.RKnee_Tbox.Location = new System.Drawing.Point(6, 19);
            this.RKnee_Tbox.Name = "RKnee_Tbox";
            this.RKnee_Tbox.ReadOnly = true;
            this.RKnee_Tbox.Size = new System.Drawing.Size(242, 20);
            this.RKnee_Tbox.TabIndex = 5;
            // 
            // RKnee_Stop_Button
            // 
            this.RKnee_Stop_Button.Location = new System.Drawing.Point(172, 45);
            this.RKnee_Stop_Button.Name = "RKnee_Stop_Button";
            this.RKnee_Stop_Button.Size = new System.Drawing.Size(75, 23);
            this.RKnee_Stop_Button.TabIndex = 4;
            this.RKnee_Stop_Button.Text = "Stop";
            this.RKnee_Stop_Button.UseVisualStyleBackColor = true;
            this.RKnee_Stop_Button.Click += new System.EventHandler(this.RKnee_Stop_Button_Click);
            // 
            // RKnee_Goto_Numeric
            // 
            this.RKnee_Goto_Numeric.Location = new System.Drawing.Point(90, 75);
            this.RKnee_Goto_Numeric.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.RKnee_Goto_Numeric.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.RKnee_Goto_Numeric.Name = "RKnee_Goto_Numeric";
            this.RKnee_Goto_Numeric.Size = new System.Drawing.Size(66, 20);
            this.RKnee_Goto_Numeric.TabIndex = 3;
            // 
            // RKnee_Goto_Button
            // 
            this.RKnee_Goto_Button.Location = new System.Drawing.Point(6, 74);
            this.RKnee_Goto_Button.Name = "RKnee_Goto_Button";
            this.RKnee_Goto_Button.Size = new System.Drawing.Size(75, 23);
            this.RKnee_Goto_Button.TabIndex = 2;
            this.RKnee_Goto_Button.Text = "Goto";
            this.RKnee_Goto_Button.UseVisualStyleBackColor = true;
            this.RKnee_Goto_Button.Click += new System.EventHandler(this.RKnee_Goto_Button_Click);
            // 
            // RKnee_Neutral_Button
            // 
            this.RKnee_Neutral_Button.Location = new System.Drawing.Point(89, 45);
            this.RKnee_Neutral_Button.Name = "RKnee_Neutral_Button";
            this.RKnee_Neutral_Button.Size = new System.Drawing.Size(75, 23);
            this.RKnee_Neutral_Button.TabIndex = 1;
            this.RKnee_Neutral_Button.Text = "HomeP";
            this.RKnee_Neutral_Button.UseVisualStyleBackColor = true;
            this.RKnee_Neutral_Button.Click += new System.EventHandler(this.RKnee_Neutral_Button_Click);
            // 
            // RKnee_Init_Button
            // 
            this.RKnee_Init_Button.Location = new System.Drawing.Point(6, 45);
            this.RKnee_Init_Button.Name = "RKnee_Init_Button";
            this.RKnee_Init_Button.Size = new System.Drawing.Size(75, 23);
            this.RKnee_Init_Button.TabIndex = 0;
            this.RKnee_Init_Button.Text = "Init";
            this.RKnee_Init_Button.UseVisualStyleBackColor = true;
            this.RKnee_Init_Button.Click += new System.EventHandler(this.RKnee_Init_Button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Disconnect_All_Button);
            this.groupBox1.Controls.Add(this.Connect_All_Button);
            this.groupBox1.Controls.Add(this.Disconnect_Button_4axis_Motor);
            this.groupBox1.Controls.Add(this.Connection_Status_Label_4axis_Motor);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Connect_Button_4axis_Motor);
            this.groupBox1.Controls.Add(this.Disconnect_Button_4axis_Biss);
            this.groupBox1.Controls.Add(this.Connection_Status_Label_4axis_Biss);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Connect_Button_4axis_Biss);
            this.groupBox1.Controls.Add(this.Disconnect_Button_8axis);
            this.groupBox1.Controls.Add(this.Connection_Status_Label_8axis);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Connect_Button_8axis);
            this.groupBox1.Location = new System.Drawing.Point(6, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 153);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection";
            // 
            // Disconnect_All_Button
            // 
            this.Disconnect_All_Button.Location = new System.Drawing.Point(228, 114);
            this.Disconnect_All_Button.Name = "Disconnect_All_Button";
            this.Disconnect_All_Button.Size = new System.Drawing.Size(172, 23);
            this.Disconnect_All_Button.TabIndex = 13;
            this.Disconnect_All_Button.Text = "Disconnect All";
            this.Disconnect_All_Button.UseVisualStyleBackColor = true;
            this.Disconnect_All_Button.Click += new System.EventHandler(this.Disconnect_All_Button_Click);
            // 
            // Connect_All_Button
            // 
            this.Connect_All_Button.Location = new System.Drawing.Point(9, 114);
            this.Connect_All_Button.Name = "Connect_All_Button";
            this.Connect_All_Button.Size = new System.Drawing.Size(172, 23);
            this.Connect_All_Button.TabIndex = 12;
            this.Connect_All_Button.Text = "Connect All";
            this.Connect_All_Button.UseVisualStyleBackColor = true;
            this.Connect_All_Button.Click += new System.EventHandler(this.Connect_All_Button_Click);
            // 
            // Disconnect_Button_4axis_Motor
            // 
            this.Disconnect_Button_4axis_Motor.Location = new System.Drawing.Point(228, 82);
            this.Disconnect_Button_4axis_Motor.Name = "Disconnect_Button_4axis_Motor";
            this.Disconnect_Button_4axis_Motor.Size = new System.Drawing.Size(75, 23);
            this.Disconnect_Button_4axis_Motor.TabIndex = 11;
            this.Disconnect_Button_4axis_Motor.Text = "Disconnect";
            this.Disconnect_Button_4axis_Motor.UseVisualStyleBackColor = true;
            this.Disconnect_Button_4axis_Motor.Click += new System.EventHandler(this.Disconnect_Button_4axis_Motor_Click);
            // 
            // Connection_Status_Label_4axis_Motor
            // 
            this.Connection_Status_Label_4axis_Motor.AutoSize = true;
            this.Connection_Status_Label_4axis_Motor.Location = new System.Drawing.Point(324, 87);
            this.Connection_Status_Label_4axis_Motor.Name = "Connection_Status_Label_4axis_Motor";
            this.Connection_Status_Label_4axis_Motor.Size = new System.Drawing.Size(73, 13);
            this.Connection_Status_Label_4axis_Motor.TabIndex = 10;
            this.Connection_Status_Label_4axis_Motor.Text = "Disconnected";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "4axis Motor (192.168.1.70)";
            // 
            // Connect_Button_4axis_Motor
            // 
            this.Connect_Button_4axis_Motor.Location = new System.Drawing.Point(140, 82);
            this.Connect_Button_4axis_Motor.Name = "Connect_Button_4axis_Motor";
            this.Connect_Button_4axis_Motor.Size = new System.Drawing.Size(75, 23);
            this.Connect_Button_4axis_Motor.TabIndex = 8;
            this.Connect_Button_4axis_Motor.Text = "Connect";
            this.Connect_Button_4axis_Motor.UseVisualStyleBackColor = true;
            this.Connect_Button_4axis_Motor.Click += new System.EventHandler(this.Connect_Button_4axis_Motor_Click);
            // 
            // Disconnect_Button_4axis_Biss
            // 
            this.Disconnect_Button_4axis_Biss.Location = new System.Drawing.Point(228, 53);
            this.Disconnect_Button_4axis_Biss.Name = "Disconnect_Button_4axis_Biss";
            this.Disconnect_Button_4axis_Biss.Size = new System.Drawing.Size(75, 23);
            this.Disconnect_Button_4axis_Biss.TabIndex = 7;
            this.Disconnect_Button_4axis_Biss.Text = "Disconnect";
            this.Disconnect_Button_4axis_Biss.UseVisualStyleBackColor = true;
            this.Disconnect_Button_4axis_Biss.Click += new System.EventHandler(this.Disconnect_Button_4axis_Biss_Click);
            // 
            // Connection_Status_Label_4axis_Biss
            // 
            this.Connection_Status_Label_4axis_Biss.AutoSize = true;
            this.Connection_Status_Label_4axis_Biss.Location = new System.Drawing.Point(324, 58);
            this.Connection_Status_Label_4axis_Biss.Name = "Connection_Status_Label_4axis_Biss";
            this.Connection_Status_Label_4axis_Biss.Size = new System.Drawing.Size(73, 13);
            this.Connection_Status_Label_4axis_Biss.TabIndex = 6;
            this.Connection_Status_Label_4axis_Biss.Text = "Disconnected";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "4axis Biss (192.168.1.80)";
            // 
            // Connect_Button_4axis_Biss
            // 
            this.Connect_Button_4axis_Biss.Location = new System.Drawing.Point(140, 53);
            this.Connect_Button_4axis_Biss.Name = "Connect_Button_4axis_Biss";
            this.Connect_Button_4axis_Biss.Size = new System.Drawing.Size(75, 23);
            this.Connect_Button_4axis_Biss.TabIndex = 4;
            this.Connect_Button_4axis_Biss.Text = "Connect";
            this.Connect_Button_4axis_Biss.UseVisualStyleBackColor = true;
            this.Connect_Button_4axis_Biss.Click += new System.EventHandler(this.Connect_Button_4axis_Biss_Click);
            // 
            // Disconnect_Button_8axis
            // 
            this.Disconnect_Button_8axis.Location = new System.Drawing.Point(228, 24);
            this.Disconnect_Button_8axis.Name = "Disconnect_Button_8axis";
            this.Disconnect_Button_8axis.Size = new System.Drawing.Size(75, 23);
            this.Disconnect_Button_8axis.TabIndex = 3;
            this.Disconnect_Button_8axis.Text = "Disconnect";
            this.Disconnect_Button_8axis.UseVisualStyleBackColor = true;
            this.Disconnect_Button_8axis.Click += new System.EventHandler(this.Disconnect_Button_8axis_Click);
            // 
            // Connection_Status_Label_8axis
            // 
            this.Connection_Status_Label_8axis.AutoSize = true;
            this.Connection_Status_Label_8axis.Location = new System.Drawing.Point(324, 29);
            this.Connection_Status_Label_8axis.Name = "Connection_Status_Label_8axis";
            this.Connection_Status_Label_8axis.Size = new System.Drawing.Size(73, 13);
            this.Connection_Status_Label_8axis.TabIndex = 2;
            this.Connection_Status_Label_8axis.Text = "Disconnected";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "8axis (192.168.1.63)";
            // 
            // Connect_Button_8axis
            // 
            this.Connect_Button_8axis.Location = new System.Drawing.Point(140, 24);
            this.Connect_Button_8axis.Name = "Connect_Button_8axis";
            this.Connect_Button_8axis.Size = new System.Drawing.Size(75, 23);
            this.Connect_Button_8axis.TabIndex = 0;
            this.Connect_Button_8axis.Text = "Connect";
            this.Connect_Button_8axis.UseVisualStyleBackColor = true;
            this.Connect_Button_8axis.Click += new System.EventHandler(this.Connect_Button_8axis_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.Motion_Progress_Label);
            this.tabPage2.Controls.Add(this.Motion_Progress_Bar);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.Loop_Button);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.Motion_ModeGbox);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.Start_Position_Gbox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1025, 732);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Motion";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Motion_Progress_Label
            // 
            this.Motion_Progress_Label.Location = new System.Drawing.Point(237, 7);
            this.Motion_Progress_Label.Name = "Motion_Progress_Label";
            this.Motion_Progress_Label.Size = new System.Drawing.Size(753, 23);
            this.Motion_Progress_Label.TabIndex = 54;
            this.Motion_Progress_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Motion_Progress_Bar
            // 
            this.Motion_Progress_Bar.Location = new System.Drawing.Point(237, 30);
            this.Motion_Progress_Bar.Name = "Motion_Progress_Bar";
            this.Motion_Progress_Bar.Size = new System.Drawing.Size(753, 23);
            this.Motion_Progress_Bar.TabIndex = 53;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(869, 235);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 51;
            this.button2.Text = "test";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(215, 485);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 50;
            this.button1.Text = "Goto end";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Loop_Button
            // 
            this.Loop_Button.Location = new System.Drawing.Point(31, 485);
            this.Loop_Button.Name = "Loop_Button";
            this.Loop_Button.Size = new System.Drawing.Size(75, 23);
            this.Loop_Button.TabIndex = 49;
            this.Loop_Button.Text = "Goto start";
            this.Loop_Button.UseVisualStyleBackColor = true;
            this.Loop_Button.Click += new System.EventHandler(this.Loop_Button_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.STB_Tbox);
            this.groupBox8.Controls.Add(this.STB_Button);
            this.groupBox8.Location = new System.Drawing.Point(31, 518);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(327, 57);
            this.groupBox8.TabIndex = 48;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Start position begin";
            // 
            // STB_Tbox
            // 
            this.STB_Tbox.Location = new System.Drawing.Point(87, 22);
            this.STB_Tbox.Name = "STB_Tbox";
            this.STB_Tbox.ReadOnly = true;
            this.STB_Tbox.Size = new System.Drawing.Size(234, 20);
            this.STB_Tbox.TabIndex = 23;
            // 
            // STB_Button
            // 
            this.STB_Button.Location = new System.Drawing.Point(6, 19);
            this.STB_Button.Name = "STB_Button";
            this.STB_Button.Size = new System.Drawing.Size(75, 23);
            this.STB_Button.TabIndex = 22;
            this.STB_Button.Text = "Browse";
            this.STB_Button.UseVisualStyleBackColor = true;
            this.STB_Button.Click += new System.EventHandler(this.STB_Button_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.STE_Tbox);
            this.groupBox7.Controls.Add(this.STE_Button);
            this.groupBox7.Location = new System.Drawing.Point(31, 585);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(327, 57);
            this.groupBox7.TabIndex = 47;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Start position end";
            // 
            // STE_Tbox
            // 
            this.STE_Tbox.Location = new System.Drawing.Point(87, 22);
            this.STE_Tbox.Name = "STE_Tbox";
            this.STE_Tbox.ReadOnly = true;
            this.STE_Tbox.Size = new System.Drawing.Size(234, 20);
            this.STE_Tbox.TabIndex = 23;
            // 
            // STE_Button
            // 
            this.STE_Button.Location = new System.Drawing.Point(6, 19);
            this.STE_Button.Name = "STE_Button";
            this.STE_Button.Size = new System.Drawing.Size(75, 23);
            this.STE_Button.TabIndex = 22;
            this.STE_Button.Text = "Browse";
            this.STE_Button.UseVisualStyleBackColor = true;
            this.STE_Button.Click += new System.EventHandler(this.STE_Button_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.PVT_Refresh_Button);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_LA);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_LK);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_LHR);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_LHC);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_LHF);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_RA);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_RK);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_RHR);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_RHC);
            this.groupBox5.Controls.Add(this.PVT_Executed_Tbox_RHF);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_LA);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_LK);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_LHR);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_LHC);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_LHF);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_RA);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_RK);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_RHR);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_RHC);
            this.groupBox5.Controls.Add(this.PVT_Space_Tbox_RHF);
            this.groupBox5.Location = new System.Drawing.Point(438, 388);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(268, 308);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "PVT Status";
            // 
            // PVT_Refresh_Button
            // 
            this.PVT_Refresh_Button.Location = new System.Drawing.Point(92, 257);
            this.PVT_Refresh_Button.Name = "PVT_Refresh_Button";
            this.PVT_Refresh_Button.Size = new System.Drawing.Size(75, 23);
            this.PVT_Refresh_Button.TabIndex = 36;
            this.PVT_Refresh_Button.Text = "Refresh";
            this.PVT_Refresh_Button.UseVisualStyleBackColor = true;
            this.PVT_Refresh_Button.Click += new System.EventHandler(this.PVT_Refresh_Button_Click);
            // 
            // PVT_Executed_Tbox_LA
            // 
            this.PVT_Executed_Tbox_LA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_LA.Location = new System.Drawing.Point(64, 167);
            this.PVT_Executed_Tbox_LA.Name = "PVT_Executed_Tbox_LA";
            this.PVT_Executed_Tbox_LA.ReadOnly = true;
            this.PVT_Executed_Tbox_LA.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_LA.TabIndex = 35;
            // 
            // PVT_Executed_Tbox_LK
            // 
            this.PVT_Executed_Tbox_LK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_LK.Location = new System.Drawing.Point(92, 142);
            this.PVT_Executed_Tbox_LK.Name = "PVT_Executed_Tbox_LK";
            this.PVT_Executed_Tbox_LK.ReadOnly = true;
            this.PVT_Executed_Tbox_LK.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_LK.TabIndex = 34;
            // 
            // PVT_Executed_Tbox_LHR
            // 
            this.PVT_Executed_Tbox_LHR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_LHR.Location = new System.Drawing.Point(64, 79);
            this.PVT_Executed_Tbox_LHR.Name = "PVT_Executed_Tbox_LHR";
            this.PVT_Executed_Tbox_LHR.ReadOnly = true;
            this.PVT_Executed_Tbox_LHR.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_LHR.TabIndex = 33;
            // 
            // PVT_Executed_Tbox_LHC
            // 
            this.PVT_Executed_Tbox_LHC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_LHC.Location = new System.Drawing.Point(92, 56);
            this.PVT_Executed_Tbox_LHC.Name = "PVT_Executed_Tbox_LHC";
            this.PVT_Executed_Tbox_LHC.ReadOnly = true;
            this.PVT_Executed_Tbox_LHC.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_LHC.TabIndex = 32;
            // 
            // PVT_Executed_Tbox_LHF
            // 
            this.PVT_Executed_Tbox_LHF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_LHF.Location = new System.Drawing.Point(64, 33);
            this.PVT_Executed_Tbox_LHF.Name = "PVT_Executed_Tbox_LHF";
            this.PVT_Executed_Tbox_LHF.ReadOnly = true;
            this.PVT_Executed_Tbox_LHF.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_LHF.TabIndex = 31;
            // 
            // PVT_Executed_Tbox_RA
            // 
            this.PVT_Executed_Tbox_RA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_RA.Location = new System.Drawing.Point(218, 167);
            this.PVT_Executed_Tbox_RA.Name = "PVT_Executed_Tbox_RA";
            this.PVT_Executed_Tbox_RA.ReadOnly = true;
            this.PVT_Executed_Tbox_RA.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_RA.TabIndex = 30;
            // 
            // PVT_Executed_Tbox_RK
            // 
            this.PVT_Executed_Tbox_RK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_RK.Location = new System.Drawing.Point(193, 142);
            this.PVT_Executed_Tbox_RK.Name = "PVT_Executed_Tbox_RK";
            this.PVT_Executed_Tbox_RK.ReadOnly = true;
            this.PVT_Executed_Tbox_RK.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_RK.TabIndex = 29;
            // 
            // PVT_Executed_Tbox_RHR
            // 
            this.PVT_Executed_Tbox_RHR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_RHR.Location = new System.Drawing.Point(218, 79);
            this.PVT_Executed_Tbox_RHR.Name = "PVT_Executed_Tbox_RHR";
            this.PVT_Executed_Tbox_RHR.ReadOnly = true;
            this.PVT_Executed_Tbox_RHR.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_RHR.TabIndex = 28;
            // 
            // PVT_Executed_Tbox_RHC
            // 
            this.PVT_Executed_Tbox_RHC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_RHC.Location = new System.Drawing.Point(193, 56);
            this.PVT_Executed_Tbox_RHC.Name = "PVT_Executed_Tbox_RHC";
            this.PVT_Executed_Tbox_RHC.ReadOnly = true;
            this.PVT_Executed_Tbox_RHC.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_RHC.TabIndex = 27;
            // 
            // PVT_Executed_Tbox_RHF
            // 
            this.PVT_Executed_Tbox_RHF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PVT_Executed_Tbox_RHF.Location = new System.Drawing.Point(218, 33);
            this.PVT_Executed_Tbox_RHF.Name = "PVT_Executed_Tbox_RHF";
            this.PVT_Executed_Tbox_RHF.ReadOnly = true;
            this.PVT_Executed_Tbox_RHF.Size = new System.Drawing.Size(30, 20);
            this.PVT_Executed_Tbox_RHF.TabIndex = 26;
            // 
            // PVT_Space_Tbox_LA
            // 
            this.PVT_Space_Tbox_LA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_LA.Location = new System.Drawing.Point(28, 167);
            this.PVT_Space_Tbox_LA.Name = "PVT_Space_Tbox_LA";
            this.PVT_Space_Tbox_LA.ReadOnly = true;
            this.PVT_Space_Tbox_LA.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_LA.TabIndex = 25;
            // 
            // PVT_Space_Tbox_LK
            // 
            this.PVT_Space_Tbox_LK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_LK.Location = new System.Drawing.Point(56, 142);
            this.PVT_Space_Tbox_LK.Name = "PVT_Space_Tbox_LK";
            this.PVT_Space_Tbox_LK.ReadOnly = true;
            this.PVT_Space_Tbox_LK.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_LK.TabIndex = 24;
            // 
            // PVT_Space_Tbox_LHR
            // 
            this.PVT_Space_Tbox_LHR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_LHR.Location = new System.Drawing.Point(28, 79);
            this.PVT_Space_Tbox_LHR.Name = "PVT_Space_Tbox_LHR";
            this.PVT_Space_Tbox_LHR.ReadOnly = true;
            this.PVT_Space_Tbox_LHR.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_LHR.TabIndex = 23;
            // 
            // PVT_Space_Tbox_LHC
            // 
            this.PVT_Space_Tbox_LHC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_LHC.Location = new System.Drawing.Point(56, 56);
            this.PVT_Space_Tbox_LHC.Name = "PVT_Space_Tbox_LHC";
            this.PVT_Space_Tbox_LHC.ReadOnly = true;
            this.PVT_Space_Tbox_LHC.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_LHC.TabIndex = 22;
            // 
            // PVT_Space_Tbox_LHF
            // 
            this.PVT_Space_Tbox_LHF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_LHF.Location = new System.Drawing.Point(28, 33);
            this.PVT_Space_Tbox_LHF.Name = "PVT_Space_Tbox_LHF";
            this.PVT_Space_Tbox_LHF.ReadOnly = true;
            this.PVT_Space_Tbox_LHF.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_LHF.TabIndex = 21;
            // 
            // PVT_Space_Tbox_RA
            // 
            this.PVT_Space_Tbox_RA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_RA.Location = new System.Drawing.Point(182, 167);
            this.PVT_Space_Tbox_RA.Name = "PVT_Space_Tbox_RA";
            this.PVT_Space_Tbox_RA.ReadOnly = true;
            this.PVT_Space_Tbox_RA.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_RA.TabIndex = 20;
            // 
            // PVT_Space_Tbox_RK
            // 
            this.PVT_Space_Tbox_RK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_RK.Location = new System.Drawing.Point(157, 142);
            this.PVT_Space_Tbox_RK.Name = "PVT_Space_Tbox_RK";
            this.PVT_Space_Tbox_RK.ReadOnly = true;
            this.PVT_Space_Tbox_RK.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_RK.TabIndex = 19;
            // 
            // PVT_Space_Tbox_RHR
            // 
            this.PVT_Space_Tbox_RHR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_RHR.Location = new System.Drawing.Point(182, 79);
            this.PVT_Space_Tbox_RHR.Name = "PVT_Space_Tbox_RHR";
            this.PVT_Space_Tbox_RHR.ReadOnly = true;
            this.PVT_Space_Tbox_RHR.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_RHR.TabIndex = 18;
            // 
            // PVT_Space_Tbox_RHC
            // 
            this.PVT_Space_Tbox_RHC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_RHC.Location = new System.Drawing.Point(157, 56);
            this.PVT_Space_Tbox_RHC.Name = "PVT_Space_Tbox_RHC";
            this.PVT_Space_Tbox_RHC.ReadOnly = true;
            this.PVT_Space_Tbox_RHC.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_RHC.TabIndex = 17;
            // 
            // PVT_Space_Tbox_RHF
            // 
            this.PVT_Space_Tbox_RHF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PVT_Space_Tbox_RHF.Location = new System.Drawing.Point(182, 33);
            this.PVT_Space_Tbox_RHF.Name = "PVT_Space_Tbox_RHF";
            this.PVT_Space_Tbox_RHF.ReadOnly = true;
            this.PVT_Space_Tbox_RHF.Size = new System.Drawing.Size(30, 20);
            this.PVT_Space_Tbox_RHF.TabIndex = 16;
            // 
            // Motion_ModeGbox
            // 
            this.Motion_ModeGbox.Controls.Add(this.NWK_Radiobutton);
            this.Motion_ModeGbox.Controls.Add(this.GWK_Radiobutton);
            this.Motion_ModeGbox.Location = new System.Drawing.Point(20, 30);
            this.Motion_ModeGbox.Name = "Motion_ModeGbox";
            this.Motion_ModeGbox.Size = new System.Drawing.Size(200, 100);
            this.Motion_ModeGbox.TabIndex = 2;
            this.Motion_ModeGbox.TabStop = false;
            this.Motion_ModeGbox.Text = "Motion Mode";
            this.Motion_ModeGbox.Visible = false;
            // 
            // NWK_Radiobutton
            // 
            this.NWK_Radiobutton.AutoSize = true;
            this.NWK_Radiobutton.Location = new System.Drawing.Point(6, 19);
            this.NWK_Radiobutton.Name = "NWK_Radiobutton";
            this.NWK_Radiobutton.Size = new System.Drawing.Size(101, 17);
            this.NWK_Radiobutton.TabIndex = 1;
            this.NWK_Radiobutton.TabStop = true;
            this.NWK_Radiobutton.Text = "Natural Walking";
            this.NWK_Radiobutton.UseVisualStyleBackColor = true;
            // 
            // GWK_Radiobutton
            // 
            this.GWK_Radiobutton.AutoSize = true;
            this.GWK_Radiobutton.Location = new System.Drawing.Point(6, 44);
            this.GWK_Radiobutton.Name = "GWK_Radiobutton";
            this.GWK_Radiobutton.Size = new System.Drawing.Size(117, 17);
            this.GWK_Radiobutton.TabIndex = 0;
            this.GWK_Radiobutton.TabStop = true;
            this.GWK_Radiobutton.Text = "Generated Walking";
            this.GWK_Radiobutton.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Record_Enable_Checkbox);
            this.groupBox2.Controls.Add(this.PVT_Record_Gbox);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.PVT_Run_Button);
            this.groupBox2.Controls.Add(this.PVT_Download_Button);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(438, 74);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(339, 308);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PVT";
            // 
            // Record_Enable_Checkbox
            // 
            this.Record_Enable_Checkbox.AutoSize = true;
            this.Record_Enable_Checkbox.Location = new System.Drawing.Point(23, 232);
            this.Record_Enable_Checkbox.Name = "Record_Enable_Checkbox";
            this.Record_Enable_Checkbox.Size = new System.Drawing.Size(61, 17);
            this.Record_Enable_Checkbox.TabIndex = 47;
            this.Record_Enable_Checkbox.Text = "Record";
            this.Record_Enable_Checkbox.UseVisualStyleBackColor = true;
            this.Record_Enable_Checkbox.CheckedChanged += new System.EventHandler(this.Record_Enable_Checkbox_CheckedChanged);
            // 
            // PVT_Record_Gbox
            // 
            this.PVT_Record_Gbox.Controls.Add(this.label17);
            this.PVT_Record_Gbox.Controls.Add(this.PVT_Record_Rate_Numeric);
            this.PVT_Record_Gbox.Controls.Add(this.label16);
            this.PVT_Record_Gbox.Controls.Add(this.Record_Velocity_Radiobutton);
            this.PVT_Record_Gbox.Controls.Add(this.Record_Torque_Radiobutton);
            this.PVT_Record_Gbox.Controls.Add(this.Record_Angle_Radiobutton);
            this.PVT_Record_Gbox.Controls.Add(this.PVT_Save_Button);
            this.PVT_Record_Gbox.Controls.Add(this.PVT_Pause_Pbox);
            this.PVT_Record_Gbox.Controls.Add(this.PVT_Record_Pbox);
            this.PVT_Record_Gbox.Controls.Add(this.Record_Len_Tbox);
            this.PVT_Record_Gbox.Enabled = false;
            this.PVT_Record_Gbox.Location = new System.Drawing.Point(12, 235);
            this.PVT_Record_Gbox.Name = "PVT_Record_Gbox";
            this.PVT_Record_Gbox.Size = new System.Drawing.Size(315, 67);
            this.PVT_Record_Gbox.TabIndex = 48;
            this.PVT_Record_Gbox.TabStop = false;
            this.PVT_Record_Gbox.Text = "                  ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(124, 45);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 13);
            this.label17.TabIndex = 52;
            this.label17.Text = "ms";
            // 
            // PVT_Record_Rate_Numeric
            // 
            this.PVT_Record_Rate_Numeric.Enabled = false;
            this.PVT_Record_Rate_Numeric.Location = new System.Drawing.Point(73, 41);
            this.PVT_Record_Rate_Numeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PVT_Record_Rate_Numeric.Name = "PVT_Record_Rate_Numeric";
            this.PVT_Record_Rate_Numeric.Size = new System.Drawing.Size(44, 20);
            this.PVT_Record_Rate_Numeric.TabIndex = 51;
            this.PVT_Record_Rate_Numeric.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 45);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 49;
            this.label16.Text = "Sample rate";
            // 
            // Record_Velocity_Radiobutton
            // 
            this.Record_Velocity_Radiobutton.AutoSize = true;
            this.Record_Velocity_Radiobutton.Location = new System.Drawing.Point(130, 19);
            this.Record_Velocity_Radiobutton.Name = "Record_Velocity_Radiobutton";
            this.Record_Velocity_Radiobutton.Size = new System.Drawing.Size(62, 17);
            this.Record_Velocity_Radiobutton.TabIndex = 48;
            this.Record_Velocity_Radiobutton.Text = "Velocity";
            this.Record_Velocity_Radiobutton.UseVisualStyleBackColor = true;
            // 
            // Record_Torque_Radiobutton
            // 
            this.Record_Torque_Radiobutton.AutoSize = true;
            this.Record_Torque_Radiobutton.Location = new System.Drawing.Point(65, 19);
            this.Record_Torque_Radiobutton.Name = "Record_Torque_Radiobutton";
            this.Record_Torque_Radiobutton.Size = new System.Drawing.Size(59, 17);
            this.Record_Torque_Radiobutton.TabIndex = 47;
            this.Record_Torque_Radiobutton.Text = "Torque";
            this.Record_Torque_Radiobutton.UseVisualStyleBackColor = true;
            // 
            // Record_Angle_Radiobutton
            // 
            this.Record_Angle_Radiobutton.AutoSize = true;
            this.Record_Angle_Radiobutton.Checked = true;
            this.Record_Angle_Radiobutton.Location = new System.Drawing.Point(7, 19);
            this.Record_Angle_Radiobutton.Name = "Record_Angle_Radiobutton";
            this.Record_Angle_Radiobutton.Size = new System.Drawing.Size(52, 17);
            this.Record_Angle_Radiobutton.TabIndex = 46;
            this.Record_Angle_Radiobutton.TabStop = true;
            this.Record_Angle_Radiobutton.Text = "Angle";
            this.Record_Angle_Radiobutton.UseVisualStyleBackColor = true;
            // 
            // PVT_Save_Button
            // 
            this.PVT_Save_Button.Location = new System.Drawing.Point(187, 36);
            this.PVT_Save_Button.Name = "PVT_Save_Button";
            this.PVT_Save_Button.Size = new System.Drawing.Size(75, 23);
            this.PVT_Save_Button.TabIndex = 45;
            this.PVT_Save_Button.Text = "Save";
            this.PVT_Save_Button.UseVisualStyleBackColor = true;
            this.PVT_Save_Button.Click += new System.EventHandler(this.PVT_Save_Button_Click);
            // 
            // PVT_Pause_Pbox
            // 
            this.PVT_Pause_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("PVT_Pause_Pbox.Image")));
            this.PVT_Pause_Pbox.Location = new System.Drawing.Point(268, 17);
            this.PVT_Pause_Pbox.Name = "PVT_Pause_Pbox";
            this.PVT_Pause_Pbox.Size = new System.Drawing.Size(41, 42);
            this.PVT_Pause_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PVT_Pause_Pbox.TabIndex = 43;
            this.PVT_Pause_Pbox.TabStop = false;
            this.PVT_Pause_Pbox.Visible = false;
            // 
            // PVT_Record_Pbox
            // 
            this.PVT_Record_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("PVT_Record_Pbox.Image")));
            this.PVT_Record_Pbox.Location = new System.Drawing.Point(268, 17);
            this.PVT_Record_Pbox.Name = "PVT_Record_Pbox";
            this.PVT_Record_Pbox.Size = new System.Drawing.Size(41, 42);
            this.PVT_Record_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PVT_Record_Pbox.TabIndex = 42;
            this.PVT_Record_Pbox.TabStop = false;
            this.PVT_Record_Pbox.Visible = false;
            // 
            // Record_Len_Tbox
            // 
            this.Record_Len_Tbox.Location = new System.Drawing.Point(217, 13);
            this.Record_Len_Tbox.Name = "Record_Len_Tbox";
            this.Record_Len_Tbox.ReadOnly = true;
            this.Record_Len_Tbox.Size = new System.Drawing.Size(41, 20);
            this.Record_Len_Tbox.TabIndex = 44;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.PVT_Filepath_Tbox_Startposition);
            this.groupBox6.Controls.Add(this.PVT_Browse_Button_Start_Position);
            this.groupBox6.Location = new System.Drawing.Point(6, 17);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(327, 57);
            this.groupBox6.TabIndex = 46;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Start position source file";
            // 
            // PVT_Filepath_Tbox_Startposition
            // 
            this.PVT_Filepath_Tbox_Startposition.Location = new System.Drawing.Point(87, 22);
            this.PVT_Filepath_Tbox_Startposition.Name = "PVT_Filepath_Tbox_Startposition";
            this.PVT_Filepath_Tbox_Startposition.ReadOnly = true;
            this.PVT_Filepath_Tbox_Startposition.Size = new System.Drawing.Size(234, 20);
            this.PVT_Filepath_Tbox_Startposition.TabIndex = 23;
            // 
            // PVT_Browse_Button_Start_Position
            // 
            this.PVT_Browse_Button_Start_Position.Location = new System.Drawing.Point(6, 19);
            this.PVT_Browse_Button_Start_Position.Name = "PVT_Browse_Button_Start_Position";
            this.PVT_Browse_Button_Start_Position.Size = new System.Drawing.Size(75, 23);
            this.PVT_Browse_Button_Start_Position.TabIndex = 22;
            this.PVT_Browse_Button_Start_Position.Text = "Browse";
            this.PVT_Browse_Button_Start_Position.UseVisualStyleBackColor = true;
            this.PVT_Browse_Button_Start_Position.Click += new System.EventHandler(this.PVT_Browse_Button_Start_Position_Click);
            // 
            // PVT_Run_Button
            // 
            this.PVT_Run_Button.Enabled = false;
            this.PVT_Run_Button.Location = new System.Drawing.Point(205, 207);
            this.PVT_Run_Button.Name = "PVT_Run_Button";
            this.PVT_Run_Button.Size = new System.Drawing.Size(75, 23);
            this.PVT_Run_Button.TabIndex = 26;
            this.PVT_Run_Button.Text = "Run";
            this.PVT_Run_Button.UseVisualStyleBackColor = true;
            this.PVT_Run_Button.Click += new System.EventHandler(this.PVT_Run_Button_Click);
            // 
            // PVT_Download_Button
            // 
            this.PVT_Download_Button.Location = new System.Drawing.Point(124, 207);
            this.PVT_Download_Button.Name = "PVT_Download_Button";
            this.PVT_Download_Button.Size = new System.Drawing.Size(75, 23);
            this.PVT_Download_Button.TabIndex = 25;
            this.PVT_Download_Button.Text = "Download";
            this.PVT_Download_Button.UseVisualStyleBackColor = true;
            this.PVT_Download_Button.Click += new System.EventHandler(this.PVT_Download_Button_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.PVT_Filepath_Tbox_4axis);
            this.groupBox4.Controls.Add(this.PVT_Browse_Button_4axis);
            this.groupBox4.Location = new System.Drawing.Point(6, 139);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(327, 57);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "4 Axis source file";
            // 
            // PVT_Filepath_Tbox_4axis
            // 
            this.PVT_Filepath_Tbox_4axis.Location = new System.Drawing.Point(87, 22);
            this.PVT_Filepath_Tbox_4axis.Name = "PVT_Filepath_Tbox_4axis";
            this.PVT_Filepath_Tbox_4axis.ReadOnly = true;
            this.PVT_Filepath_Tbox_4axis.Size = new System.Drawing.Size(234, 20);
            this.PVT_Filepath_Tbox_4axis.TabIndex = 23;
            // 
            // PVT_Browse_Button_4axis
            // 
            this.PVT_Browse_Button_4axis.Location = new System.Drawing.Point(6, 19);
            this.PVT_Browse_Button_4axis.Name = "PVT_Browse_Button_4axis";
            this.PVT_Browse_Button_4axis.Size = new System.Drawing.Size(75, 23);
            this.PVT_Browse_Button_4axis.TabIndex = 22;
            this.PVT_Browse_Button_4axis.Text = "Browse";
            this.PVT_Browse_Button_4axis.UseVisualStyleBackColor = true;
            this.PVT_Browse_Button_4axis.Click += new System.EventHandler(this.PVT_Browse_Button_4axis_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.PVT_Filepath_Tbox_8axis);
            this.groupBox3.Controls.Add(this.PVT_Browse_Button_8axis);
            this.groupBox3.Location = new System.Drawing.Point(6, 78);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(327, 57);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "8 Axis source file";
            // 
            // PVT_Filepath_Tbox_8axis
            // 
            this.PVT_Filepath_Tbox_8axis.Location = new System.Drawing.Point(87, 22);
            this.PVT_Filepath_Tbox_8axis.Name = "PVT_Filepath_Tbox_8axis";
            this.PVT_Filepath_Tbox_8axis.ReadOnly = true;
            this.PVT_Filepath_Tbox_8axis.Size = new System.Drawing.Size(234, 20);
            this.PVT_Filepath_Tbox_8axis.TabIndex = 23;
            // 
            // PVT_Browse_Button_8axis
            // 
            this.PVT_Browse_Button_8axis.Location = new System.Drawing.Point(6, 19);
            this.PVT_Browse_Button_8axis.Name = "PVT_Browse_Button_8axis";
            this.PVT_Browse_Button_8axis.Size = new System.Drawing.Size(75, 23);
            this.PVT_Browse_Button_8axis.TabIndex = 22;
            this.PVT_Browse_Button_8axis.Text = "Browse";
            this.PVT_Browse_Button_8axis.UseVisualStyleBackColor = true;
            this.PVT_Browse_Button_8axis.Click += new System.EventHandler(this.PVT_Browse_Button_8axis_Click);
            // 
            // Start_Position_Gbox
            // 
            this.Start_Position_Gbox.Controls.Add(this.Tick_LA_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Tick_RK_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Tick_RA_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Tick_RHF_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Tick_RHR_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Warning_RK_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Tick_RHC_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Warning_RA_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Warning_RHF_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Tick_LHC_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Tick_LK_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Tick_LHR_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_Check_Button);
            this.Start_Position_Gbox.Controls.Add(this.ST_Go_Button);
            this.Start_Position_Gbox.Controls.Add(this.Tick_LHF_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_RA_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_RK_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_RHR_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.Warning_LA_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_RHC_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_RHF_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.Warning_LK_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.label11);
            this.Start_Position_Gbox.Controls.Add(this.label12);
            this.Start_Position_Gbox.Controls.Add(this.Warning_LHR_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.label13);
            this.Start_Position_Gbox.Controls.Add(this.label14);
            this.Start_Position_Gbox.Controls.Add(this.Warning_LHC_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.label15);
            this.Start_Position_Gbox.Controls.Add(this.ST_LA_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.Warning_LHF_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_LK_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_LHR_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_LHC_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.ST_LHF_Tbox);
            this.Start_Position_Gbox.Controls.Add(this.label10);
            this.Start_Position_Gbox.Controls.Add(this.label9);
            this.Start_Position_Gbox.Controls.Add(this.label8);
            this.Start_Position_Gbox.Controls.Add(this.label7);
            this.Start_Position_Gbox.Controls.Add(this.label5);
            this.Start_Position_Gbox.Controls.Add(this.Warning_RHC_Pbox);
            this.Start_Position_Gbox.Controls.Add(this.Warning_RHR_Pbox);
            this.Start_Position_Gbox.Location = new System.Drawing.Point(20, 144);
            this.Start_Position_Gbox.Name = "Start_Position_Gbox";
            this.Start_Position_Gbox.Size = new System.Drawing.Size(412, 238);
            this.Start_Position_Gbox.TabIndex = 0;
            this.Start_Position_Gbox.TabStop = false;
            this.Start_Position_Gbox.Text = "Start Position";
            // 
            // Tick_LA_Pbox
            // 
            this.Tick_LA_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_LA_Pbox.Image")));
            this.Tick_LA_Pbox.Location = new System.Drawing.Point(178, 133);
            this.Tick_LA_Pbox.Name = "Tick_LA_Pbox";
            this.Tick_LA_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_LA_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_LA_Pbox.TabIndex = 39;
            this.Tick_LA_Pbox.TabStop = false;
            this.Tick_LA_Pbox.Visible = false;
            // 
            // Tick_RK_Pbox
            // 
            this.Tick_RK_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_RK_Pbox.Image")));
            this.Tick_RK_Pbox.Location = new System.Drawing.Point(384, 108);
            this.Tick_RK_Pbox.Name = "Tick_RK_Pbox";
            this.Tick_RK_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_RK_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_RK_Pbox.TabIndex = 47;
            this.Tick_RK_Pbox.TabStop = false;
            this.Tick_RK_Pbox.Visible = false;
            // 
            // Tick_RA_Pbox
            // 
            this.Tick_RA_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_RA_Pbox.Image")));
            this.Tick_RA_Pbox.Location = new System.Drawing.Point(384, 133);
            this.Tick_RA_Pbox.Name = "Tick_RA_Pbox";
            this.Tick_RA_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_RA_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_RA_Pbox.TabIndex = 41;
            this.Tick_RA_Pbox.TabStop = false;
            this.Tick_RA_Pbox.Visible = false;
            // 
            // Tick_RHF_Pbox
            // 
            this.Tick_RHF_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_RHF_Pbox.Image")));
            this.Tick_RHF_Pbox.Location = new System.Drawing.Point(384, 33);
            this.Tick_RHF_Pbox.Name = "Tick_RHF_Pbox";
            this.Tick_RHF_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_RHF_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_RHF_Pbox.TabIndex = 45;
            this.Tick_RHF_Pbox.TabStop = false;
            this.Tick_RHF_Pbox.Visible = false;
            // 
            // Tick_RHR_Pbox
            // 
            this.Tick_RHR_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_RHR_Pbox.Image")));
            this.Tick_RHR_Pbox.Location = new System.Drawing.Point(384, 83);
            this.Tick_RHR_Pbox.Name = "Tick_RHR_Pbox";
            this.Tick_RHR_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_RHR_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_RHR_Pbox.TabIndex = 39;
            this.Tick_RHR_Pbox.TabStop = false;
            this.Tick_RHR_Pbox.Visible = false;
            // 
            // Warning_RK_Pbox
            // 
            this.Warning_RK_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_RK_Pbox.Image")));
            this.Warning_RK_Pbox.Location = new System.Drawing.Point(384, 108);
            this.Warning_RK_Pbox.Name = "Warning_RK_Pbox";
            this.Warning_RK_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_RK_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_RK_Pbox.TabIndex = 40;
            this.Warning_RK_Pbox.TabStop = false;
            this.Warning_RK_Pbox.Visible = false;
            // 
            // Tick_RHC_Pbox
            // 
            this.Tick_RHC_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_RHC_Pbox.Image")));
            this.Tick_RHC_Pbox.Location = new System.Drawing.Point(384, 58);
            this.Tick_RHC_Pbox.Name = "Tick_RHC_Pbox";
            this.Tick_RHC_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_RHC_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_RHC_Pbox.TabIndex = 43;
            this.Tick_RHC_Pbox.TabStop = false;
            this.Tick_RHC_Pbox.Visible = false;
            // 
            // Warning_RA_Pbox
            // 
            this.Warning_RA_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_RA_Pbox.Image")));
            this.Warning_RA_Pbox.Location = new System.Drawing.Point(384, 133);
            this.Warning_RA_Pbox.Name = "Warning_RA_Pbox";
            this.Warning_RA_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_RA_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_RA_Pbox.TabIndex = 44;
            this.Warning_RA_Pbox.TabStop = false;
            this.Warning_RA_Pbox.Visible = false;
            // 
            // Warning_RHF_Pbox
            // 
            this.Warning_RHF_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_RHF_Pbox.Image")));
            this.Warning_RHF_Pbox.Location = new System.Drawing.Point(384, 33);
            this.Warning_RHF_Pbox.Name = "Warning_RHF_Pbox";
            this.Warning_RHF_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_RHF_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_RHF_Pbox.TabIndex = 50;
            this.Warning_RHF_Pbox.TabStop = false;
            this.Warning_RHF_Pbox.Visible = false;
            // 
            // Tick_LHC_Pbox
            // 
            this.Tick_LHC_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_LHC_Pbox.Image")));
            this.Tick_LHC_Pbox.Location = new System.Drawing.Point(178, 58);
            this.Tick_LHC_Pbox.Name = "Tick_LHC_Pbox";
            this.Tick_LHC_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_LHC_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_LHC_Pbox.TabIndex = 49;
            this.Tick_LHC_Pbox.TabStop = false;
            this.Tick_LHC_Pbox.Visible = false;
            // 
            // Tick_LK_Pbox
            // 
            this.Tick_LK_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_LK_Pbox.Image")));
            this.Tick_LK_Pbox.Location = new System.Drawing.Point(178, 108);
            this.Tick_LK_Pbox.Name = "Tick_LK_Pbox";
            this.Tick_LK_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_LK_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_LK_Pbox.TabIndex = 37;
            this.Tick_LK_Pbox.TabStop = false;
            this.Tick_LK_Pbox.Visible = false;
            // 
            // Tick_LHR_Pbox
            // 
            this.Tick_LHR_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_LHR_Pbox.Image")));
            this.Tick_LHR_Pbox.Location = new System.Drawing.Point(178, 83);
            this.Tick_LHR_Pbox.Name = "Tick_LHR_Pbox";
            this.Tick_LHR_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_LHR_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_LHR_Pbox.TabIndex = 39;
            this.Tick_LHR_Pbox.TabStop = false;
            this.Tick_LHR_Pbox.Visible = false;
            // 
            // ST_Check_Button
            // 
            this.ST_Check_Button.Location = new System.Drawing.Point(173, 165);
            this.ST_Check_Button.Name = "ST_Check_Button";
            this.ST_Check_Button.Size = new System.Drawing.Size(75, 23);
            this.ST_Check_Button.TabIndex = 23;
            this.ST_Check_Button.Text = "Check";
            this.ST_Check_Button.UseVisualStyleBackColor = true;
            this.ST_Check_Button.Click += new System.EventHandler(this.ST_Check_Button_Click);
            // 
            // ST_Go_Button
            // 
            this.ST_Go_Button.Location = new System.Drawing.Point(92, 165);
            this.ST_Go_Button.Name = "ST_Go_Button";
            this.ST_Go_Button.Size = new System.Drawing.Size(75, 23);
            this.ST_Go_Button.TabIndex = 22;
            this.ST_Go_Button.Text = "Go";
            this.ST_Go_Button.UseVisualStyleBackColor = true;
            this.ST_Go_Button.Click += new System.EventHandler(this.ST_Go_Button_Click);
            // 
            // Tick_LHF_Pbox
            // 
            this.Tick_LHF_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Tick_LHF_Pbox.Image")));
            this.Tick_LHF_Pbox.Location = new System.Drawing.Point(178, 33);
            this.Tick_LHF_Pbox.Name = "Tick_LHF_Pbox";
            this.Tick_LHF_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Tick_LHF_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Tick_LHF_Pbox.TabIndex = 35;
            this.Tick_LHF_Pbox.TabStop = false;
            this.Tick_LHF_Pbox.Visible = false;
            // 
            // ST_RA_Tbox
            // 
            this.ST_RA_Tbox.Location = new System.Drawing.Point(328, 131);
            this.ST_RA_Tbox.Name = "ST_RA_Tbox";
            this.ST_RA_Tbox.ReadOnly = true;
            this.ST_RA_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_RA_Tbox.TabIndex = 20;
            // 
            // ST_RK_Tbox
            // 
            this.ST_RK_Tbox.Location = new System.Drawing.Point(328, 106);
            this.ST_RK_Tbox.Name = "ST_RK_Tbox";
            this.ST_RK_Tbox.ReadOnly = true;
            this.ST_RK_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_RK_Tbox.TabIndex = 19;
            // 
            // ST_RHR_Tbox
            // 
            this.ST_RHR_Tbox.Location = new System.Drawing.Point(328, 81);
            this.ST_RHR_Tbox.Name = "ST_RHR_Tbox";
            this.ST_RHR_Tbox.ReadOnly = true;
            this.ST_RHR_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_RHR_Tbox.TabIndex = 18;
            // 
            // Warning_LA_Pbox
            // 
            this.Warning_LA_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_LA_Pbox.Image")));
            this.Warning_LA_Pbox.Location = new System.Drawing.Point(178, 133);
            this.Warning_LA_Pbox.Name = "Warning_LA_Pbox";
            this.Warning_LA_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_LA_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_LA_Pbox.TabIndex = 40;
            this.Warning_LA_Pbox.TabStop = false;
            this.Warning_LA_Pbox.Visible = false;
            // 
            // ST_RHC_Tbox
            // 
            this.ST_RHC_Tbox.Location = new System.Drawing.Point(328, 56);
            this.ST_RHC_Tbox.Name = "ST_RHC_Tbox";
            this.ST_RHC_Tbox.ReadOnly = true;
            this.ST_RHC_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_RHC_Tbox.TabIndex = 17;
            // 
            // ST_RHF_Tbox
            // 
            this.ST_RHF_Tbox.Location = new System.Drawing.Point(328, 31);
            this.ST_RHF_Tbox.Name = "ST_RHF_Tbox";
            this.ST_RHF_Tbox.ReadOnly = true;
            this.ST_RHF_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_RHF_Tbox.TabIndex = 16;
            // 
            // Warning_LK_Pbox
            // 
            this.Warning_LK_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_LK_Pbox.Image")));
            this.Warning_LK_Pbox.Location = new System.Drawing.Point(178, 108);
            this.Warning_LK_Pbox.Name = "Warning_LK_Pbox";
            this.Warning_LK_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_LK_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_LK_Pbox.TabIndex = 42;
            this.Warning_LK_Pbox.TabStop = false;
            this.Warning_LK_Pbox.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(203, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Right ankle angle: ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(203, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Right knee angle: ";
            // 
            // Warning_LHR_Pbox
            // 
            this.Warning_LHR_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_LHR_Pbox.Image")));
            this.Warning_LHR_Pbox.Location = new System.Drawing.Point(178, 83);
            this.Warning_LHR_Pbox.Name = "Warning_LHR_Pbox";
            this.Warning_LHR_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_LHR_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_LHR_Pbox.TabIndex = 40;
            this.Warning_LHR_Pbox.TabStop = false;
            this.Warning_LHR_Pbox.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(203, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Right hip rear angle: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(203, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(117, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Right hip center angle: ";
            // 
            // Warning_LHC_Pbox
            // 
            this.Warning_LHC_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_LHC_Pbox.Image")));
            this.Warning_LHC_Pbox.Location = new System.Drawing.Point(178, 58);
            this.Warning_LHC_Pbox.Name = "Warning_LHC_Pbox";
            this.Warning_LHC_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_LHC_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_LHC_Pbox.TabIndex = 38;
            this.Warning_LHC_Pbox.TabStop = false;
            this.Warning_LHC_Pbox.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(203, 35);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(108, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "Right hip front angle: ";
            // 
            // ST_LA_Tbox
            // 
            this.ST_LA_Tbox.Location = new System.Drawing.Point(125, 131);
            this.ST_LA_Tbox.Name = "ST_LA_Tbox";
            this.ST_LA_Tbox.ReadOnly = true;
            this.ST_LA_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_LA_Tbox.TabIndex = 10;
            // 
            // Warning_LHF_Pbox
            // 
            this.Warning_LHF_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_LHF_Pbox.Image")));
            this.Warning_LHF_Pbox.Location = new System.Drawing.Point(178, 33);
            this.Warning_LHF_Pbox.Name = "Warning_LHF_Pbox";
            this.Warning_LHF_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_LHF_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_LHF_Pbox.TabIndex = 36;
            this.Warning_LHF_Pbox.TabStop = false;
            this.Warning_LHF_Pbox.Visible = false;
            // 
            // ST_LK_Tbox
            // 
            this.ST_LK_Tbox.Location = new System.Drawing.Point(125, 106);
            this.ST_LK_Tbox.Name = "ST_LK_Tbox";
            this.ST_LK_Tbox.ReadOnly = true;
            this.ST_LK_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_LK_Tbox.TabIndex = 9;
            // 
            // ST_LHR_Tbox
            // 
            this.ST_LHR_Tbox.Location = new System.Drawing.Point(125, 81);
            this.ST_LHR_Tbox.Name = "ST_LHR_Tbox";
            this.ST_LHR_Tbox.ReadOnly = true;
            this.ST_LHR_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_LHR_Tbox.TabIndex = 8;
            // 
            // ST_LHC_Tbox
            // 
            this.ST_LHC_Tbox.Location = new System.Drawing.Point(125, 56);
            this.ST_LHC_Tbox.Name = "ST_LHC_Tbox";
            this.ST_LHC_Tbox.ReadOnly = true;
            this.ST_LHC_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_LHC_Tbox.TabIndex = 7;
            // 
            // ST_LHF_Tbox
            // 
            this.ST_LHF_Tbox.Location = new System.Drawing.Point(125, 31);
            this.ST_LHF_Tbox.Name = "ST_LHF_Tbox";
            this.ST_LHF_Tbox.ReadOnly = true;
            this.ST_LHF_Tbox.Size = new System.Drawing.Size(50, 20);
            this.ST_LHF_Tbox.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Left ankle angle: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Left knee angle: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Left hip rear angle: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Left hip center angle: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Left hip front angle: ";
            // 
            // Warning_RHC_Pbox
            // 
            this.Warning_RHC_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_RHC_Pbox.Image")));
            this.Warning_RHC_Pbox.Location = new System.Drawing.Point(384, 58);
            this.Warning_RHC_Pbox.Name = "Warning_RHC_Pbox";
            this.Warning_RHC_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_RHC_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_RHC_Pbox.TabIndex = 48;
            this.Warning_RHC_Pbox.TabStop = false;
            this.Warning_RHC_Pbox.Visible = false;
            // 
            // Warning_RHR_Pbox
            // 
            this.Warning_RHR_Pbox.Image = ((System.Drawing.Image)(resources.GetObject("Warning_RHR_Pbox.Image")));
            this.Warning_RHR_Pbox.Location = new System.Drawing.Point(384, 83);
            this.Warning_RHR_Pbox.Name = "Warning_RHR_Pbox";
            this.Warning_RHR_Pbox.Size = new System.Drawing.Size(22, 17);
            this.Warning_RHR_Pbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Warning_RHR_Pbox.TabIndex = 46;
            this.Warning_RHR_Pbox.TabStop = false;
            this.Warning_RHR_Pbox.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1025, 732);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Analyze";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // Stop_ALL_Button
            // 
            this.Stop_ALL_Button.Location = new System.Drawing.Point(1279, 22);
            this.Stop_ALL_Button.Name = "Stop_ALL_Button";
            this.Stop_ALL_Button.Size = new System.Drawing.Size(75, 23);
            this.Stop_ALL_Button.TabIndex = 23;
            this.Stop_ALL_Button.Text = "Stop All";
            this.Stop_ALL_Button.UseVisualStyleBackColor = true;
            this.Stop_ALL_Button.Click += new System.EventHandler(this.Stop_ALL_Button_Click);
            // 
            // Basetick
            // 
            this.Basetick.Enabled = true;
            this.Basetick.Tick += new System.EventHandler(this.Basetick_Tick);
            // 
            // Clear_Button
            // 
            this.Clear_Button.Location = new System.Drawing.Point(1279, 96);
            this.Clear_Button.Name = "Clear_Button";
            this.Clear_Button.Size = new System.Drawing.Size(75, 23);
            this.Clear_Button.TabIndex = 1;
            this.Clear_Button.Text = "Clear";
            this.Clear_Button.UseVisualStyleBackColor = true;
            this.Clear_Button.Click += new System.EventHandler(this.Clear_Button_Click);
            // 
            // RHF_A_Tbox
            // 
            this.RHF_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.RHF_A_Tbox.Location = new System.Drawing.Point(1314, 134);
            this.RHF_A_Tbox.Name = "RHF_A_Tbox";
            this.RHF_A_Tbox.ReadOnly = true;
            this.RHF_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RHF_A_Tbox.TabIndex = 6;
            // 
            // RHC_A_Tbox
            // 
            this.RHC_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.RHC_A_Tbox.Location = new System.Drawing.Point(1289, 157);
            this.RHC_A_Tbox.Name = "RHC_A_Tbox";
            this.RHC_A_Tbox.ReadOnly = true;
            this.RHC_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RHC_A_Tbox.TabIndex = 7;
            // 
            // RHR_A_Tbox
            // 
            this.RHR_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.RHR_A_Tbox.Location = new System.Drawing.Point(1314, 180);
            this.RHR_A_Tbox.Name = "RHR_A_Tbox";
            this.RHR_A_Tbox.ReadOnly = true;
            this.RHR_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RHR_A_Tbox.TabIndex = 8;
            // 
            // RK_A_Tbox
            // 
            this.RK_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.RK_A_Tbox.Location = new System.Drawing.Point(1289, 243);
            this.RK_A_Tbox.Name = "RK_A_Tbox";
            this.RK_A_Tbox.ReadOnly = true;
            this.RK_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RK_A_Tbox.TabIndex = 9;
            // 
            // RA_A_Tbox
            // 
            this.RA_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.RA_A_Tbox.Location = new System.Drawing.Point(1314, 268);
            this.RA_A_Tbox.Name = "RA_A_Tbox";
            this.RA_A_Tbox.ReadOnly = true;
            this.RA_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RA_A_Tbox.TabIndex = 10;
            // 
            // LHF_A_Tbox
            // 
            this.LHF_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LHF_A_Tbox.Location = new System.Drawing.Point(1160, 134);
            this.LHF_A_Tbox.Name = "LHF_A_Tbox";
            this.LHF_A_Tbox.ReadOnly = true;
            this.LHF_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LHF_A_Tbox.TabIndex = 11;
            // 
            // LHC_A_Tbox
            // 
            this.LHC_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LHC_A_Tbox.Location = new System.Drawing.Point(1188, 157);
            this.LHC_A_Tbox.Name = "LHC_A_Tbox";
            this.LHC_A_Tbox.ReadOnly = true;
            this.LHC_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LHC_A_Tbox.TabIndex = 12;
            // 
            // LHR_A_Tbox
            // 
            this.LHR_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LHR_A_Tbox.Location = new System.Drawing.Point(1160, 180);
            this.LHR_A_Tbox.Name = "LHR_A_Tbox";
            this.LHR_A_Tbox.ReadOnly = true;
            this.LHR_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LHR_A_Tbox.TabIndex = 13;
            // 
            // LK_A_Tbox
            // 
            this.LK_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LK_A_Tbox.Location = new System.Drawing.Point(1188, 243);
            this.LK_A_Tbox.Name = "LK_A_Tbox";
            this.LK_A_Tbox.ReadOnly = true;
            this.LK_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LK_A_Tbox.TabIndex = 14;
            // 
            // LA_A_Tbox
            // 
            this.LA_A_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LA_A_Tbox.Location = new System.Drawing.Point(1160, 268);
            this.LA_A_Tbox.Name = "LA_A_Tbox";
            this.LA_A_Tbox.ReadOnly = true;
            this.LA_A_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LA_A_Tbox.TabIndex = 15;
            // 
            // Refresh_Button
            // 
            this.Refresh_Button.Location = new System.Drawing.Point(1222, 306);
            this.Refresh_Button.Name = "Refresh_Button";
            this.Refresh_Button.Size = new System.Drawing.Size(75, 23);
            this.Refresh_Button.TabIndex = 16;
            this.Refresh_Button.Text = "Refresh";
            this.Refresh_Button.UseVisualStyleBackColor = true;
            this.Refresh_Button.Click += new System.EventHandler(this.Refresh_Button_Click);
            // 
            // Reset_All_Button
            // 
            this.Reset_All_Button.Location = new System.Drawing.Point(1175, 22);
            this.Reset_All_Button.Name = "Reset_All_Button";
            this.Reset_All_Button.Size = new System.Drawing.Size(75, 23);
            this.Reset_All_Button.TabIndex = 24;
            this.Reset_All_Button.Text = "Reset All";
            this.Reset_All_Button.UseVisualStyleBackColor = true;
            this.Reset_All_Button.Click += new System.EventHandler(this.Reset_All_Button_Click);
            // 
            // LA_T_Tbox
            // 
            this.LA_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LA_T_Tbox.Location = new System.Drawing.Point(1160, 481);
            this.LA_T_Tbox.Name = "LA_T_Tbox";
            this.LA_T_Tbox.ReadOnly = true;
            this.LA_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LA_T_Tbox.TabIndex = 34;
            // 
            // LK_T_Tbox
            // 
            this.LK_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LK_T_Tbox.Location = new System.Drawing.Point(1188, 456);
            this.LK_T_Tbox.Name = "LK_T_Tbox";
            this.LK_T_Tbox.ReadOnly = true;
            this.LK_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LK_T_Tbox.TabIndex = 33;
            // 
            // LHR_T_Tbox
            // 
            this.LHR_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LHR_T_Tbox.Location = new System.Drawing.Point(1160, 393);
            this.LHR_T_Tbox.Name = "LHR_T_Tbox";
            this.LHR_T_Tbox.ReadOnly = true;
            this.LHR_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LHR_T_Tbox.TabIndex = 32;
            // 
            // LHC_T_Tbox
            // 
            this.LHC_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LHC_T_Tbox.Location = new System.Drawing.Point(1188, 370);
            this.LHC_T_Tbox.Name = "LHC_T_Tbox";
            this.LHC_T_Tbox.ReadOnly = true;
            this.LHC_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LHC_T_Tbox.TabIndex = 31;
            // 
            // LHF_T_Tbox
            // 
            this.LHF_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.LHF_T_Tbox.Location = new System.Drawing.Point(1160, 347);
            this.LHF_T_Tbox.Name = "LHF_T_Tbox";
            this.LHF_T_Tbox.ReadOnly = true;
            this.LHF_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.LHF_T_Tbox.TabIndex = 30;
            // 
            // RA_T_Tbox
            // 
            this.RA_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.RA_T_Tbox.Location = new System.Drawing.Point(1314, 481);
            this.RA_T_Tbox.Name = "RA_T_Tbox";
            this.RA_T_Tbox.ReadOnly = true;
            this.RA_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RA_T_Tbox.TabIndex = 29;
            // 
            // RK_T_Tbox
            // 
            this.RK_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.RK_T_Tbox.Location = new System.Drawing.Point(1289, 456);
            this.RK_T_Tbox.Name = "RK_T_Tbox";
            this.RK_T_Tbox.ReadOnly = true;
            this.RK_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RK_T_Tbox.TabIndex = 28;
            // 
            // RHR_T_Tbox
            // 
            this.RHR_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.RHR_T_Tbox.Location = new System.Drawing.Point(1314, 393);
            this.RHR_T_Tbox.Name = "RHR_T_Tbox";
            this.RHR_T_Tbox.ReadOnly = true;
            this.RHR_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RHR_T_Tbox.TabIndex = 27;
            // 
            // RHC_T_Tbox
            // 
            this.RHC_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.RHC_T_Tbox.Location = new System.Drawing.Point(1289, 370);
            this.RHC_T_Tbox.Name = "RHC_T_Tbox";
            this.RHC_T_Tbox.ReadOnly = true;
            this.RHC_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RHC_T_Tbox.TabIndex = 26;
            // 
            // RHF_T_Tbox
            // 
            this.RHF_T_Tbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.RHF_T_Tbox.Location = new System.Drawing.Point(1314, 347);
            this.RHF_T_Tbox.Name = "RHF_T_Tbox";
            this.RHF_T_Tbox.ReadOnly = true;
            this.RHF_T_Tbox.Size = new System.Drawing.Size(40, 20);
            this.RHF_T_Tbox.TabIndex = 25;
            // 
            // Error_Gbox_8axis
            // 
            this.Error_Gbox_8axis.Controls.Add(this.Error_ELO_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_Peakcurrent_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_Hall_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_UndervoltageEH_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_OvertemperetureEH_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_OvervoltageEH_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_OvercurrentEH_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_UndervoltageAD_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_OvertemperetureAD_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_OvervoltageAD_Checkbox_8axis);
            this.Error_Gbox_8axis.Controls.Add(this.Error_OvercurrentAD_Checkbox_8axis);
            this.Error_Gbox_8axis.Location = new System.Drawing.Point(1041, 507);
            this.Error_Gbox_8axis.Name = "Error_Gbox_8axis";
            this.Error_Gbox_8axis.Size = new System.Drawing.Size(147, 251);
            this.Error_Gbox_8axis.TabIndex = 0;
            this.Error_Gbox_8axis.TabStop = false;
            this.Error_Gbox_8axis.Text = "8 Axis Errors";
            // 
            // Error_ELO_Checkbox_8axis
            // 
            this.Error_ELO_Checkbox_8axis.AutoSize = true;
            this.Error_ELO_Checkbox_8axis.Enabled = false;
            this.Error_ELO_Checkbox_8axis.Location = new System.Drawing.Point(6, 16);
            this.Error_ELO_Checkbox_8axis.Name = "Error_ELO_Checkbox_8axis";
            this.Error_ELO_Checkbox_8axis.Size = new System.Drawing.Size(47, 17);
            this.Error_ELO_Checkbox_8axis.TabIndex = 10;
            this.Error_ELO_Checkbox_8axis.Text = "ELO";
            this.Error_ELO_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_Peakcurrent_Checkbox_8axis
            // 
            this.Error_Peakcurrent_Checkbox_8axis.AutoSize = true;
            this.Error_Peakcurrent_Checkbox_8axis.Enabled = false;
            this.Error_Peakcurrent_Checkbox_8axis.Location = new System.Drawing.Point(6, 37);
            this.Error_Peakcurrent_Checkbox_8axis.Name = "Error_Peakcurrent_Checkbox_8axis";
            this.Error_Peakcurrent_Checkbox_8axis.Size = new System.Drawing.Size(88, 17);
            this.Error_Peakcurrent_Checkbox_8axis.TabIndex = 9;
            this.Error_Peakcurrent_Checkbox_8axis.Text = "Peak Current";
            this.Error_Peakcurrent_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_Hall_Checkbox_8axis
            // 
            this.Error_Hall_Checkbox_8axis.AutoSize = true;
            this.Error_Hall_Checkbox_8axis.Enabled = false;
            this.Error_Hall_Checkbox_8axis.Location = new System.Drawing.Point(6, 58);
            this.Error_Hall_Checkbox_8axis.Name = "Error_Hall_Checkbox_8axis";
            this.Error_Hall_Checkbox_8axis.Size = new System.Drawing.Size(69, 17);
            this.Error_Hall_Checkbox_8axis.TabIndex = 8;
            this.Error_Hall_Checkbox_8axis.Text = "Hall Error";
            this.Error_Hall_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_UndervoltageEH_Checkbox_8axis
            // 
            this.Error_UndervoltageEH_Checkbox_8axis.AutoSize = true;
            this.Error_UndervoltageEH_Checkbox_8axis.Enabled = false;
            this.Error_UndervoltageEH_Checkbox_8axis.Location = new System.Drawing.Point(6, 226);
            this.Error_UndervoltageEH_Checkbox_8axis.Name = "Error_UndervoltageEH_Checkbox_8axis";
            this.Error_UndervoltageEH_Checkbox_8axis.Size = new System.Drawing.Size(118, 17);
            this.Error_UndervoltageEH_Checkbox_8axis.TabIndex = 7;
            this.Error_UndervoltageEH_Checkbox_8axis.Text = "Under Voltage(E-H)";
            this.Error_UndervoltageEH_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvertemperetureEH_Checkbox_8axis
            // 
            this.Error_OvertemperetureEH_Checkbox_8axis.AutoSize = true;
            this.Error_OvertemperetureEH_Checkbox_8axis.Enabled = false;
            this.Error_OvertemperetureEH_Checkbox_8axis.Location = new System.Drawing.Point(6, 205);
            this.Error_OvertemperetureEH_Checkbox_8axis.Name = "Error_OvertemperetureEH_Checkbox_8axis";
            this.Error_OvertemperetureEH_Checkbox_8axis.Size = new System.Drawing.Size(130, 17);
            this.Error_OvertemperetureEH_Checkbox_8axis.TabIndex = 6;
            this.Error_OvertemperetureEH_Checkbox_8axis.Text = "Over Temperture(E-H)";
            this.Error_OvertemperetureEH_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvervoltageEH_Checkbox_8axis
            // 
            this.Error_OvervoltageEH_Checkbox_8axis.AutoSize = true;
            this.Error_OvervoltageEH_Checkbox_8axis.Enabled = false;
            this.Error_OvervoltageEH_Checkbox_8axis.Location = new System.Drawing.Point(6, 184);
            this.Error_OvervoltageEH_Checkbox_8axis.Name = "Error_OvervoltageEH_Checkbox_8axis";
            this.Error_OvervoltageEH_Checkbox_8axis.Size = new System.Drawing.Size(115, 17);
            this.Error_OvervoltageEH_Checkbox_8axis.TabIndex = 5;
            this.Error_OvervoltageEH_Checkbox_8axis.Text = "Over Voltage(E_H)";
            this.Error_OvervoltageEH_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvercurrentEH_Checkbox_8axis
            // 
            this.Error_OvercurrentEH_Checkbox_8axis.AutoSize = true;
            this.Error_OvercurrentEH_Checkbox_8axis.Enabled = false;
            this.Error_OvercurrentEH_Checkbox_8axis.Location = new System.Drawing.Point(6, 163);
            this.Error_OvercurrentEH_Checkbox_8axis.Name = "Error_OvercurrentEH_Checkbox_8axis";
            this.Error_OvercurrentEH_Checkbox_8axis.Size = new System.Drawing.Size(110, 17);
            this.Error_OvercurrentEH_Checkbox_8axis.TabIndex = 4;
            this.Error_OvercurrentEH_Checkbox_8axis.Text = "Over Current(E-H)";
            this.Error_OvercurrentEH_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_UndervoltageAD_Checkbox_8axis
            // 
            this.Error_UndervoltageAD_Checkbox_8axis.AutoSize = true;
            this.Error_UndervoltageAD_Checkbox_8axis.Enabled = false;
            this.Error_UndervoltageAD_Checkbox_8axis.Location = new System.Drawing.Point(6, 142);
            this.Error_UndervoltageAD_Checkbox_8axis.Name = "Error_UndervoltageAD_Checkbox_8axis";
            this.Error_UndervoltageAD_Checkbox_8axis.Size = new System.Drawing.Size(118, 17);
            this.Error_UndervoltageAD_Checkbox_8axis.TabIndex = 3;
            this.Error_UndervoltageAD_Checkbox_8axis.Text = "Under Voltage(A-D)";
            this.Error_UndervoltageAD_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvertemperetureAD_Checkbox_8axis
            // 
            this.Error_OvertemperetureAD_Checkbox_8axis.AutoSize = true;
            this.Error_OvertemperetureAD_Checkbox_8axis.Enabled = false;
            this.Error_OvertemperetureAD_Checkbox_8axis.Location = new System.Drawing.Point(6, 121);
            this.Error_OvertemperetureAD_Checkbox_8axis.Name = "Error_OvertemperetureAD_Checkbox_8axis";
            this.Error_OvertemperetureAD_Checkbox_8axis.Size = new System.Drawing.Size(130, 17);
            this.Error_OvertemperetureAD_Checkbox_8axis.TabIndex = 2;
            this.Error_OvertemperetureAD_Checkbox_8axis.Text = "Over Temperture(A-D)";
            this.Error_OvertemperetureAD_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvervoltageAD_Checkbox_8axis
            // 
            this.Error_OvervoltageAD_Checkbox_8axis.AutoSize = true;
            this.Error_OvervoltageAD_Checkbox_8axis.Enabled = false;
            this.Error_OvervoltageAD_Checkbox_8axis.Location = new System.Drawing.Point(6, 100);
            this.Error_OvervoltageAD_Checkbox_8axis.Name = "Error_OvervoltageAD_Checkbox_8axis";
            this.Error_OvervoltageAD_Checkbox_8axis.Size = new System.Drawing.Size(112, 17);
            this.Error_OvervoltageAD_Checkbox_8axis.TabIndex = 1;
            this.Error_OvervoltageAD_Checkbox_8axis.Text = "Over Voltage(A-D)";
            this.Error_OvervoltageAD_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvercurrentAD_Checkbox_8axis
            // 
            this.Error_OvercurrentAD_Checkbox_8axis.AutoSize = true;
            this.Error_OvercurrentAD_Checkbox_8axis.Enabled = false;
            this.Error_OvercurrentAD_Checkbox_8axis.Location = new System.Drawing.Point(6, 79);
            this.Error_OvercurrentAD_Checkbox_8axis.Name = "Error_OvercurrentAD_Checkbox_8axis";
            this.Error_OvercurrentAD_Checkbox_8axis.Size = new System.Drawing.Size(110, 17);
            this.Error_OvercurrentAD_Checkbox_8axis.TabIndex = 0;
            this.Error_OvercurrentAD_Checkbox_8axis.Text = "Over Current(A-D)";
            this.Error_OvercurrentAD_Checkbox_8axis.UseVisualStyleBackColor = true;
            // 
            // Error_Gbox_4axis
            // 
            this.Error_Gbox_4axis.Controls.Add(this.Error_ELO_Checkbox_4axis);
            this.Error_Gbox_4axis.Controls.Add(this.Error_Peakcurrent_Checkbox_4axis);
            this.Error_Gbox_4axis.Controls.Add(this.Error_Hall_Checkbox_4axis);
            this.Error_Gbox_4axis.Controls.Add(this.Error_UndervoltageAD_Checkbox_4axis);
            this.Error_Gbox_4axis.Controls.Add(this.Error_OvertemperetureAD_Checkbox_4axis);
            this.Error_Gbox_4axis.Controls.Add(this.Error_OvervoltageAD_Checkbox_4axis);
            this.Error_Gbox_4axis.Controls.Add(this.Error_OvercurrentAD_Checkbox_4axis);
            this.Error_Gbox_4axis.Location = new System.Drawing.Point(1207, 507);
            this.Error_Gbox_4axis.Name = "Error_Gbox_4axis";
            this.Error_Gbox_4axis.Size = new System.Drawing.Size(147, 251);
            this.Error_Gbox_4axis.TabIndex = 35;
            this.Error_Gbox_4axis.TabStop = false;
            this.Error_Gbox_4axis.Text = "4 Axis Errors";
            // 
            // Error_ELO_Checkbox_4axis
            // 
            this.Error_ELO_Checkbox_4axis.AutoSize = true;
            this.Error_ELO_Checkbox_4axis.Enabled = false;
            this.Error_ELO_Checkbox_4axis.Location = new System.Drawing.Point(6, 16);
            this.Error_ELO_Checkbox_4axis.Name = "Error_ELO_Checkbox_4axis";
            this.Error_ELO_Checkbox_4axis.Size = new System.Drawing.Size(47, 17);
            this.Error_ELO_Checkbox_4axis.TabIndex = 10;
            this.Error_ELO_Checkbox_4axis.Text = "ELO";
            this.Error_ELO_Checkbox_4axis.UseVisualStyleBackColor = true;
            // 
            // Error_Peakcurrent_Checkbox_4axis
            // 
            this.Error_Peakcurrent_Checkbox_4axis.AutoSize = true;
            this.Error_Peakcurrent_Checkbox_4axis.Enabled = false;
            this.Error_Peakcurrent_Checkbox_4axis.Location = new System.Drawing.Point(6, 37);
            this.Error_Peakcurrent_Checkbox_4axis.Name = "Error_Peakcurrent_Checkbox_4axis";
            this.Error_Peakcurrent_Checkbox_4axis.Size = new System.Drawing.Size(88, 17);
            this.Error_Peakcurrent_Checkbox_4axis.TabIndex = 9;
            this.Error_Peakcurrent_Checkbox_4axis.Text = "Peak Current";
            this.Error_Peakcurrent_Checkbox_4axis.UseVisualStyleBackColor = true;
            // 
            // Error_Hall_Checkbox_4axis
            // 
            this.Error_Hall_Checkbox_4axis.AutoSize = true;
            this.Error_Hall_Checkbox_4axis.Enabled = false;
            this.Error_Hall_Checkbox_4axis.Location = new System.Drawing.Point(6, 58);
            this.Error_Hall_Checkbox_4axis.Name = "Error_Hall_Checkbox_4axis";
            this.Error_Hall_Checkbox_4axis.Size = new System.Drawing.Size(69, 17);
            this.Error_Hall_Checkbox_4axis.TabIndex = 8;
            this.Error_Hall_Checkbox_4axis.Text = "Hall Error";
            this.Error_Hall_Checkbox_4axis.UseVisualStyleBackColor = true;
            // 
            // Error_UndervoltageAD_Checkbox_4axis
            // 
            this.Error_UndervoltageAD_Checkbox_4axis.AutoSize = true;
            this.Error_UndervoltageAD_Checkbox_4axis.Enabled = false;
            this.Error_UndervoltageAD_Checkbox_4axis.Location = new System.Drawing.Point(6, 142);
            this.Error_UndervoltageAD_Checkbox_4axis.Name = "Error_UndervoltageAD_Checkbox_4axis";
            this.Error_UndervoltageAD_Checkbox_4axis.Size = new System.Drawing.Size(118, 17);
            this.Error_UndervoltageAD_Checkbox_4axis.TabIndex = 3;
            this.Error_UndervoltageAD_Checkbox_4axis.Text = "Under Voltage(A-D)";
            this.Error_UndervoltageAD_Checkbox_4axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvertemperetureAD_Checkbox_4axis
            // 
            this.Error_OvertemperetureAD_Checkbox_4axis.AutoSize = true;
            this.Error_OvertemperetureAD_Checkbox_4axis.Enabled = false;
            this.Error_OvertemperetureAD_Checkbox_4axis.Location = new System.Drawing.Point(6, 121);
            this.Error_OvertemperetureAD_Checkbox_4axis.Name = "Error_OvertemperetureAD_Checkbox_4axis";
            this.Error_OvertemperetureAD_Checkbox_4axis.Size = new System.Drawing.Size(130, 17);
            this.Error_OvertemperetureAD_Checkbox_4axis.TabIndex = 2;
            this.Error_OvertemperetureAD_Checkbox_4axis.Text = "Over Temperture(A-D)";
            this.Error_OvertemperetureAD_Checkbox_4axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvervoltageAD_Checkbox_4axis
            // 
            this.Error_OvervoltageAD_Checkbox_4axis.AutoSize = true;
            this.Error_OvervoltageAD_Checkbox_4axis.Enabled = false;
            this.Error_OvervoltageAD_Checkbox_4axis.Location = new System.Drawing.Point(6, 100);
            this.Error_OvervoltageAD_Checkbox_4axis.Name = "Error_OvervoltageAD_Checkbox_4axis";
            this.Error_OvervoltageAD_Checkbox_4axis.Size = new System.Drawing.Size(112, 17);
            this.Error_OvervoltageAD_Checkbox_4axis.TabIndex = 1;
            this.Error_OvervoltageAD_Checkbox_4axis.Text = "Over Voltage(A-D)";
            this.Error_OvervoltageAD_Checkbox_4axis.UseVisualStyleBackColor = true;
            // 
            // Error_OvercurrentAD_Checkbox_4axis
            // 
            this.Error_OvercurrentAD_Checkbox_4axis.AutoSize = true;
            this.Error_OvercurrentAD_Checkbox_4axis.Enabled = false;
            this.Error_OvercurrentAD_Checkbox_4axis.Location = new System.Drawing.Point(6, 79);
            this.Error_OvercurrentAD_Checkbox_4axis.Name = "Error_OvercurrentAD_Checkbox_4axis";
            this.Error_OvercurrentAD_Checkbox_4axis.Size = new System.Drawing.Size(110, 17);
            this.Error_OvercurrentAD_Checkbox_4axis.TabIndex = 0;
            this.Error_OvercurrentAD_Checkbox_4axis.Text = "Over Current(A-D)";
            this.Error_OvercurrentAD_Checkbox_4axis.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 762);
            this.Controls.Add(this.Error_Gbox_4axis);
            this.Controls.Add(this.Error_Gbox_8axis);
            this.Controls.Add(this.LA_T_Tbox);
            this.Controls.Add(this.LK_T_Tbox);
            this.Controls.Add(this.LHR_T_Tbox);
            this.Controls.Add(this.LHC_T_Tbox);
            this.Controls.Add(this.LHF_T_Tbox);
            this.Controls.Add(this.RA_T_Tbox);
            this.Controls.Add(this.RK_T_Tbox);
            this.Controls.Add(this.RHR_T_Tbox);
            this.Controls.Add(this.RHC_T_Tbox);
            this.Controls.Add(this.RHF_T_Tbox);
            this.Controls.Add(this.Reset_All_Button);
            this.Controls.Add(this.Stop_ALL_Button);
            this.Controls.Add(this.Refresh_Button);
            this.Controls.Add(this.LA_A_Tbox);
            this.Controls.Add(this.LK_A_Tbox);
            this.Controls.Add(this.LHR_A_Tbox);
            this.Controls.Add(this.LHC_A_Tbox);
            this.Controls.Add(this.LHF_A_Tbox);
            this.Controls.Add(this.RA_A_Tbox);
            this.Controls.Add(this.RK_A_Tbox);
            this.Controls.Add(this.RHR_A_Tbox);
            this.Controls.Add(this.RHC_A_Tbox);
            this.Controls.Add(this.RHF_A_Tbox);
            this.Controls.Add(this.Clear_Button);
            this.Controls.Add(this.Main_Tab);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Galil Tester";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Main_Tab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.LAnkle_Gbox.ResumeLayout(false);
            this.LAnkle_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LAnkle_Goto_Numeric)).EndInit();
            this.LHRear_Gbox.ResumeLayout(false);
            this.LHRear_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LHRear_Goto_Numeric)).EndInit();
            this.LHFront_Gbox.ResumeLayout(false);
            this.LHFront_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LHFront_Goto_Numeric)).EndInit();
            this.LHCenter_Gbox.ResumeLayout(false);
            this.LHCenter_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LHCenter_Goto_Numeric)).EndInit();
            this.LKnee_Gbox.ResumeLayout(false);
            this.LKnee_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LKnee_Goto_Numeric)).EndInit();
            this.RAnkle_Gbox.ResumeLayout(false);
            this.RAnkle_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RAnkle_Goto_Numeric)).EndInit();
            this.RHRear_Gbox.ResumeLayout(false);
            this.RHRear_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RHRear_Goto_Numeric)).EndInit();
            this.RHFront_Gbox.ResumeLayout(false);
            this.RHFront_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RHFront_Goto_Numeric)).EndInit();
            this.RHCenter_Gbox.ResumeLayout(false);
            this.RHCenter_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RHCenter_Goto_Numeric)).EndInit();
            this.Terminal_Gbox.ResumeLayout(false);
            this.Terminal_Gbox.PerformLayout();
            this.RKnee_Gbox.ResumeLayout(false);
            this.RKnee_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RKnee_Goto_Numeric)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.Motion_ModeGbox.ResumeLayout(false);
            this.Motion_ModeGbox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.PVT_Record_Gbox.ResumeLayout(false);
            this.PVT_Record_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PVT_Record_Rate_Numeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PVT_Pause_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PVT_Record_Pbox)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Start_Position_Gbox.ResumeLayout(false);
            this.Start_Position_Gbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LA_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RK_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RA_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RHF_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RHR_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RK_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_RHC_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RA_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RHF_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LHC_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LK_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LHR_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tick_LHF_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LA_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LK_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LHR_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LHC_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_LHF_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RHC_Pbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Warning_RHR_Pbox)).EndInit();
            this.Error_Gbox_8axis.ResumeLayout(false);
            this.Error_Gbox_8axis.PerformLayout();
            this.Error_Gbox_4axis.ResumeLayout(false);
            this.Error_Gbox_4axis.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl Main_Tab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Disconnect_Button_4axis_Biss;
        private System.Windows.Forms.Label Connection_Status_Label_4axis_Biss;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Connect_Button_4axis_Biss;
        private System.Windows.Forms.Button Disconnect_Button_8axis;
        private System.Windows.Forms.Label Connection_Status_Label_8axis;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Connect_Button_8axis;
        private System.Windows.Forms.Button Disconnect_All_Button;
        private System.Windows.Forms.Button Connect_All_Button;
        private System.Windows.Forms.Button Disconnect_Button_4axis_Motor;
        private System.Windows.Forms.Label Connection_Status_Label_4axis_Motor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Connect_Button_4axis_Motor;
        private System.Windows.Forms.Timer Basetick;
        private System.Windows.Forms.GroupBox RKnee_Gbox;
        private System.Windows.Forms.Button RKnee_Init_Button;
        private System.Windows.Forms.Button RKnee_Stop_Button;
        private System.Windows.Forms.NumericUpDown RKnee_Goto_Numeric;
        private System.Windows.Forms.Button RKnee_Goto_Button;
        private System.Windows.Forms.Button RKnee_Neutral_Button;
        private System.Windows.Forms.TextBox RKnee_Tbox;
        private System.Windows.Forms.Button Clear_Button;
        private System.Windows.Forms.GroupBox Terminal_Gbox;
        private System.Windows.Forms.ComboBox Terminal_Device_CBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Terminal_Command_CBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Terminal_Tbox;
        private System.Windows.Forms.Button Terminal_Send_Button;
        private System.Windows.Forms.GroupBox LAnkle_Gbox;
        private System.Windows.Forms.TextBox LAnkle_Tbox;
        private System.Windows.Forms.Button LAnkle_Stop_Button;
        private System.Windows.Forms.NumericUpDown LAnkle_Goto_Numeric;
        private System.Windows.Forms.Button LAnkle_Goto_Button;
        private System.Windows.Forms.Button LAnkle_Neutral_Button;
        private System.Windows.Forms.Button LAnkle_Init_Button;
        private System.Windows.Forms.GroupBox LHRear_Gbox;
        private System.Windows.Forms.TextBox LHRear_Tbox;
        private System.Windows.Forms.Button LHRear_Stop_Button;
        private System.Windows.Forms.NumericUpDown LHRear_Goto_Numeric;
        private System.Windows.Forms.Button LHRear_Goto_Button;
        private System.Windows.Forms.Button LHRear_Neutral_Button;
        private System.Windows.Forms.Button LHRear_Init_Button;
        private System.Windows.Forms.GroupBox LHFront_Gbox;
        private System.Windows.Forms.TextBox LHFront_Tbox;
        private System.Windows.Forms.Button LHFront_Stop_Button;
        private System.Windows.Forms.NumericUpDown LHFront_Goto_Numeric;
        private System.Windows.Forms.Button LHFront_Goto_Button;
        private System.Windows.Forms.Button LHFront_Neutral_Button;
        private System.Windows.Forms.Button LHFront_Init_Button;
        private System.Windows.Forms.GroupBox LHCenter_Gbox;
        private System.Windows.Forms.TextBox LHCenter_Tbox;
        private System.Windows.Forms.Button LHCenter_Stop_Button;
        private System.Windows.Forms.NumericUpDown LHCenter_Goto_Numeric;
        private System.Windows.Forms.Button LHCenter_Goto_Button;
        private System.Windows.Forms.Button LHCenter_Neutral_Button;
        private System.Windows.Forms.Button LHCenter_Init_Button;
        private System.Windows.Forms.GroupBox LKnee_Gbox;
        private System.Windows.Forms.TextBox LKnee_Tbox;
        private System.Windows.Forms.Button LKnee_Stop_Button;
        private System.Windows.Forms.NumericUpDown LKnee_Goto_Numeric;
        private System.Windows.Forms.Button LKnee_Goto_Button;
        private System.Windows.Forms.Button LKnee_Neutral_Button;
        private System.Windows.Forms.Button LKnee_Init_Button;
        private System.Windows.Forms.GroupBox RAnkle_Gbox;
        private System.Windows.Forms.TextBox RAnkle_Tbox;
        private System.Windows.Forms.Button RAnkle_Stop_Button;
        private System.Windows.Forms.NumericUpDown RAnkle_Goto_Numeric;
        private System.Windows.Forms.Button RAnkle_Goto_Button;
        private System.Windows.Forms.Button RAnkle_Neutral_Button;
        private System.Windows.Forms.Button RAnkle_Init_Button;
        private System.Windows.Forms.GroupBox RHRear_Gbox;
        private System.Windows.Forms.TextBox RHRear_Tbox;
        private System.Windows.Forms.Button RHRear_Stop_Button;
        private System.Windows.Forms.NumericUpDown RHRear_Goto_Numeric;
        private System.Windows.Forms.Button RHRear_Goto_Button;
        private System.Windows.Forms.Button RHRear_Neutral_Button;
        private System.Windows.Forms.Button RHRear_Init_Button;
        private System.Windows.Forms.GroupBox RHFront_Gbox;
        private System.Windows.Forms.TextBox RHFront_Tbox;
        private System.Windows.Forms.Button RHFront_Stop_Button;
        private System.Windows.Forms.NumericUpDown RHFront_Goto_Numeric;
        private System.Windows.Forms.Button RHFront_Goto_Button;
        private System.Windows.Forms.Button RHFront_Neutral_Button;
        private System.Windows.Forms.Button RHFront_Init_Button;
        private System.Windows.Forms.GroupBox RHCenter_Gbox;
        private System.Windows.Forms.TextBox RHCenter_Tbox;
        private System.Windows.Forms.Button RHCenter_Stop_Button;
        private System.Windows.Forms.NumericUpDown RHCenter_Goto_Numeric;
        private System.Windows.Forms.Button RHCenter_Goto_Button;
        private System.Windows.Forms.Button RHCenter_Neutral_Button;
        private System.Windows.Forms.Button RHCenter_Init_Button;
        private System.Windows.Forms.GroupBox Start_Position_Gbox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Stop_ALL_Button;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button PVT_Run_Button;
        private System.Windows.Forms.Button PVT_Download_Button;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox PVT_Filepath_Tbox_4axis;
        private System.Windows.Forms.Button PVT_Browse_Button_4axis;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox PVT_Filepath_Tbox_8axis;
        private System.Windows.Forms.Button PVT_Browse_Button_8axis;
        private System.Windows.Forms.Button ST_Go_Button;
        private System.Windows.Forms.TextBox ST_RA_Tbox;
        private System.Windows.Forms.TextBox ST_RK_Tbox;
        private System.Windows.Forms.TextBox ST_RHR_Tbox;
        private System.Windows.Forms.TextBox ST_RHC_Tbox;
        private System.Windows.Forms.TextBox ST_RHF_Tbox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox ST_LA_Tbox;
        private System.Windows.Forms.TextBox ST_LK_Tbox;
        private System.Windows.Forms.TextBox ST_LHR_Tbox;
        private System.Windows.Forms.TextBox ST_LHC_Tbox;
        private System.Windows.Forms.TextBox ST_LHF_Tbox;
        private System.Windows.Forms.TextBox RHF_A_Tbox;
        private System.Windows.Forms.TextBox RHC_A_Tbox;
        private System.Windows.Forms.TextBox RHR_A_Tbox;
        private System.Windows.Forms.TextBox RK_A_Tbox;
        private System.Windows.Forms.TextBox RA_A_Tbox;
        private System.Windows.Forms.TextBox LHF_A_Tbox;
        private System.Windows.Forms.TextBox LHC_A_Tbox;
        private System.Windows.Forms.TextBox LHR_A_Tbox;
        private System.Windows.Forms.TextBox LK_A_Tbox;
        private System.Windows.Forms.TextBox LA_A_Tbox;
        private System.Windows.Forms.Button Refresh_Button;
        private System.Windows.Forms.Button Reset_All_Button;
        private System.Windows.Forms.GroupBox Motion_ModeGbox;
        private System.Windows.Forms.RadioButton NWK_Radiobutton;
        private System.Windows.Forms.RadioButton GWK_Radiobutton;
        private System.Windows.Forms.TextBox LA_T_Tbox;
        private System.Windows.Forms.TextBox LK_T_Tbox;
        private System.Windows.Forms.TextBox LHR_T_Tbox;
        private System.Windows.Forms.TextBox LHC_T_Tbox;
        private System.Windows.Forms.TextBox LHF_T_Tbox;
        private System.Windows.Forms.TextBox RA_T_Tbox;
        private System.Windows.Forms.TextBox RK_T_Tbox;
        private System.Windows.Forms.TextBox RHR_T_Tbox;
        private System.Windows.Forms.TextBox RHC_T_Tbox;
        private System.Windows.Forms.TextBox RHF_T_Tbox;
        private System.Windows.Forms.PictureBox Tick_LA_Pbox;
        private System.Windows.Forms.PictureBox Tick_RK_Pbox;
        private System.Windows.Forms.PictureBox Tick_RA_Pbox;
        private System.Windows.Forms.PictureBox Tick_RHF_Pbox;
        private System.Windows.Forms.PictureBox Tick_RHR_Pbox;
        private System.Windows.Forms.PictureBox Warning_RK_Pbox;
        private System.Windows.Forms.PictureBox Tick_RHC_Pbox;
        private System.Windows.Forms.PictureBox Warning_RA_Pbox;
        private System.Windows.Forms.PictureBox Warning_RHF_Pbox;
        private System.Windows.Forms.PictureBox Tick_LHC_Pbox;
        private System.Windows.Forms.PictureBox Tick_LK_Pbox;
        private System.Windows.Forms.PictureBox Tick_LHR_Pbox;
        private System.Windows.Forms.Button ST_Check_Button;
        private System.Windows.Forms.PictureBox Tick_LHF_Pbox;
        private System.Windows.Forms.PictureBox Warning_LA_Pbox;
        private System.Windows.Forms.PictureBox Warning_LK_Pbox;
        private System.Windows.Forms.PictureBox Warning_LHR_Pbox;
        private System.Windows.Forms.PictureBox Warning_LHC_Pbox;
        private System.Windows.Forms.PictureBox Warning_LHF_Pbox;
        private System.Windows.Forms.PictureBox Warning_RHC_Pbox;
        private System.Windows.Forms.PictureBox Warning_RHR_Pbox;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox PVT_Pause_Pbox;
        private System.Windows.Forms.PictureBox PVT_Record_Pbox;
        private System.Windows.Forms.TextBox Record_Len_Tbox;
        private System.Windows.Forms.Button PVT_Save_Button;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_LA;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_LK;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_LHR;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_LHC;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_LHF;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_RA;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_RK;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_RHR;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_RHC;
        private System.Windows.Forms.TextBox PVT_Executed_Tbox_RHF;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_LA;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_LK;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_LHR;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_LHC;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_LHF;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_RA;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_RK;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_RHR;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_RHC;
        private System.Windows.Forms.TextBox PVT_Space_Tbox_RHF;
        private System.Windows.Forms.Button PVT_Refresh_Button;
        private System.Windows.Forms.GroupBox Error_Gbox_8axis;
        private System.Windows.Forms.CheckBox Error_ELO_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_Peakcurrent_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_Hall_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_UndervoltageEH_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_OvertemperetureEH_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_OvervoltageEH_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_OvercurrentEH_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_UndervoltageAD_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_OvertemperetureAD_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_OvervoltageAD_Checkbox_8axis;
        private System.Windows.Forms.CheckBox Error_OvercurrentAD_Checkbox_8axis;
        private System.Windows.Forms.GroupBox Error_Gbox_4axis;
        private System.Windows.Forms.CheckBox Error_ELO_Checkbox_4axis;
        private System.Windows.Forms.CheckBox Error_Peakcurrent_Checkbox_4axis;
        private System.Windows.Forms.CheckBox Error_Hall_Checkbox_4axis;
        private System.Windows.Forms.CheckBox Error_UndervoltageAD_Checkbox_4axis;
        private System.Windows.Forms.CheckBox Error_OvertemperetureAD_Checkbox_4axis;
        private System.Windows.Forms.CheckBox Error_OvervoltageAD_Checkbox_4axis;
        private System.Windows.Forms.CheckBox Error_OvercurrentAD_Checkbox_4axis;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox PVT_Filepath_Tbox_Startposition;
        private System.Windows.Forms.Button PVT_Browse_Button_Start_Position;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox STB_Tbox;
        private System.Windows.Forms.Button STB_Button;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox STE_Tbox;
        private System.Windows.Forms.Button STE_Button;
        private System.Windows.Forms.Button Loop_Button;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox PVT_Record_Gbox;
        private System.Windows.Forms.CheckBox Record_Enable_Checkbox;
        private System.Windows.Forms.RadioButton Record_Velocity_Radiobutton;
        private System.Windows.Forms.RadioButton Record_Torque_Radiobutton;
        private System.Windows.Forms.RadioButton Record_Angle_Radiobutton;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown PVT_Record_Rate_Numeric;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ProgressBar Motion_Progress_Bar;
        private System.Windows.Forms.Label Motion_Progress_Label;
    }
}

