﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Galil_Integration
{
    public static class Galil_Programs
    {
        public static Exo_Motor Exo_Right_Knee = new Exo_Motor();
        public static Exo_Motor Exo_Right_Ankle = new Exo_Motor();
        public static Exo_Motor Exo_Right_Hip_Center = new Exo_Motor();
        public static Exo_Motor Exo_Right_Hip_Front = new Exo_Motor();
        public static Exo_Motor Exo_Right_Hip_Rear = new Exo_Motor();
        public static Exo_Motor Exo_Left_Knee = new Exo_Motor();
        public static Exo_Motor Exo_Left_Ankle = new Exo_Motor();
        public static Exo_Motor Exo_Left_Hip_Center = new Exo_Motor();
        public static Exo_Motor Exo_Left_Hip_Front = new Exo_Motor();
        public static Exo_Motor Exo_Left_Hip_Rear = new Exo_Motor();

        public static void Exo_Definition()
        {
            // define Right knee
            Exo_Right_Knee.Main_Controller_ID = "8axis";
            Exo_Right_Knee.Main_Axis_ID = "A";
            Exo_Right_Knee.Biss_Controller_ID = "8axis";
            Exo_Right_Knee.Biss_Axis_ID = "A";
            Exo_Right_Knee.ABS_Encoder = -1;
            Exo_Right_Knee.Gear_Head_Ratio = 14.0625;
            Exo_Right_Knee.Nuetral_Angle = 0;

            // define Left knee
            Exo_Left_Knee.Main_Controller_ID = "8axis";
            Exo_Left_Knee.Main_Axis_ID = "E";
            Exo_Left_Knee.Biss_Controller_ID = "8axis";
            Exo_Left_Knee.Biss_Axis_ID = "E";
            Exo_Left_Knee.ABS_Encoder = -1;
            Exo_Left_Knee.Gear_Head_Ratio = 14.0625;
            Exo_Left_Knee.Nuetral_Angle = 0;

            // define Right Hip center
            Exo_Right_Hip_Center.Main_Controller_ID = "4axis Motor";
            Exo_Right_Hip_Center.Main_Axis_ID = "C";
            Exo_Right_Hip_Center.Biss_Controller_ID = "4axis Biss";
            Exo_Right_Hip_Center.Biss_Axis_ID = "A";
            Exo_Right_Hip_Center.ABS_Encoder = -1;
            Exo_Right_Hip_Center.Gear_Head_Ratio = 22.5;
            Exo_Right_Hip_Center.Nuetral_Angle = 0;

            // define Right Hip front
            Exo_Right_Hip_Front.Main_Controller_ID = "8axis";
            Exo_Right_Hip_Front.Main_Axis_ID = "C";
            Exo_Right_Hip_Front.Biss_Controller_ID = "8axis";
            Exo_Right_Hip_Front.Biss_Axis_ID = "C";
            Exo_Right_Hip_Front.ABS_Encoder = -1;
            Exo_Right_Hip_Front.Gear_Head_Ratio = 22.5;
            Exo_Right_Hip_Front.Nuetral_Angle = 0;

            // define Right Hip rear
            Exo_Right_Hip_Rear.Main_Controller_ID = "8axis";
            Exo_Right_Hip_Rear.Main_Axis_ID = "D";
            Exo_Right_Hip_Rear.Biss_Controller_ID = "8axis";
            Exo_Right_Hip_Rear.Biss_Axis_ID = "D";
            Exo_Right_Hip_Rear.ABS_Encoder = -1;
            Exo_Right_Hip_Rear.Gear_Head_Ratio = 22.5;
            Exo_Right_Hip_Rear.Nuetral_Angle = 0;

            // define Left Hip center
            Exo_Left_Hip_Center.Main_Controller_ID = "4axis Motor";
            Exo_Left_Hip_Center.Main_Axis_ID = "B";
            Exo_Left_Hip_Center.Biss_Controller_ID = "4axis Biss";
            Exo_Left_Hip_Center.Biss_Axis_ID = "B";
            Exo_Left_Hip_Center.ABS_Encoder = -1;
            Exo_Left_Hip_Center.Gear_Head_Ratio = 22.5;
            Exo_Left_Hip_Center.Nuetral_Angle = 0;

            // define Left Hip front
            Exo_Left_Hip_Front.Main_Controller_ID = "8axis";
            Exo_Left_Hip_Front.Main_Axis_ID = "G";
            Exo_Left_Hip_Front.Biss_Controller_ID = "8axis";
            Exo_Left_Hip_Front.Biss_Axis_ID = "G";
            Exo_Left_Hip_Front.ABS_Encoder = -1;
            Exo_Left_Hip_Front.Gear_Head_Ratio = 22.5;
            Exo_Left_Hip_Front.Nuetral_Angle = 0;

            // define Left Hip rear
            Exo_Left_Hip_Rear.Main_Controller_ID = "8axis";
            Exo_Left_Hip_Rear.Main_Axis_ID = "H";
            Exo_Left_Hip_Rear.Biss_Controller_ID = "8axis";
            Exo_Left_Hip_Rear.Biss_Axis_ID = "H";
            Exo_Left_Hip_Rear.ABS_Encoder = -1;
            Exo_Left_Hip_Rear.Gear_Head_Ratio = 22.5;
            Exo_Left_Hip_Rear.Nuetral_Angle = 0;

            // define Right Ankle
            Exo_Right_Ankle.Main_Controller_ID = "8axis";
            Exo_Right_Ankle.Main_Axis_ID = "B";
            Exo_Right_Ankle.Biss_Controller_ID = "8axis";
            Exo_Right_Ankle.Biss_Axis_ID = "B";
            Exo_Right_Ankle.ABS_Encoder = -1;
            Exo_Right_Ankle.Gear_Head_Ratio = 22.5;
            Exo_Right_Ankle.Nuetral_Angle = 0;

            // define Left Ankle
            Exo_Left_Ankle.Main_Controller_ID = "8axis";
            Exo_Left_Ankle.Main_Axis_ID = "F";
            Exo_Left_Ankle.Biss_Controller_ID = "8axis";
            Exo_Left_Ankle.Biss_Axis_ID = "F";
            Exo_Left_Ankle.ABS_Encoder = -1;
            Exo_Left_Ankle.Gear_Head_Ratio = 22.5;
            Exo_Left_Ankle.Nuetral_Angle = 0;

            //define offsets
            Exo_Right_Knee.ABS_Encoder_Offset = 15593;
            Exo_Right_Hip_Center.ABS_Encoder_Offset = 5973;
            Exo_Right_Hip_Front.ABS_Encoder_Offset = 9292;
            Exo_Right_Hip_Rear.ABS_Encoder_Offset = 15655;
            Exo_Right_Ankle.ABS_Encoder_Offset = 14385;



            Exo_Left_Knee.ABS_Encoder_Offset = 16044;
            Exo_Left_Hip_Center.ABS_Encoder_Offset = 8484;
            Exo_Left_Hip_Front.ABS_Encoder_Offset = 9341;
            Exo_Left_Hip_Rear.ABS_Encoder_Offset = 14327;
            Exo_Left_Ankle.ABS_Encoder_Offset = 128;


            // define start position for generated walking
            Exo_Left_Hip_Front.PVT_Start_Angle = 15.779368;
            Exo_Left_Hip_Rear.PVT_Start_Angle = -2.337113;
            Exo_Left_Hip_Center.PVT_Start_Angle = 11.201407;
            Exo_Left_Knee.PVT_Start_Angle = 47.803014;
            Exo_Left_Ankle.PVT_Start_Angle = -17.377819;


            Exo_Right_Hip_Front.PVT_Start_Angle = -15.777391;
            Exo_Right_Hip_Rear.PVT_Start_Angle = 2.334764;
            Exo_Right_Hip_Center.PVT_Start_Angle = -11.202980;
            Exo_Right_Knee.PVT_Start_Angle = -47.805509;
            Exo_Right_Ankle.PVT_Start_Angle = 17.378962;



            /*   // define start position for natural walking
               Exo_Left_Hip_Front.Natural_Walking_Start_Angle = 13.940458;
               Exo_Left_Hip_Rear.Natural_Walking_Start_Angle = -1.663068;
               Exo_Left_Hip_Center.Natural_Walking_Start_Angle = 9.168627;
               Exo_Left_Knee.Natural_Walking_Start_Angle = 7.396744;
               Exo_Left_Ankle.Natural_Walking_Start_Angle = 18.151750;


               Exo_Right_Hip_Front.Natural_Walking_Start_Angle = 1.525901;
               Exo_Right_Hip_Rear.Natural_Walking_Start_Angle = 9.549143;
               Exo_Right_Hip_Center.Natural_Walking_Start_Angle = -12.332014;
               Exo_Right_Knee.Natural_Walking_Start_Angle = -16.007780;
               Exo_Right_Ankle.Natural_Walking_Start_Angle = 23.705274;*/

        }

        public static string Init_Motor(Exo_Motor inp)
        {
            string Command;
            string Device;
            string Response;
            Device = inp.Main_Controller_ID;
            // AUx =0 
            Command = "AU" + inp.Main_Axis_ID + " =0 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in AU");
                return "fail";
            }
            //  CEx = 2 
            Command = "CE" + inp.Main_Axis_ID + " =2 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in CE");
                return "fail";
            }

            //  BAx 
            Command = "BA " + inp.Main_Axis_ID + "\r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in BA");
                return "fail";
            }

            //  BMx = 230.4 
            Command = "BM" + inp.Main_Axis_ID + " =230.4 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in BM");
                return "fail";
            }
            //  ERx = 230.4 
            Command = "ER" + inp.Main_Axis_ID + " =230.4 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in BM");
                return "fail";
            }

            //  Bzx = 3 
            Command = "BZ" + inp.Main_Axis_ID + " =3 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in BZ");
                Command = "TC1 \r\n";
                return Galil_Interface.Transeive_Galil(Command, Device); ;
            }

            //  KIx = 4 
            Command = "KI" + inp.Main_Axis_ID + " =4 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in KI");
                return "fail";
            }

            //  KPx = 234 
            Command = "KP" + inp.Main_Axis_ID + " =234 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in KP");
                return "fail";
            }

            //  KDx = 2480 
            Command = "KD" + inp.Main_Axis_ID + " =2480 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in KD");
                return "fail";
            }

            //  SYx = 0 
            Command = "SY" + inp.Biss_Axis_ID + " =0 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, inp.Biss_Controller_ID);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in SY");
                return "fail";
            }

            //  SSx = 2,15,15,0 <9 
            Command = "SS" + inp.Biss_Axis_ID + " = 2,15,15,0 < 9 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, inp.Biss_Controller_ID);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in SS");
                return "fail";
            }

            //  TLx = 5 
            Command = "TL" + inp.Main_Axis_ID + " =9.9 \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in TL");
                return "fail";
            }

            //  SHx  
            Command = "SH" + inp.Main_Axis_ID + " \r\n";
            Response = Galil_Interface.Transeive_Galil(Command, Device);
            if ((Response != ":") && (Response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in SH");
                return "fail";
            }

            return "Succeed";
        }
        private static int Encoder_response_reader(string inp)
        {
            int a = -1;
            string firstline = inp.Substring(0, inp.IndexOf(Environment.NewLine));
            string secondline = inp.Substring(inp.IndexOf(Environment.NewLine) + 2);
            try
            {
                a = int.Parse(firstline);
            }
            catch
            {
                MessageBox.Show("Error!!! Encoder value should be a number");
            }

            if (secondline != ":")
            {
                MessageBox.Show("Failed");
            }
            return a;
        }

        public static string Motor_Goto_Angle(Exo_Motor inp, double angle)
        {
            // normalize angle to +-360
            int cof = 1;
            if ((angle > 360) || (angle < -360))
            {
                angle = 0;
            }
            double pos = inp.ABS_Encoder_Offset - (16384.0 * angle / 360);
            if (pos > 16383)
            {
                pos -= 16383;
                cof = -1;
            }
            // read absolote encoder
            string command = "TD" + inp.Biss_Axis_ID + "\r\n";
            string device = inp.Biss_Controller_ID;
            string response = Galil_Interface.Transeive_Galil(command, device);
            int abs_en = Encoder_response_reader(response);
            double delta = abs_en - pos;
            double delta_abs = delta;
            if (delta_abs < 0)
            {
                delta_abs = -delta_abs;
            }
            device = inp.Main_Controller_ID;
            // DPx = 0
            command = "DP" + inp.Main_Axis_ID + " = 0 \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in DP");
                return "fail";
            }

            // SPx = 4000
            command = "SP" + inp.Main_Axis_ID + " = 4000 \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in SP");
                return "fail";
            }

            if (delta_abs < 16384 - delta_abs)
            {
                //PAx =  22.5 * delta
                string ss = (inp.Gear_Head_Ratio * delta).ToString();
                command = "PA" + inp.Main_Axis_ID + " =" + ss + "\r\n";
                response = Galil_Interface.Transeive_Galil(command, device);
                if ((response != ":") && (response != "\r\n:"))
                {
                    MessageBox.Show("fail response from galil Controller in PA");
                    return "fail";
                }

                //BGx
                command = "BG" + inp.Main_Axis_ID + "\r\n";
                response = Galil_Interface.Transeive_Galil(command, device);
                if ((response != ":") && (response != "\r\n:"))
                {
                    MessageBox.Show("fail response from galil Controller in BG");
                    return "fail";
                }

            }
            else
            {
                //PAx = 22.5 * (16384 - @ABS[delta])
                string ss = (inp.Gear_Head_Ratio * cof * (16384 - delta_abs)).ToString();
                //              string ss = (22.5 * (delta_abs-16384)).ToString();
                command = "PA" + inp.Main_Axis_ID + " =" + ss + "\r\n";
                response = Galil_Interface.Transeive_Galil(command, device);
                if ((response != ":") && (response != "\r\n:"))
                {
                    MessageBox.Show("fail response from galil Controller in PA");
                    return "fail";
                }
                //BGx
                command = "BG" + inp.Main_Axis_ID + "\r\n";
                response = Galil_Interface.Transeive_Galil(command, device);
                if ((response != ":") && (response != "\r\n:"))
                {
                    MessageBox.Show("fail response from galil Controller in BG");
                    return "fail";
                }
            }
            return "Succeed";
        }

        public static void Refresh_Angles()
        {
            //send TD command to 8 axis
            string command = "TD \r\n";
            string response = Galil_Interface.Transeive_Galil(command, "8axis");
            string value_string_8axis = response.Substring(0, response.IndexOf(Environment.NewLine));
            string secondline = response.Substring(response.IndexOf(Environment.NewLine) + 2);
            if ((secondline != ":") && (secondline != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in TD 8axis");
                return;
            }

            //send TD command to 4 axis
            response = Galil_Interface.Transeive_Galil(command, "4axis Biss");
            string value_string_4axis = response.Substring(0, response.IndexOf(Environment.NewLine));
            secondline = response.Substring(response.IndexOf(Environment.NewLine) + 2);
            if ((secondline != ":") && (secondline != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in TD 4axis biss");
                return;
            }
            //extract numbers
            int[] nums_8axis = Array.ConvertAll(value_string_8axis.Split(','), int.Parse);
            int[] nums_4axis = Array.ConvertAll(value_string_4axis.Split(','), int.Parse);
            Exo_Right_Knee.ABS_Encoder = nums_8axis[0];
            Exo_Right_Ankle.ABS_Encoder = nums_8axis[1];
            Exo_Right_Hip_Front.ABS_Encoder = nums_8axis[2];
            Exo_Right_Hip_Rear.ABS_Encoder = nums_8axis[3];
            Exo_Left_Knee.ABS_Encoder = nums_8axis[4];
            Exo_Left_Ankle.ABS_Encoder = nums_8axis[5];
            Exo_Left_Hip_Front.ABS_Encoder = nums_8axis[6];
            Exo_Left_Hip_Rear.ABS_Encoder = nums_8axis[7];

            Exo_Right_Hip_Center.ABS_Encoder = nums_4axis[0];
            Exo_Left_Hip_Center.ABS_Encoder = nums_4axis[1];
        }

        public static void Refresh_Torques()
        {
            //send TT command to 8 axis
            string command = "TT \r\n";
            string response = Galil_Interface.Transeive_Galil(command, "8axis");
            string value_string_8axis = response.Substring(0, response.IndexOf(Environment.NewLine));
            string secondline = response.Substring(response.IndexOf(Environment.NewLine) + 2);
            if ((secondline != ":") && (secondline != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in TD 8axis");
                return;
            }

            //send TT command to 4 axis
            response = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            string value_string_4axis = response.Substring(0, response.IndexOf(Environment.NewLine));
            secondline = response.Substring(response.IndexOf(Environment.NewLine) + 2);
            if ((secondline != ":") && (secondline != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in TD 4axis Motor");
                return;
            }
            //extract numbers
            double[] nums_8axis = Array.ConvertAll(value_string_8axis.Split(','), double.Parse);
            double[] nums_4axis = Array.ConvertAll(value_string_4axis.Split(','), double.Parse);
            Exo_Right_Knee.Torque_Value = nums_8axis[0];
            Exo_Right_Ankle.Torque_Value = nums_8axis[1];
            Exo_Right_Hip_Front.Torque_Value = nums_8axis[2];
            Exo_Right_Hip_Rear.Torque_Value = nums_8axis[3];
            Exo_Left_Knee.Torque_Value = nums_8axis[4];
            Exo_Left_Ankle.Torque_Value = nums_8axis[5];
            Exo_Left_Hip_Front.Torque_Value = nums_8axis[6];
            Exo_Left_Hip_Rear.Torque_Value = nums_8axis[7];

            Exo_Right_Hip_Center.Torque_Value = nums_4axis[2];
            Exo_Left_Hip_Center.Torque_Value = nums_4axis[1];
        }
        public static void Refresh_Velocity()
        {
            //send TV command to 8 axis
            string command = "TV \r\n";
            string response = Galil_Interface.Transeive_Galil(command, "8axis");
            string value_string_8axis = response.Substring(0, response.IndexOf(Environment.NewLine));
            string secondline = response.Substring(response.IndexOf(Environment.NewLine) + 2);
            if ((secondline != ":") && (secondline != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in TD 8axis");
                return;
            }

            //send TV command to 4 axis
            response = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            string value_string_4axis = response.Substring(0, response.IndexOf(Environment.NewLine));
            secondline = response.Substring(response.IndexOf(Environment.NewLine) + 2);
            if ((secondline != ":") && (secondline != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in TD 4axis biss");
                return;
            }
            //extract numbers
            double[] nums_8axis = Array.ConvertAll(value_string_8axis.Split(','), double.Parse);
            double[] nums_4axis = Array.ConvertAll(value_string_4axis.Split(','), double.Parse);
            Exo_Right_Knee.Velocity_Value = nums_8axis[0];
            Exo_Right_Ankle.Velocity_Value = nums_8axis[1];
            Exo_Right_Hip_Front.Velocity_Value = nums_8axis[2];
            Exo_Right_Hip_Rear.Velocity_Value = nums_8axis[3];
            Exo_Left_Knee.Velocity_Value = nums_8axis[4];
            Exo_Left_Ankle.Velocity_Value = nums_8axis[5];
            Exo_Left_Hip_Front.Velocity_Value = nums_8axis[6];
            Exo_Left_Hip_Rear.Velocity_Value = nums_8axis[7];

            Exo_Right_Hip_Center.Velocity_Value = nums_4axis[2];
            Exo_Left_Hip_Center.Velocity_Value = nums_4axis[1];

        }

        public static string Convert_Angle(int inp, int offset)
        {
            double degree = (offset - inp) * 360 / 16383.0;
            if (degree > 180)
            {
                degree = degree - 360;
            }
            else if (degree < -180)
            {
                degree = degree + 360;
            }
            return degree.ToString("0.00");
        }
        public static bool Check_Start_Position_Error(Exo_Motor inp)
        {
            double reference_error_margin = 0.2;
            double base_val = inp.PVT_Start_Angle;
            double current_val = 0;
            current_val = double.Parse(Convert_Angle(inp.ABS_Encoder, inp.ABS_Encoder_Offset));
            double err = base_val - current_val;
            if (err < 0)
            {
                err = -err;
            }
            if (err < reference_error_margin)
                return true;
            else
                return false;
        }
        public static Exo_Logs Read_Data_for_Capture()
        {
            Exo_Logs outp = new Exo_Logs();
            Refresh_Angles();
            Refresh_Torques();
            Refresh_Velocity();

            //put angles
            outp.Left_Hip_Front.Angle = double.Parse(Convert_Angle(Exo_Left_Hip_Front.ABS_Encoder, Exo_Left_Hip_Front.ABS_Encoder_Offset));
            outp.Left_Hip_Center.Angle = double.Parse(Convert_Angle(Exo_Left_Hip_Center.ABS_Encoder, Exo_Left_Hip_Center.ABS_Encoder_Offset));
            outp.Left_Hip_Rear.Angle = double.Parse(Convert_Angle(Exo_Left_Hip_Rear.ABS_Encoder, Exo_Left_Hip_Rear.ABS_Encoder_Offset));
            outp.Left_Knee.Angle = double.Parse(Convert_Angle(Exo_Left_Knee.ABS_Encoder, Exo_Left_Knee.ABS_Encoder_Offset));
            outp.Left_Ankle.Angle = double.Parse(Convert_Angle(Exo_Left_Ankle.ABS_Encoder, Exo_Left_Ankle.ABS_Encoder_Offset));

            outp.Right_Hip_Front.Angle = double.Parse(Convert_Angle(Exo_Right_Hip_Front.ABS_Encoder, Exo_Right_Hip_Front.ABS_Encoder_Offset));
            outp.Right_Hip_Center.Angle = double.Parse(Convert_Angle(Exo_Right_Hip_Center.ABS_Encoder, Exo_Right_Hip_Center.ABS_Encoder_Offset));
            outp.Right_Hip_Rear.Angle = double.Parse(Convert_Angle(Exo_Right_Hip_Rear.ABS_Encoder, Exo_Right_Hip_Rear.ABS_Encoder_Offset));
            outp.Right_Knee.Angle = double.Parse(Convert_Angle(Exo_Right_Knee.ABS_Encoder, Exo_Right_Knee.ABS_Encoder_Offset));
            outp.Right_Ankle.Angle = double.Parse(Convert_Angle(Exo_Right_Ankle.ABS_Encoder, Exo_Right_Ankle.ABS_Encoder_Offset));

            //put velocity
            outp.Left_Hip_Front.Velocity = Exo_Left_Hip_Front.Velocity_Value;
            outp.Left_Hip_Center.Velocity = Exo_Left_Hip_Center.Velocity_Value;
            outp.Left_Hip_Rear.Velocity = Exo_Left_Hip_Rear.Velocity_Value;
            outp.Left_Knee.Velocity = Exo_Left_Knee.Velocity_Value;
            outp.Left_Ankle.Velocity = Exo_Left_Ankle.Velocity_Value;

            outp.Left_Hip_Front.Velocity = Exo_Left_Hip_Front.Velocity_Value;
            outp.Left_Hip_Center.Velocity = Exo_Left_Hip_Center.Velocity_Value;
            outp.Left_Hip_Rear.Velocity = Exo_Left_Hip_Rear.Velocity_Value;
            outp.Left_Knee.Velocity = Exo_Left_Knee.Velocity_Value;
            outp.Left_Ankle.Velocity = Exo_Left_Ankle.Velocity_Value;

            // put torque
            outp.Left_Hip_Front.Torque = Exo_Left_Hip_Front.Torque_Value;
            outp.Left_Hip_Center.Torque = Exo_Left_Hip_Center.Torque_Value;
            outp.Left_Hip_Rear.Torque = Exo_Left_Hip_Rear.Torque_Value;
            outp.Left_Knee.Torque = Exo_Left_Knee.Torque_Value;
            outp.Left_Ankle.Torque = Exo_Left_Ankle.Torque_Value;

            outp.Right_Hip_Front.Torque = Exo_Right_Hip_Front.Torque_Value;
            outp.Right_Hip_Center.Torque = Exo_Right_Hip_Center.Torque_Value;
            outp.Right_Hip_Rear.Torque = Exo_Right_Hip_Rear.Torque_Value;
            outp.Right_Knee.Torque = Exo_Right_Knee.Torque_Value;
            outp.Right_Ankle.Torque = Exo_Right_Ankle.Torque_Value;

            outp.milisecond = Get_Msecond();
            return outp;
        }
        public static int Get_Msecond()
        {
            DateTime d = DateTime.Now;
            return ((d.Hour * 3600) + (d.Minute * 60) + d.Second) * 1000 + d.Millisecond;
        }
        public static string Make_CaptureCSV_Title()
        {
            string s = "Time,RHF A,RHF T,RHF V,RHC A,RHC T,RHC V,RHR A,RHR T,RHR V,RK A,RK T,RK V,RA A,RA T,RA V,LHF A,LHF T,LHF V,LHC A,LHC T,LHC V,LHR A,LHR T,LHR V,LK A,LK T,LK V,LA A,LA T,LA V";
            return s;
        }
        public static string Make_CaptureCSV_Line(Exo_Logs inp)
        {
            string s = "";
            s += inp.milisecond.ToString() + ",";

            s += inp.Right_Hip_Front.Angle.ToString() + ",";
            s += inp.Right_Hip_Front.Torque.ToString() + ",";
            s += inp.Right_Hip_Front.Velocity.ToString() + ",";

            s += inp.Right_Hip_Center.Angle.ToString() + ",";
            s += inp.Right_Hip_Center.Torque.ToString() + ",";
            s += inp.Right_Hip_Center.Velocity.ToString() + ",";

            s += inp.Right_Hip_Rear.Angle.ToString() + ",";
            s += inp.Right_Hip_Rear.Torque.ToString() + ",";
            s += inp.Right_Hip_Rear.Velocity.ToString() + ",";

            s += inp.Right_Knee.Angle.ToString() + ",";
            s += inp.Right_Knee.Torque.ToString() + ",";
            s += inp.Right_Knee.Velocity.ToString() + ",";

            s += inp.Right_Ankle.Angle.ToString() + ",";
            s += inp.Right_Ankle.Torque.ToString() + ",";
            s += inp.Right_Ankle.Velocity.ToString() + ",";

            s += inp.Left_Hip_Front.Angle.ToString() + ",";
            s += inp.Left_Hip_Front.Torque.ToString() + ",";
            s += inp.Left_Hip_Front.Velocity.ToString() + ",";

            s += inp.Left_Hip_Center.Angle.ToString() + ",";
            s += inp.Left_Hip_Center.Torque.ToString() + ",";
            s += inp.Left_Hip_Center.Velocity.ToString() + ",";

            s += inp.Left_Hip_Rear.Angle.ToString() + ",";
            s += inp.Left_Hip_Rear.Torque.ToString() + ",";
            s += inp.Left_Hip_Rear.Velocity.ToString() + ",";

            s += inp.Left_Knee.Angle.ToString() + ",";
            s += inp.Left_Knee.Torque.ToString() + ",";
            s += inp.Left_Knee.Velocity.ToString() + ",";

            s += inp.Left_Ankle.Angle.ToString() + ",";
            s += inp.Left_Ankle.Torque.ToString() + ",";
            s += inp.Left_Ankle.Velocity.ToString();
            return s;
        }
        public static int PVT_response_reader(string inp)
        {
            double a = -1;
            string firstline = inp.Substring(0, inp.IndexOf(Environment.NewLine));
            string secondline = inp.Substring(inp.IndexOf(Environment.NewLine) + 2);
            try
            {
                a = double.Parse(firstline);
            }
            catch
            {
                MessageBox.Show("Error!!! Encoder value should be a number");
            }

            if (secondline != ":")
            {
                MessageBox.Show("Failed");
            }
            return (int)a;
        }

        public static int Read_PVT_Remain(Exo_Motor inp)
        {
            // read pvt remain encoder
            string command = "MG_PV" + inp.Main_Axis_ID + "\r\n";
            string device = inp.Main_Controller_ID;
            string response = Galil_Interface.Transeive_Galil(command, device);
            int pvt_remain = PVT_response_reader(response);
            return pvt_remain;
        }
        public static int Read_PVT_Execuate(Exo_Motor inp)
        {
            // read pvt remain encoder
            string command = "MG_BT" + inp.Main_Axis_ID + "\r\n";
            string device = inp.Main_Controller_ID;
            string response = Galil_Interface.Transeive_Galil(command, device);
            int pvt_remain = PVT_response_reader(response);
            return pvt_remain;
        }
        public static string Read_PVT_Startposition_file(string filename)
        {
            string[] lines_start_position;
            try
            {
                lines_start_position = File.ReadAllLines(filename);
            }
            catch
            {
                return "problem in reading start posintion pvt file, pleas browse it and try again";
            }
            if (lines_start_position.Length != 10)
            {
                return "not enough lines in the source file";
            }

            for (int i = 0; i < lines_start_position.Length; i++)
            {
                string[] parts;
                try
                {
                    // Split a string delimited by another string and return all elements.
                    parts = lines_start_position[i].Split(':');
                }
                catch
                {
                    return (lines_start_position[i] + " is invalid line");

                }
                if (parts[0] == "Front Left")
                {
                    try
                    {
                        Exo_Left_Hip_Front.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Back Left")
                {
                    try
                    {
                        Exo_Left_Hip_Rear.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Center Left")
                {
                    try
                    {
                        Exo_Left_Hip_Center.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Knee Left")
                {
                    try
                    {
                        Exo_Left_Knee.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Ankle Left")
                {
                    try
                    {
                        Exo_Left_Ankle.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Front Right")
                {
                    try
                    {
                        Exo_Right_Hip_Front.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Back Right")
                {
                    try
                    {
                        Exo_Right_Hip_Rear.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Center Right")
                {
                    try
                    {
                        Exo_Right_Hip_Center.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Knee Right")
                {
                    try
                    {
                        Exo_Right_Knee.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
                else if (parts[0] == "Ankle Right")
                {
                    try
                    {
                        Exo_Right_Ankle.PVT_Start_Angle = double.Parse(parts[1]);
                    }
                    catch
                    {
                        return (lines_start_position[i] + " is invalid line");
                    }
                }
            }

            return "Succeed";
        }

        public static string Check_Record_Status(int mode)
        {
            string command = "MG _RC \r\n";
            string s_8axis = Galil_Interface.Transeive_Galil(command, "8axis");
            string s_4axis = "";
            if (mode == 1) //this is angle record check 8 axis and 4 axis biss 
            {
                s_4axis = Galil_Interface.Transeive_Galil(command, "4axis Biss");
            }
            else if ((mode == 2) || (mode == 3)) // this is tourqe or velocity record check 8 axis and 4 axis motor
            {
                s_4axis = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            }
            else
            {
                return "Invalid record mode!!!";
            }
            // analyse responses from 8 axis 
            double record_status = -1;
            try
            {
                string value_string = s_8axis.Substring(0, s_8axis.IndexOf(Environment.NewLine));
                try
                {
                    record_status = double.Parse(value_string);
                    if (record_status > 0)
                    {
                        return "8 axis is recording, please wait until end of recording and try again";
                    }
                }
                catch
                {
                    return "Invalid response from 8 axis";
                }
                string secondline = s_8axis.Substring(s_8axis.IndexOf(Environment.NewLine) + 2);
                if ((secondline != ":") && (secondline != "\r\n:"))
                {
                    return "Invalid response from 8 axis";
                }
            }
            catch
            {
                return "problem in reading response from 8 axis";
            }
            // analyse responses from 8 axis 
            record_status = -1;
            try
            {
                string value_string = s_4axis.Substring(0, s_4axis.IndexOf(Environment.NewLine));
                try
                {
                    record_status = double.Parse(value_string);
                    if (record_status > 0)
                    {
                        return "4 axis is recording, please wait until end of recording and try again";
                    }
                }
                catch
                {
                    return "Invalid response from 4 axis";
                }
                string secondline = s_4axis.Substring(s_4axis.IndexOf(Environment.NewLine) + 2);
                if ((secondline != ":") && (secondline != "\r\n:"))
                {
                    return "Invalid response from 4 axis";
                }
            }
            catch
            {
                return "problem in reading response from 4 axis";
            }
            return "Succeed";
        }
        public static string Record_Init(int mode, int len)
        {
            //init 8 axis
            string device = "8axis";

            //DA *,*[0]; ' clear all arrays and variables
            string command = "DA *,*[0] \r\n";
            string response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DA *,*[0] ";
            }
            //DM A_BUF[1000]; ' motorA angle
            command = "DM A_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DM A_BUF ";
            }
            //DM B_BUF[1000]; ' motorB angle
            command = "DM B_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DM B_BUF ";
            }
            //DM C_BUF[1000]; ' motorC angle
            command = "DM C_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DM C_BUF ";
            }
            //DM D_BUF[1000]; ' motorD angle
            command = "DM D_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DM D_BUF ";
            }
            //DM E_BUF[1000]; ' motorE angle
            command = "DM E_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DM E_BUF ";
            }
            //DM F_BUF[1000]; ' motorF angle
            command = "DM F_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DM F_BUF ";
            }
            //DM G_BUF[1000]; ' motorG angle
            command = "DM G_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DM G_BUF ";
            }
            //DM H_BUF[1000]; ' motorH angle
            command = "DM H_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command DM H_BUF ";
            }
            //RA A_BUF[], B_BUF[], C_BUF[], D_BUF[], E_BUF[], F_BUF[], G_BUF[], H_BUF[]
            command = "RA A_BUF[], B_BUF[], C_BUF[], D_BUF[], E_BUF[], F_BUF[], G_BUF[], H_BUF[] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command RA ";
            }
            //RD _TDA, _TDB, _TDC, _TDD, _TDE, _TDF, _TDG, _TDH
            if (mode == 1)
                command = "RD _TDA, _TDB, _TDC, _TDD, _TDE, _TDF, _TDG, _TDH \r\n";
            else if (mode == 2)
                command = "RD _TTA, _TTB, _TTC, _TTD, _TTE, _TTF, _TTG, _TTH \r\n";
            else if (mode == 3)
                command = "RD _TVA, _TVB, _TVC, _TVD, _TVE, _TVF, _TVG, _TVH \r\n";
            else
                return "Invalid record mode!!!";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 8 axis in command RD ";
            }
            //init 4 axis 
            if (mode == 1)
                device = "4axis Biss";
            else
                device = "4axis Motor";
            //DA *,*[0]; ' clear all arrays and variables
            command = "DA *,*[0] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 4 axis in command DA *,*[0] ";
            }
            //DM A_BUF[1000]; ' motorA angle
            command = "DM A_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 4 axis in command DM A_BUF ";
            }
            //DM B_BUF[1000]; ' motorB angle
            command = "DM B_BUF[" + len.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 4 axis in command DM B_BUF ";
            }
            //RA A_BUF[], B_BUF[]
            command = "RA A_BUF[], B_BUF[] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 4 axis in command RA ";
            }
            //RD _TDA, _TDB
            if (mode == 1)
                command = "RD _TDA, _TDB \r\n";
            else if (mode == 2)
                command = "RD _TTB, _TTC \r\n";
            else if (mode == 3)
                command = "RD _TVB, _TVC \r\n";
            else
                return "Invalid record mode!!!";
            response = Galil_Interface.Transeive_Galil(command, device);
            if ((response != ":") && (response != "\r\n:"))
            {
                return "Invalid response from 4 axis in command RD ";
            }

            return "Succeed";
        }
        public static string Record_Start(int mode)
        {
            string command = "RC 5 \r\n";
            string device = "";
            if (mode == 1)
                device = "4axis Biss";
            else if ((mode == 2) || (mode == 3))
                device = "4axis Motor";
            else
                return "Invalid record mode!!!";
            string response_8axis = Galil_Interface.Transeive_Galil(command, "8axis");
            string response_4axis = Galil_Interface.Transeive_Galil(command, device);
            if ((response_8axis != ":") && (response_8axis != "\r\n:"))
            {
                return "Invalid response from 8 axis in command RD ";
            }
            if ((response_4axis != ":") && (response_4axis != "\r\n:"))
            {
                return "Invalid response from 4 axis in command RD ";
            }

            return "Succeed";
        }
        public static double Record_Invalid_Value = -1000000;
        public static Exo_Logs Read_Recorded_Data(int mode, int index)
        {
            Exo_Logs outp = new Exo_Logs();
            //read from 8 axis
            string device = "8axis";
            string command = "MG A_BUF[" + index.ToString() + "] \r\n";
            string response = Galil_Interface.Transeive_Galil(command, device);
            double a_val = analyze_record_string(response);
            if (a_val == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }
            command = "MG B_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double b_val = analyze_record_string(response);
            if (b_val == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }
            command = "MG C_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double c_val = analyze_record_string(response);
            if (c_val == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }
            command = "MG D_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double d_val = analyze_record_string(response);
            if (d_val == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }
            command = "MG E_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double e_val = analyze_record_string(response);
            if (e_val == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }
            command = "MG F_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double f_val = analyze_record_string(response);
            if (f_val == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }
            command = "MG G_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double g_val = analyze_record_string(response);
            if (g_val == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }
            command = "MG H_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double h_val = analyze_record_string(response);
            if (h_val == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }



            //read from 4 axis
            if (mode == 1)
                device = "4axis Biss";
            else if ((mode == 2) || (mode == 3))
                device = "4axis Motor";
            else
            {
                MessageBox.Show("invalid record mode");
                return null;
            }
            command = "MG A_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double a_val_4axis = analyze_record_string(response);
            if (a_val_4axis == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }
            command = "MG B_BUF[" + index.ToString() + "] \r\n";
            response = Galil_Interface.Transeive_Galil(command, device);
            double b_val_4axis = analyze_record_string(response);
            if (b_val_4axis == Record_Invalid_Value)
            {
                MessageBox.Show("problem in reading recorded data from 8 axis galil");
                return null;
            }

            
            // put invalid value in all fields of log class
            outp.Right_Knee.Angle = Record_Invalid_Value;
            outp.Right_Knee.Torque = Record_Invalid_Value;
            outp.Right_Knee.Velocity = Record_Invalid_Value;
            outp.Right_Hip_Center.Angle = Record_Invalid_Value;
            outp.Right_Hip_Center.Torque = Record_Invalid_Value;
            outp.Right_Hip_Center.Velocity = Record_Invalid_Value;
            outp.Right_Hip_Front.Angle = Record_Invalid_Value;
            outp.Right_Hip_Front.Torque = Record_Invalid_Value;
            outp.Right_Hip_Front.Velocity = Record_Invalid_Value;
            outp.Right_Hip_Rear.Angle = Record_Invalid_Value;
            outp.Right_Hip_Rear.Torque = Record_Invalid_Value;
            outp.Right_Hip_Rear.Velocity = Record_Invalid_Value;
            outp.Right_Ankle.Angle = Record_Invalid_Value;
            outp.Right_Ankle.Torque = Record_Invalid_Value;
            outp.Right_Ankle.Velocity = Record_Invalid_Value;

            outp.Left_Knee.Angle = Record_Invalid_Value;
            outp.Left_Knee.Torque = Record_Invalid_Value;
            outp.Left_Knee.Velocity = Record_Invalid_Value;
            outp.Left_Hip_Center.Angle = Record_Invalid_Value;
            outp.Left_Hip_Center.Torque = Record_Invalid_Value;
            outp.Left_Hip_Center.Velocity = Record_Invalid_Value;
            outp.Left_Hip_Front.Angle = Record_Invalid_Value;
            outp.Left_Hip_Front.Torque = Record_Invalid_Value;
            outp.Left_Hip_Front.Velocity = Record_Invalid_Value;
            outp.Left_Hip_Rear.Angle = Record_Invalid_Value;
            outp.Left_Hip_Rear.Torque = Record_Invalid_Value;
            outp.Left_Hip_Rear.Velocity = Record_Invalid_Value;
            outp.Left_Ankle.Angle = Record_Invalid_Value;
            outp.Left_Ankle.Torque = Record_Invalid_Value;
            outp.Left_Ankle.Velocity = Record_Invalid_Value;

            //put readed data into approprate field of the class
            outp.milisecond = 32 * index;
            if (mode == 1) // this is ankle
            {
                outp.Right_Knee.Angle = Convert_Record_Angle(a_val, Exo_Right_Knee.ABS_Encoder_Offset);
                outp.Right_Ankle.Angle = Convert_Record_Angle(b_val, Exo_Right_Ankle.ABS_Encoder_Offset);
                outp.Right_Hip_Front.Angle = Convert_Record_Angle(c_val, Exo_Right_Hip_Front.ABS_Encoder_Offset);
                outp.Right_Hip_Rear.Angle = Convert_Record_Angle(d_val, Exo_Right_Hip_Rear.ABS_Encoder_Offset);
                outp.Right_Hip_Center.Angle = Convert_Record_Angle(a_val_4axis, Exo_Right_Hip_Center.ABS_Encoder_Offset);

                outp.Left_Knee.Angle = Convert_Record_Angle(e_val, Exo_Left_Knee.ABS_Encoder_Offset);
                outp.Left_Ankle.Angle = Convert_Record_Angle(f_val, Exo_Left_Ankle.ABS_Encoder_Offset);
                outp.Left_Hip_Front.Angle = Convert_Record_Angle(g_val, Exo_Left_Hip_Front.ABS_Encoder_Offset);
                outp.Left_Hip_Rear.Angle = Convert_Record_Angle(h_val, Exo_Left_Hip_Rear.ABS_Encoder_Offset);
                outp.Left_Hip_Center.Angle = Convert_Record_Angle(b_val_4axis, Exo_Left_Hip_Center.ABS_Encoder_Offset);
            }
            else if (mode == 2) // this is torque
            {
                outp.Right_Knee.Torque = a_val;
                outp.Right_Ankle.Torque = b_val;
                outp.Right_Hip_Front.Torque = c_val;
                outp.Right_Hip_Rear.Torque = d_val;
                outp.Right_Hip_Center.Torque = a_val_4axis;

                outp.Left_Knee.Torque = e_val;
                outp.Left_Ankle.Torque = f_val;
                outp.Left_Hip_Front.Torque = g_val;
                outp.Left_Hip_Rear.Torque = h_val;
                outp.Left_Hip_Center.Torque = b_val_4axis;
            }
            else if (mode == 3) // this is velocity
            {
                outp.Right_Knee.Velocity = a_val;
                outp.Right_Ankle.Velocity = b_val;
                outp.Right_Hip_Front.Velocity = c_val;
                outp.Right_Hip_Rear.Velocity = d_val;
                outp.Right_Hip_Center.Velocity = a_val_4axis;

                outp.Left_Knee.Velocity = e_val;
                outp.Left_Ankle.Velocity = f_val;
                outp.Left_Hip_Front.Velocity = g_val;
                outp.Left_Hip_Rear.Velocity = h_val;
                outp.Left_Hip_Center.Velocity = b_val_4axis;
            }
            return outp;
        }
        private static double analyze_record_string(string inp)
        {
            double outp = Record_Invalid_Value; // some invalid value
            if (inp == "?")
            {
                return outp;
            }
            string value_string = "";
            string secondline = "";
            try
            {
                value_string = inp.Substring(0, inp.IndexOf(Environment.NewLine));
                secondline = inp.Substring(inp.IndexOf(Environment.NewLine) + 2);
            }
            catch
            {
                return outp;
            }
            if ((secondline != ":") && (secondline != "\r\n:"))
            {
                return outp;
            }
            try
            {
                outp = double.Parse(value_string);
            }
            catch
            {
                return outp;
            }

            return outp;
        }
        public static double Convert_Record_Angle(double inp, int offset)
        {
            double degree = (offset - inp) * 360 / 16383.0;
            if (degree > 180)
            {
                degree = degree - 360;
            }
            else if (degree < -180)
            {
                degree = degree + 360;
            }
            return degree;
        }
        public static string Make_CaptureCSV_Line2(Exo_Logs inp)
        {
            string s = "";
            string s2 = "";
            s += inp.milisecond.ToString() + ",";

            if (inp.Right_Hip_Front.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Front.Angle.ToString();
            s += s2 + ",";
            if (inp.Right_Hip_Front.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Front.Torque.ToString();
            s += s2 + ",";
            if (inp.Right_Hip_Front.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Front.Velocity.ToString();
            s += s2 + ",";
            ///////////////////
            if (inp.Right_Hip_Center.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Center.Angle.ToString();
            s += s2 + ",";
            if (inp.Right_Hip_Center.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Center.Torque.ToString();
            s += s2 + ",";
            if (inp.Right_Hip_Center.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Center.Velocity.ToString();
            s += s2 + ",";
            /////////
            if (inp.Right_Hip_Rear.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Rear.Angle.ToString();
            s += s2 + ",";
            if (inp.Right_Hip_Rear.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Rear.Torque.ToString();
            s += s2 + ",";
            if (inp.Right_Hip_Rear.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Hip_Rear.Velocity.ToString();
            s += s2 + ",";
            ////////////////
            if (inp.Right_Knee.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Knee.Angle.ToString();
            s += s2 + ",";
            if (inp.Right_Knee.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Knee.Torque.ToString();
            s += s2 + ",";
            if (inp.Right_Knee.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Knee.Velocity.ToString();
            s += s2 + ",";
            ////////////////
            if (inp.Right_Ankle.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Ankle.Angle.ToString();
            s += s2 + ",";
            if (inp.Right_Ankle.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Ankle.Torque.ToString();
            s += s2 + ",";
            if (inp.Right_Ankle.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Right_Ankle.Velocity.ToString();
            s += s2 + ",";
            ///////////////////////
            if (inp.Left_Hip_Front.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Front.Angle.ToString();
            s += s2 + ",";
            if (inp.Left_Hip_Front.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Front.Torque.ToString();
            s += s2 + ",";
            if (inp.Left_Hip_Front.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Front.Velocity.ToString();
            s += s2 + ",";
            ///////////////////
            if (inp.Left_Hip_Center.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Center.Angle.ToString();
            s += s2 + ",";
            if (inp.Left_Hip_Center.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Center.Torque.ToString();
            s += s2 + ",";
            if (inp.Left_Hip_Center.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Center.Velocity.ToString();
            s += s2 + ",";
            /////////
            if (inp.Left_Hip_Rear.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Rear.Angle.ToString();
            s += s2 + ",";
            if (inp.Left_Hip_Rear.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Rear.Torque.ToString();
            s += s2 + ",";
            if (inp.Left_Hip_Rear.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Hip_Rear.Velocity.ToString();
            s += s2 + ",";
            ////////////////
            if (inp.Left_Knee.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Knee.Angle.ToString();
            s += s2 + ",";
            if (inp.Left_Knee.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Knee.Torque.ToString();
            s += s2 + ",";
            if (inp.Left_Knee.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Knee.Velocity.ToString();
            s += s2 + ",";
            ////////////////
            if (inp.Left_Ankle.Angle == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Ankle.Angle.ToString();
            s += s2 + ",";
            if (inp.Left_Ankle.Torque == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Ankle.Torque.ToString();
            s += s2 + ",";
            if (inp.Left_Ankle.Velocity == Record_Invalid_Value)
                s2 = "";
            else
                s2 = inp.Left_Ankle.Velocity.ToString();
            s += s2;

            return s;
        }
        public static string Record_Terminate(int mode)
        {
            string command = "DA *,*[0] \r\n";
            string device = "";
            if (mode == 1)
                device = "4axis Biss";
            else if ((mode == 2) || (mode == 3))
                device = "4axis Motor";
            else
                return "Invalid record mode!!!";
            string response_8axis = Galil_Interface.Transeive_Galil(command, "8axis");
            string response_4axis = Galil_Interface.Transeive_Galil(command, device);
            if ((response_8axis != ":") && (response_8axis != "\r\n:"))
            {
                return "Invalid response from 8 axis in terminate command ";
            }
            if ((response_4axis != ":") && (response_4axis != "\r\n:"))
            {
                return "Invalid response from 4 axis in terminate command";
            }

            return "Succeed";
        }

    }
}
