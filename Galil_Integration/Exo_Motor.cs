﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galil_Integration
{
    public class Exo_Motor
    {
        public string Main_Controller_ID { get; set; }
        public string Main_Axis_ID { get; set; }
        public string Biss_Controller_ID { get; set; }
        public string Biss_Axis_ID { get; set; }
        public int ABS_Encoder { get; set; }
        public int ABS_Encoder_Offset { get; set; }
        public double Gear_Head_Ratio { get; set; }
        public double Nuetral_Angle { get; set; }
        public double Torque_Value { get; set; }
        public double Velocity_Value { get; set; }
        public double PVT_Start_Angle { get; set; }
    }
}
