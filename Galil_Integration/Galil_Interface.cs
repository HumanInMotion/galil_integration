﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows.Forms;
namespace Galil_Integration
{
    public static class Galil_Interface
    {
        private static string IP_add_8axis;
        private static string IP_add_4axis_Biss;
        private static string IP_add_4axis_Motor;


        public static bool Connection_status_8axis;
        public static bool Connection_status_4axis_Biss;
        public static bool Connection_status_4axis_Motor;
        private static TcpClient client_8axis;
        private static TcpClient client_4axis_Biss;
        private static TcpClient client_4axis_Motor;

        public static void Connect_to_Galil_8axis()
        {
            client_8axis = new TcpClient();
            try
            {
                client_8axis.Connect(IP_add_8axis, 23);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
                Connection_status_8axis = false;
            }

            if (client_8axis.Connected)
            {
                Connection_status_8axis = true;
            }
            else
            {
                Connection_status_8axis = false;
            }
        }
        public static void Connect_to_Galil_4axis_Biss()
        {
            client_4axis_Biss = new TcpClient();
            try
            {
                client_4axis_Biss.Connect(IP_add_4axis_Biss, 23);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
                Connection_status_4axis_Biss = false;
            }

            if (client_4axis_Biss.Connected)
            {
                Connection_status_4axis_Biss = true;
            }
            else
            {
                Connection_status_4axis_Biss = false;
            }
        }
        public static void Connect_to_Galil_4axis_Motor()
        {
            client_4axis_Motor = new TcpClient();
            try
            {
                client_4axis_Motor.Connect(IP_add_4axis_Motor, 23);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
                Connection_status_4axis_Motor = false;
            }

            if (client_4axis_Motor.Connected)
            {
                Connection_status_4axis_Motor = true;
            }
            else
            {
                Connection_status_4axis_Motor = false;
            }
        }

        public static void Disconnect_from_Galil_8axis()
        {
            client_8axis.Close();
            Connection_status_8axis = false;
        }
        public static void Disconnect_from_Galil_4axis_Biss()
        {
            client_4axis_Biss.Close();
            Connection_status_4axis_Biss = false;
        }
        public static void Disconnect_from_Galil_4axis_Motor()
        {
            client_4axis_Motor.Close();
            Connection_status_4axis_Motor = false;
        }

        public static void Init()
        {
            IP_add_8axis = "192.168.1.63";
            IP_add_4axis_Biss = "192.168.1.80";
            IP_add_4axis_Motor = "192.168.1.70";
            Connection_status_8axis = false;
            Connection_status_4axis_Biss = false;
            Connection_status_4axis_Motor = false;
        }
        public static void Connect_All()
        {
            if (!Connection_status_8axis)
            {
                Connect_to_Galil_8axis();
            }
            if (!Connection_status_4axis_Biss)
            {
                Connect_to_Galil_4axis_Biss();
            }
            if (!Connection_status_4axis_Motor)
            {
                Connect_to_Galil_4axis_Motor();
            }
        }

        public static void Disconnect_All()
        {
            if (Connection_status_8axis)
            {
                Disconnect_from_Galil_8axis();
            }
            if (Connection_status_4axis_Biss)
            {
                Disconnect_from_Galil_4axis_Biss();
            }
            if (Connection_status_4axis_Motor)
            {
                Disconnect_from_Galil_4axis_Motor();
            }
        }
        public static string Transeive_Galil(string command,string device)
        {
            string response = "Connection Error";
            NetworkStream nwStream;
            if (device == "8axis")
            {
                nwStream = client_8axis.GetStream();
            }
            else if (device=="4axis Biss")
            {
                nwStream = client_4axis_Biss.GetStream();
            }
            else if (device == "4axis Motor")
            {
                nwStream = client_4axis_Motor.GetStream();
            }
            else
            {
                MessageBox.Show("Unknown device");
                return response;
            }
            nwStream.ReadTimeout = 1000;
            nwStream.WriteTimeout = 1000;
            byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(command);

            try
            {
                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
            }
            catch
            {
                MessageBox.Show("Problem in TCP send");
                return response;
            }
            try
            {
                byte[] bytesToRead = new byte[client_8axis.ReceiveBufferSize];
                int bytesRead = nwStream.Read(bytesToRead, 0, client_8axis.ReceiveBufferSize);
                response = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                return response;
            }
            catch
            {
                MessageBox.Show("Problem in TCP receive");
                return response;
            }

        }
    }
}
