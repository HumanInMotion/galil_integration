﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galil_Integration
{
    public class Exo_Logs
    {
        public int milisecond;
        public Log_Data Right_Hip_Front = new Log_Data();
        public Log_Data Right_Hip_Center = new Log_Data();
        public Log_Data Right_Hip_Rear = new Log_Data();
        public Log_Data Right_Knee = new Log_Data();
        public Log_Data Right_Ankle = new Log_Data();
        public Log_Data Left_Hip_Front = new Log_Data();
        public Log_Data Left_Hip_Center = new Log_Data();
        public Log_Data Left_Hip_Rear = new Log_Data();
        public Log_Data Left_Knee = new Log_Data();
        public Log_Data Left_Ankle = new Log_Data();
    }
}
