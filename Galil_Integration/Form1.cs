﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Galil_Integration
{
    public partial class Form1 : Form
    {


        public Form1()
        {
            InitializeComponent();
            Galil_Interface.Init();
            Galil_Programs.Exo_Definition();

        }
        #region Capture
        List<Exo_Logs> Galil_Capture = new List<Exo_Logs>();
        bool Capture_Enable_flag = false;
        int Capture_start_Momoent_Milisecond;
        double Capture_Duration_Milisecond = 15000;


        #endregion

        #region TCP Connection
        private bool First_Call = false;
        private void Connection_Update_Screen()
        {
            #region Connect Buttons
            if ((Galil_Interface.Connection_status_8axis) && (Connect_Button_8axis.Enabled))
            {
                Connect_Button_8axis.Invoke((MethodInvoker)delegate { Connect_Button_8axis.Enabled = false; });
            }
            if ((!Galil_Interface.Connection_status_8axis) && (!Connect_Button_8axis.Enabled))
            {
                Connect_Button_8axis.Invoke((MethodInvoker)delegate { Connect_Button_8axis.Enabled = true; });
            }
            if ((Galil_Interface.Connection_status_4axis_Biss) && (Connect_Button_4axis_Biss.Enabled))
            {
                Connect_Button_4axis_Biss.Invoke((MethodInvoker)delegate { Connect_Button_4axis_Biss.Enabled = false; });
            }
            if ((!Galil_Interface.Connection_status_4axis_Biss) && (!Connect_Button_4axis_Biss.Enabled))
            {
                Connect_Button_4axis_Biss.Invoke((MethodInvoker)delegate { Connect_Button_4axis_Biss.Enabled = true; });
            }
            if ((Galil_Interface.Connection_status_4axis_Motor) && (Connect_Button_4axis_Motor.Enabled))
            {
                Connect_Button_4axis_Motor.Invoke((MethodInvoker)delegate { Connect_Button_4axis_Motor.Enabled = false; });
            }
            if ((!Galil_Interface.Connection_status_4axis_Motor) && (!Connect_Button_4axis_Motor.Enabled))
            {
                Connect_Button_4axis_Motor.Invoke((MethodInvoker)delegate { Connect_Button_4axis_Motor.Enabled = true; });
            }
            if ((Galil_Interface.Connection_status_8axis) && (Galil_Interface.Connection_status_4axis_Biss) && (Galil_Interface.Connection_status_4axis_Motor))
            {
                if (Connect_All_Button.Enabled)
                {
                    Connect_All_Button.Invoke((MethodInvoker)delegate { Connect_All_Button.Enabled = false; });
                }
            }
            else
            {
                if (!Connect_All_Button.Enabled)
                {
                    Connect_All_Button.Invoke((MethodInvoker)delegate { Connect_All_Button.Enabled = true; });
                }
            }
            #endregion
            #region Disonnect Buttons
            if ((Galil_Interface.Connection_status_8axis) && (!Disconnect_Button_8axis.Enabled))
            {
                Disconnect_Button_8axis.Invoke((MethodInvoker)delegate { Disconnect_Button_8axis.Enabled = true; });
            }
            if ((!Galil_Interface.Connection_status_8axis) && (Disconnect_Button_8axis.Enabled))
            {
                Disconnect_Button_8axis.Invoke((MethodInvoker)delegate { Disconnect_Button_8axis.Enabled = false; });
            }
            if ((Galil_Interface.Connection_status_4axis_Biss) && (!Disconnect_Button_4axis_Biss.Enabled))
            {
                Disconnect_Button_4axis_Biss.Invoke((MethodInvoker)delegate { Disconnect_Button_4axis_Biss.Enabled = true; });
            }
            if ((!Galil_Interface.Connection_status_4axis_Biss) && (Disconnect_Button_4axis_Biss.Enabled))
            {
                Disconnect_Button_4axis_Biss.Invoke((MethodInvoker)delegate { Disconnect_Button_4axis_Biss.Enabled = false; });
            }
            if ((Galil_Interface.Connection_status_4axis_Motor) && (!Disconnect_Button_4axis_Motor.Enabled))
            {
                Disconnect_Button_4axis_Motor.Invoke((MethodInvoker)delegate { Disconnect_Button_4axis_Motor.Enabled = true; });
            }
            if ((!Galil_Interface.Connection_status_4axis_Motor) && (Disconnect_Button_4axis_Motor.Enabled))
            {
                Disconnect_Button_4axis_Motor.Invoke((MethodInvoker)delegate { Disconnect_Button_4axis_Motor.Enabled = false; });
            }
            if ((Galil_Interface.Connection_status_8axis) || (Galil_Interface.Connection_status_4axis_Biss) || (Galil_Interface.Connection_status_4axis_Motor))
            {
                if (!Disconnect_All_Button.Enabled)
                {
                    Disconnect_All_Button.Invoke((MethodInvoker)delegate { Disconnect_All_Button.Enabled = true; });
                }
            }
            else
            {
                if (Disconnect_All_Button.Enabled)
                {
                    Disconnect_All_Button.Invoke((MethodInvoker)delegate { Disconnect_All_Button.Enabled = false; });
                }
            }
            #endregion
            #region Connection Status Labels
            if ((Galil_Interface.Connection_status_8axis) && (Connection_Status_Label_8axis.Text != "Connected"))
            {
                Connection_Status_Label_8axis.Invoke((MethodInvoker)delegate { Connection_Status_Label_8axis.Text = "Connected"; });
            }
            if ((!Galil_Interface.Connection_status_8axis) && (Connection_Status_Label_8axis.Text != "Disconnected"))
            {
                Connection_Status_Label_8axis.Invoke((MethodInvoker)delegate { Connection_Status_Label_8axis.Text = "Disconnected"; });
            }
            if ((Galil_Interface.Connection_status_4axis_Biss) && (Connection_Status_Label_4axis_Biss.Text != "Connected"))
            {
                Connection_Status_Label_4axis_Biss.Invoke((MethodInvoker)delegate { Connection_Status_Label_4axis_Biss.Text = "Connected"; });
            }
            if ((!Galil_Interface.Connection_status_4axis_Biss) && (Connection_Status_Label_4axis_Biss.Text != "Disconnected"))
            {
                Connection_Status_Label_4axis_Biss.Invoke((MethodInvoker)delegate { Connection_Status_Label_4axis_Biss.Text = "Disconnected"; });
            }
            if ((Galil_Interface.Connection_status_4axis_Motor) && (Connection_Status_Label_4axis_Motor.Text != "Connected"))
            {
                Connection_Status_Label_4axis_Motor.Invoke((MethodInvoker)delegate { Connection_Status_Label_4axis_Motor.Text = "Connected"; });
            }
            if ((!Galil_Interface.Connection_status_4axis_Motor) && (Connection_Status_Label_4axis_Motor.Text != "Disconnected"))
            {
                Connection_Status_Label_4axis_Motor.Invoke((MethodInvoker)delegate { Connection_Status_Label_4axis_Motor.Text = "Disconnected"; });
            }
            #endregion
            #region Exo Groupboxes
            if ((Galil_Interface.Connection_status_8axis) && (Galil_Interface.Connection_status_4axis_Biss) && (Galil_Interface.Connection_status_4axis_Motor))
            {
                if (!RKnee_Gbox.Enabled)
                {
                    RKnee_Gbox.Invoke((MethodInvoker)delegate { RKnee_Gbox.Enabled = true; });
                }
                if (!LKnee_Gbox.Enabled)
                {
                    LKnee_Gbox.Invoke((MethodInvoker)delegate { LKnee_Gbox.Enabled = true; });
                }
                if (!RAnkle_Gbox.Enabled)
                {
                    RAnkle_Gbox.Invoke((MethodInvoker)delegate { RAnkle_Gbox.Enabled = true; });
                }
                if (!LAnkle_Gbox.Enabled)
                {
                    LAnkle_Gbox.Invoke((MethodInvoker)delegate { LAnkle_Gbox.Enabled = true; });
                }
                if (!RHCenter_Gbox.Enabled)
                {
                    RHCenter_Gbox.Invoke((MethodInvoker)delegate { RHCenter_Gbox.Enabled = true; });
                }
                if (!LHCenter_Gbox.Enabled)
                {
                    LHCenter_Gbox.Invoke((MethodInvoker)delegate { LHCenter_Gbox.Enabled = true; });
                }
                if (!RHFront_Gbox.Enabled)
                {
                    RHFront_Gbox.Invoke((MethodInvoker)delegate { RHFront_Gbox.Enabled = true; });
                }
                if (!LHFront_Gbox.Enabled)
                {
                    LHFront_Gbox.Invoke((MethodInvoker)delegate { LHFront_Gbox.Enabled = true; });
                }
                if (!RHRear_Gbox.Enabled)
                {
                    RHRear_Gbox.Invoke((MethodInvoker)delegate { RHRear_Gbox.Enabled = true; });
                }
                if (!LHRear_Gbox.Enabled)
                {
                    LHRear_Gbox.Invoke((MethodInvoker)delegate { LHRear_Gbox.Enabled = true; });
                }
            }
            else
            {
                if (RKnee_Gbox.Enabled)
                {
                    RKnee_Gbox.Invoke((MethodInvoker)delegate { RKnee_Gbox.Enabled = false; });
                }
                if (LKnee_Gbox.Enabled)
                {
                    LKnee_Gbox.Invoke((MethodInvoker)delegate { LKnee_Gbox.Enabled = false; });
                }
                if (RAnkle_Gbox.Enabled)
                {
                    RAnkle_Gbox.Invoke((MethodInvoker)delegate { RAnkle_Gbox.Enabled = false; });
                }
                if (LAnkle_Gbox.Enabled)
                {
                    LAnkle_Gbox.Invoke((MethodInvoker)delegate { LAnkle_Gbox.Enabled = false; });
                }
                if (RHCenter_Gbox.Enabled)
                {
                    RHCenter_Gbox.Invoke((MethodInvoker)delegate { RHCenter_Gbox.Enabled = false; });
                }
                if (LHCenter_Gbox.Enabled)
                {
                    LHCenter_Gbox.Invoke((MethodInvoker)delegate { LHCenter_Gbox.Enabled = false; });
                }
                if (RHFront_Gbox.Enabled)
                {
                    RHFront_Gbox.Invoke((MethodInvoker)delegate { RHFront_Gbox.Enabled = false; });
                }
                if (LHFront_Gbox.Enabled)
                {
                    LHFront_Gbox.Invoke((MethodInvoker)delegate { LHFront_Gbox.Enabled = false; });
                }
                if (RHRear_Gbox.Enabled)
                {
                    RHRear_Gbox.Invoke((MethodInvoker)delegate { RHRear_Gbox.Enabled = false; });
                }
                if (LHRear_Gbox.Enabled)
                {
                    LHRear_Gbox.Invoke((MethodInvoker)delegate { LHRear_Gbox.Enabled = false; });
                }
            }

            #endregion
            #region other buttons
            if (Refresh_Button.Enabled != LHRear_Gbox.Enabled)
            {
                Refresh_Button.Invoke((MethodInvoker)delegate { Refresh_Button.Enabled = LHRear_Gbox.Enabled; });
            }
            if (Reset_All_Button.Enabled != LHRear_Gbox.Enabled)
            {
                Reset_All_Button.Invoke((MethodInvoker)delegate { Reset_All_Button.Enabled = LHRear_Gbox.Enabled; });
            }
            if (Stop_ALL_Button.Enabled != LHRear_Gbox.Enabled)
            {
                Stop_ALL_Button.Invoke((MethodInvoker)delegate { Stop_ALL_Button.Enabled = LHRear_Gbox.Enabled; });
            }

            #endregion
        }

        private void Connect_Button_8axis_Click(object sender, EventArgs e)
        {
            Connect_Button_8axis.Enabled = false;
            Connect_All_Button.Enabled = false;
            Galil_Interface.Connect_to_Galil_8axis();
            Connection_Update_Screen();
        }

        private void Connect_Button_4axis_Biss_Click(object sender, EventArgs e)
        {
            Connect_Button_4axis_Biss.Enabled = false;
            Connect_All_Button.Enabled = false;
            Galil_Interface.Connect_to_Galil_4axis_Biss();
            Connection_Update_Screen();
        }

        private void Connect_Button_4axis_Motor_Click(object sender, EventArgs e)
        {
            Connect_Button_4axis_Motor.Enabled = false;
            Connect_All_Button.Enabled = false;
            Galil_Interface.Connect_to_Galil_4axis_Motor();
            Connection_Update_Screen();
        }

        private void Disconnect_Button_8axis_Click(object sender, EventArgs e)
        {
            Disconnect_Button_8axis.Enabled = false;
            Disconnect_All_Button.Enabled = false;
            Galil_Interface.Disconnect_from_Galil_8axis();
            Connection_Update_Screen();
        }

        private void Disconnect_Button_4axis_Biss_Click(object sender, EventArgs e)
        {
            Disconnect_Button_4axis_Biss.Enabled = false;
            Disconnect_All_Button.Enabled = false;
            Galil_Interface.Disconnect_from_Galil_4axis_Biss();
            Connection_Update_Screen();
        }

        private void Disconnect_Button_4axis_Motor_Click(object sender, EventArgs e)
        {
            Disconnect_Button_4axis_Motor.Enabled = false;
            Disconnect_All_Button.Enabled = false;
            Galil_Interface.Disconnect_from_Galil_4axis_Motor();
            Connection_Update_Screen();
        }

        private void Connect_All_Button_Click(object sender, EventArgs e)
        {
            Disconnect_All_Button.Enabled = false;
            Galil_Interface.Connect_All();
            Connection_Update_Screen();

        }

        private void Disconnect_All_Button_Click(object sender, EventArgs e)
        {
            Disconnect_All_Button.Enabled = false;
            Galil_Interface.Disconnect_All();
            Connection_Update_Screen();
        }

        #endregion
        #region BaseTick
        private int blink_cntr;
        private void Basetick_Tick(object sender, EventArgs e)
        {
            if (!First_Call)
            {
                Connection_Update_Screen();
                First_Call = true;
            }
            if (Capture_Enable_flag)
            {
                //List<Exo_Logs> Galil_Capture = new List<Exo_Logs>();

                int milisecond = Galil_Programs.Get_Msecond();
                milisecond -= Capture_start_Momoent_Milisecond;
                // Galil_Capture.Add(clog);
                if (milisecond > Capture_Duration_Milisecond)
                {
                    Capture_Enable_flag = false;
                }
                Refresh_PVT_Tboxes();
                /*       if (PVT_Pause_Pbox.Visible)
                       {
                           PVT_Pause_Pbox.Invoke((MethodInvoker)delegate { PVT_Pause_Pbox.Visible = false; });
                       }
                       if (blink_cntr == 0)
                       {
                           PVT_Record_Pbox.Invoke((MethodInvoker)delegate { PVT_Record_Pbox.Visible = !PVT_Record_Pbox.Visible; });
                       }*/
            }
            else
            {
                if (PVT_Record_Pbox.Visible)
                {
                    PVT_Record_Pbox.Invoke((MethodInvoker)delegate { PVT_Record_Pbox.Visible = false; });
                }
                if (!PVT_Pause_Pbox.Visible)
                {
                    PVT_Pause_Pbox.Invoke((MethodInvoker)delegate { PVT_Pause_Pbox.Visible = true; });
                }
            }
            blink_cntr++;
            blink_cntr &= 3;
            string s = Galil_Capture.Count.ToString();
            if (s != Record_Len_Tbox.Text)
            {
                Record_Len_Tbox.Invoke((MethodInvoker)delegate { Record_Len_Tbox.Text = s; });
            }

            if (show_progress_flag)
            {
                int mytime = Galil_Programs.Get_Msecond();
                mytime -= show_progress_moment;
                if (mytime > 1000*32)
                {
                    int temp = 100;
                    Motion_Progress_Bar.Invoke((MethodInvoker)delegate { Motion_Progress_Bar.Value = temp; });
                    Motion_Progress_Label.Invoke((MethodInvoker)delegate { Motion_Progress_Label.Text = temp.ToString() + " %"; });
                    show_progress_flag = false;
                }
                else
                {
                    int temp = mytime /320;
                    Motion_Progress_Bar.Invoke((MethodInvoker)delegate { Motion_Progress_Bar.Value = temp; });
                    Motion_Progress_Label.Invoke((MethodInvoker)delegate { Motion_Progress_Label.Text = temp.ToString() + " %"; });

                }
            }
        }
        #endregion
        #region Terminal
        private void Terminal_Send_Button_Click(object sender, EventArgs e)
        {
            if ((Terminal_Command_CBox.SelectedIndex < 0) || (Terminal_Device_CBox.SelectedIndex < 0))
            {
                MessageBox.Show("Pleas select command and device");
            }
            else
            {
                string command;
                string device = "";
                if (Terminal_Device_CBox.SelectedIndex == 0)
                {
                    device = "8axis";
                }
                else if (Terminal_Device_CBox.SelectedIndex == 1)
                {
                    device = "4axis Biss";
                }
                else if (Terminal_Device_CBox.SelectedIndex == 2)
                {
                    device = "4axis Motor";
                }
                command = Terminal_Command_CBox.SelectedItem.ToString();
                command += "\r\n";
                string s = Galil_Interface.Transeive_Galil(command, device);
                Terminal_Tbox.Invoke((MethodInvoker)delegate { Terminal_Tbox.Text = s; });
            }
        }
        #endregion
        private void Clear_Button_Click(object sender, EventArgs e)
        {
            RKnee_Tbox.Invoke((MethodInvoker)delegate { RKnee_Tbox.Text = ""; });
            RHCenter_Tbox.Invoke((MethodInvoker)delegate { RHCenter_Tbox.Text = ""; });
            RHFront_Tbox.Invoke((MethodInvoker)delegate { RHFront_Tbox.Text = ""; });
            RHRear_Tbox.Invoke((MethodInvoker)delegate { RHRear_Tbox.Text = ""; });
            RAnkle_Tbox.Invoke((MethodInvoker)delegate { RAnkle_Tbox.Text = ""; });

            LKnee_Tbox.Invoke((MethodInvoker)delegate { LKnee_Tbox.Text = ""; });
            LHCenter_Tbox.Invoke((MethodInvoker)delegate { LHCenter_Tbox.Text = ""; });
            LHFront_Tbox.Invoke((MethodInvoker)delegate { LHFront_Tbox.Text = ""; });
            LHRear_Tbox.Invoke((MethodInvoker)delegate { LHRear_Tbox.Text = ""; });
            LAnkle_Tbox.Invoke((MethodInvoker)delegate { LAnkle_Tbox.Text = ""; });

            Terminal_Tbox.Invoke((MethodInvoker)delegate { Terminal_Tbox.Text = ""; });

            ST_LHF_Tbox.Invoke((MethodInvoker)delegate { ST_LHF_Tbox.Text = ""; });
            ST_LHC_Tbox.Invoke((MethodInvoker)delegate { ST_LHC_Tbox.Text = ""; });
            ST_LHR_Tbox.Invoke((MethodInvoker)delegate { ST_LHR_Tbox.Text = ""; });
            ST_LK_Tbox.Invoke((MethodInvoker)delegate { ST_LK_Tbox.Text = ""; });
            ST_LA_Tbox.Invoke((MethodInvoker)delegate { ST_LA_Tbox.Text = ""; });
            ST_RHF_Tbox.Invoke((MethodInvoker)delegate { ST_RHF_Tbox.Text = ""; });
            ST_RHC_Tbox.Invoke((MethodInvoker)delegate { ST_RHC_Tbox.Text = ""; });
            ST_RHR_Tbox.Invoke((MethodInvoker)delegate { ST_RHR_Tbox.Text = ""; });
            ST_RK_Tbox.Invoke((MethodInvoker)delegate { ST_RK_Tbox.Text = ""; });
            ST_RA_Tbox.Invoke((MethodInvoker)delegate { ST_RA_Tbox.Text = ""; });


        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Galil_Interface.Disconnect_All();
        }


        #region Right Knee
        private void RKnee_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Right_Knee);
            RKnee_Tbox.Invoke((MethodInvoker)delegate { RKnee_Tbox.Text = s; });
        }

        private void RKnee_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Right_Knee.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Right_Knee.Main_Controller_ID);
            RKnee_Tbox.Invoke((MethodInvoker)delegate { RKnee_Tbox.Text = s; });
        }

        private void RKnee_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Knee, Galil_Programs.Exo_Right_Knee.Nuetral_Angle);
            RKnee_Tbox.Invoke((MethodInvoker)delegate { RKnee_Tbox.Text = s; });
        }

        private void RKnee_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Knee, (int)RKnee_Goto_Numeric.Value);
            RKnee_Tbox.Invoke((MethodInvoker)delegate { RKnee_Tbox.Text = s; });
        }
        #endregion
        #region Left Knee
        private void LKnee_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Left_Knee);
            LKnee_Tbox.Invoke((MethodInvoker)delegate { LKnee_Tbox.Text = s; });
        }

        private void LKnee_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Knee, Galil_Programs.Exo_Left_Knee.Nuetral_Angle);
            LKnee_Tbox.Invoke((MethodInvoker)delegate { LKnee_Tbox.Text = s; });
        }

        private void LKnee_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Left_Knee.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Left_Knee.Main_Controller_ID);
            LKnee_Tbox.Invoke((MethodInvoker)delegate { LKnee_Tbox.Text = s; });
        }

        private void LKnee_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Knee, (int)LKnee_Goto_Numeric.Value);
            LKnee_Tbox.Invoke((MethodInvoker)delegate { LKnee_Tbox.Text = s; });
        }

        #endregion
        #region Right Hip Rear
        private void RHRear_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Right_Hip_Rear);
            RHRear_Tbox.Invoke((MethodInvoker)delegate { RHRear_Tbox.Text = s; });
        }

        private void RHRear_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Rear, Galil_Programs.Exo_Right_Hip_Rear.Nuetral_Angle);
            RHRear_Tbox.Invoke((MethodInvoker)delegate { RHRear_Tbox.Text = s; });
        }

        private void RHRear_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Right_Hip_Rear.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Right_Hip_Rear.Main_Controller_ID);
            RHRear_Tbox.Invoke((MethodInvoker)delegate { RHRear_Tbox.Text = s; });
        }

        private void RHRear_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Rear, (int)RHRear_Goto_Numeric.Value);
            RHRear_Tbox.Invoke((MethodInvoker)delegate { RHRear_Tbox.Text = s; });
        }
        #endregion
        #region Left Hip Rear
        private void LHRear_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Left_Hip_Rear);
            LHRear_Tbox.Invoke((MethodInvoker)delegate { LHRear_Tbox.Text = s; });
        }

        private void LHRear_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Rear, Galil_Programs.Exo_Left_Hip_Rear.Nuetral_Angle);
            LHRear_Tbox.Invoke((MethodInvoker)delegate { LHRear_Tbox.Text = s; });
        }

        private void LHRear_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Left_Hip_Rear.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Left_Hip_Rear.Main_Controller_ID);
            LHRear_Tbox.Invoke((MethodInvoker)delegate { LHRear_Tbox.Text = s; });
        }

        private void LHRear_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Rear, (int)LHRear_Goto_Numeric.Value);
            LHRear_Tbox.Invoke((MethodInvoker)delegate { LHRear_Tbox.Text = s; });
        }
        #endregion
        #region Right Hip Center
        private void RHCenter_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Right_Hip_Center);
            RHCenter_Tbox.Invoke((MethodInvoker)delegate { RHCenter_Tbox.Text = s; });
        }

        private void RHCenter_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Center, Galil_Programs.Exo_Right_Hip_Center.Nuetral_Angle);
            RHCenter_Tbox.Invoke((MethodInvoker)delegate { RHCenter_Tbox.Text = s; });
        }

        private void RHCenter_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Right_Hip_Center.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Right_Hip_Center.Main_Controller_ID);
            RHCenter_Tbox.Invoke((MethodInvoker)delegate { RHCenter_Tbox.Text = s; });
        }

        private void RHCenter_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Center, (int)RHCenter_Goto_Numeric.Value);
            RHCenter_Tbox.Invoke((MethodInvoker)delegate { RHCenter_Tbox.Text = s; });
        }
        #endregion
        #region Left Hip Center
        private void LHCenter_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Left_Hip_Center);
            LHCenter_Tbox.Invoke((MethodInvoker)delegate { LHCenter_Tbox.Text = s; });
        }

        private void LHCenter_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Center, Galil_Programs.Exo_Left_Hip_Center.Nuetral_Angle);
            LHCenter_Tbox.Invoke((MethodInvoker)delegate { LHCenter_Tbox.Text = s; });
        }

        private void LHCenter_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Left_Hip_Center.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Left_Hip_Center.Main_Controller_ID);
            LHCenter_Tbox.Invoke((MethodInvoker)delegate { LHCenter_Tbox.Text = s; });
        }

        private void LHCenter_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Center, (int)LHCenter_Goto_Numeric.Value);
            LHCenter_Tbox.Invoke((MethodInvoker)delegate { LHCenter_Tbox.Text = s; });
        }
        #endregion
        #region Right Hip front
        private void RHFront_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Right_Hip_Front);
            RHFront_Tbox.Invoke((MethodInvoker)delegate { RHFront_Tbox.Text = s; });
        }

        private void RHFront_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Front, Galil_Programs.Exo_Right_Hip_Front.Nuetral_Angle);
            RHFront_Tbox.Invoke((MethodInvoker)delegate { RHFront_Tbox.Text = s; });
        }

        private void RHFront_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Right_Hip_Front.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Right_Hip_Front.Main_Controller_ID);
            RHFront_Tbox.Invoke((MethodInvoker)delegate { RHFront_Tbox.Text = s; });
        }

        private void RHFront_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Front, (int)RHFront_Goto_Numeric.Value);
            RHFront_Tbox.Invoke((MethodInvoker)delegate { RHFront_Tbox.Text = s; });
        }
        #endregion
        #region Left Hip Front
        private void LHFront_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Left_Hip_Front);
            LHFront_Tbox.Invoke((MethodInvoker)delegate { LHFront_Tbox.Text = s; });
        }

        private void LHFront_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Front, Galil_Programs.Exo_Left_Hip_Front.Nuetral_Angle);
            LHFront_Tbox.Invoke((MethodInvoker)delegate { LHFront_Tbox.Text = s; });
        }

        private void LHFront_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Left_Hip_Front.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Left_Hip_Front.Main_Controller_ID);
            LHFront_Tbox.Invoke((MethodInvoker)delegate { LHFront_Tbox.Text = s; });
        }

        private void LHFront_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Front, (int)LHFront_Goto_Numeric.Value);
            LHFront_Tbox.Invoke((MethodInvoker)delegate { LHFront_Tbox.Text = s; });
        }
        #endregion
        #region Right Ankle
        private void RAnkle_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Right_Ankle);
            RAnkle_Tbox.Invoke((MethodInvoker)delegate { RAnkle_Tbox.Text = s; });
        }

        private void RAnkle_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Ankle, Galil_Programs.Exo_Right_Ankle.Nuetral_Angle);
            RAnkle_Tbox.Invoke((MethodInvoker)delegate { RAnkle_Tbox.Text = s; });
        }

        private void RAnkle_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Right_Ankle.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Right_Ankle.Main_Controller_ID);
            RAnkle_Tbox.Invoke((MethodInvoker)delegate { RAnkle_Tbox.Text = s; });
        }

        private void RAnkle_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Ankle, (int)RAnkle_Goto_Numeric.Value);
            RAnkle_Tbox.Invoke((MethodInvoker)delegate { RAnkle_Tbox.Text = s; });
        }
        #endregion
        #region Left_Ankle
        private void LAnkle_Init_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Init_Motor(Galil_Programs.Exo_Left_Ankle);
            LAnkle_Tbox.Invoke((MethodInvoker)delegate { LAnkle_Tbox.Text = s; });
        }

        private void LAnkle_Neutral_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Ankle, Galil_Programs.Exo_Left_Ankle.Nuetral_Angle);
            LAnkle_Tbox.Invoke((MethodInvoker)delegate { LAnkle_Tbox.Text = s; });
        }

        private void LAnkle_Stop_Button_Click(object sender, EventArgs e)
        {
            string command = "ST" + Galil_Programs.Exo_Left_Ankle.Main_Axis_ID + " \r\n";
            string s = Galil_Interface.Transeive_Galil(command, Galil_Programs.Exo_Left_Ankle.Main_Controller_ID);
            LAnkle_Tbox.Invoke((MethodInvoker)delegate { LAnkle_Tbox.Text = s; });
        }

        private void LAnkle_Goto_Button_Click(object sender, EventArgs e)
        {
            string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Ankle, (int)LAnkle_Goto_Numeric.Value);
            LAnkle_Tbox.Invoke((MethodInvoker)delegate { LAnkle_Tbox.Text = s; });
        }
        #endregion
        private void Refresh_Parms()
        {
            Galil_Programs.Refresh_Angles();
            RK_A_Tbox.Invoke((MethodInvoker)delegate { RK_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Right_Knee.ABS_Encoder, Galil_Programs.Exo_Right_Knee.ABS_Encoder_Offset); });
            RHC_A_Tbox.Invoke((MethodInvoker)delegate { RHC_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Right_Hip_Center.ABS_Encoder, Galil_Programs.Exo_Right_Hip_Center.ABS_Encoder_Offset); });
            RHF_A_Tbox.Invoke((MethodInvoker)delegate { RHF_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Right_Hip_Front.ABS_Encoder, Galil_Programs.Exo_Right_Hip_Front.ABS_Encoder_Offset); });
            RHR_A_Tbox.Invoke((MethodInvoker)delegate { RHR_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Right_Hip_Rear.ABS_Encoder, Galil_Programs.Exo_Right_Hip_Rear.ABS_Encoder_Offset); });
            RA_A_Tbox.Invoke((MethodInvoker)delegate { RA_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Right_Ankle.ABS_Encoder, Galil_Programs.Exo_Right_Ankle.ABS_Encoder_Offset); });

            LK_A_Tbox.Invoke((MethodInvoker)delegate { LK_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Left_Knee.ABS_Encoder, Galil_Programs.Exo_Left_Knee.ABS_Encoder_Offset); });
            LHC_A_Tbox.Invoke((MethodInvoker)delegate { LHC_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Left_Hip_Center.ABS_Encoder, Galil_Programs.Exo_Left_Hip_Center.ABS_Encoder_Offset); });
            LHF_A_Tbox.Invoke((MethodInvoker)delegate { LHF_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Left_Hip_Front.ABS_Encoder, Galil_Programs.Exo_Left_Hip_Front.ABS_Encoder_Offset); });
            LHR_A_Tbox.Invoke((MethodInvoker)delegate { LHR_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Left_Hip_Rear.ABS_Encoder, Galil_Programs.Exo_Left_Hip_Rear.ABS_Encoder_Offset); });
            LA_A_Tbox.Invoke((MethodInvoker)delegate { LA_A_Tbox.Text = Galil_Programs.Convert_Angle(Galil_Programs.Exo_Left_Ankle.ABS_Encoder, Galil_Programs.Exo_Left_Ankle.ABS_Encoder_Offset); });
            Galil_Programs.Refresh_Torques();
            RK_T_Tbox.Invoke((MethodInvoker)delegate { RK_T_Tbox.Text = Galil_Programs.Exo_Right_Knee.Torque_Value.ToString(); });
            RHC_T_Tbox.Invoke((MethodInvoker)delegate { RHC_T_Tbox.Text = Galil_Programs.Exo_Right_Hip_Center.Torque_Value.ToString(); });
            RHF_T_Tbox.Invoke((MethodInvoker)delegate { RHF_T_Tbox.Text = Galil_Programs.Exo_Right_Hip_Front.Torque_Value.ToString(); });
            RHR_T_Tbox.Invoke((MethodInvoker)delegate { RHR_T_Tbox.Text = Galil_Programs.Exo_Right_Hip_Rear.Torque_Value.ToString(); });
            RA_T_Tbox.Invoke((MethodInvoker)delegate { RA_T_Tbox.Text = Galil_Programs.Exo_Right_Ankle.Torque_Value.ToString(); });

            LK_T_Tbox.Invoke((MethodInvoker)delegate { LK_T_Tbox.Text = Galil_Programs.Exo_Left_Knee.Torque_Value.ToString(); });
            LHC_T_Tbox.Invoke((MethodInvoker)delegate { LHC_T_Tbox.Text = Galil_Programs.Exo_Left_Hip_Center.Torque_Value.ToString(); });
            LHF_T_Tbox.Invoke((MethodInvoker)delegate { LHF_T_Tbox.Text = Galil_Programs.Exo_Left_Hip_Front.Torque_Value.ToString(); });
            LHR_T_Tbox.Invoke((MethodInvoker)delegate { LHR_T_Tbox.Text = Galil_Programs.Exo_Left_Hip_Rear.Torque_Value.ToString(); });
            LA_T_Tbox.Invoke((MethodInvoker)delegate { LA_T_Tbox.Text = Galil_Programs.Exo_Left_Ankle.Torque_Value.ToString(); });
            // check errors
            //send TA0 command
            string command = "TA0 \r\n";
            string ta0_resp_4axis = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            string ta0_resp_8axis = Galil_Interface.Transeive_Galil(command, "8axis");
            //send TA1 command
            command = "TA1 \r\n";
            string ta1_resp_4axis = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            string ta1_resp_8axis = Galil_Interface.Transeive_Galil(command, "8axis");
            //send TA2 command
            command = "TA2 \r\n";
            string ta2_resp_4axis = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            string ta2_resp_8axis = Galil_Interface.Transeive_Galil(command, "8axis");
            //send TA3 command
            command = "TA3 \r\n";
            string ta3_resp_4axis = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            string ta3_resp_8axis = Galil_Interface.Transeive_Galil(command, "8axis");
            // check validity of responses
            int val = Galil_Programs.PVT_response_reader(ta3_resp_8axis);
            Update_Checkbox(val, Error_ELO_Checkbox_8axis);
            val = Galil_Programs.PVT_response_reader(ta3_resp_4axis);
            Update_Checkbox(val, Error_ELO_Checkbox_4axis);
            val = Galil_Programs.PVT_response_reader(ta2_resp_8axis);
            Update_Checkbox(val, Error_Peakcurrent_Checkbox_8axis);
            val = Galil_Programs.PVT_response_reader(ta2_resp_4axis);
            Update_Checkbox(val, Error_Peakcurrent_Checkbox_4axis);
            val = Galil_Programs.PVT_response_reader(ta1_resp_8axis);
            Update_Checkbox(val, Error_Hall_Checkbox_8axis);
            val = Galil_Programs.PVT_response_reader(ta1_resp_4axis);
            Update_Checkbox(val, Error_Hall_Checkbox_4axis);
            val = Galil_Programs.PVT_response_reader(ta0_resp_8axis);
            Update_Checkbox((val & 1), Error_OvercurrentAD_Checkbox_8axis);
            Update_Checkbox((val & 2), Error_OvervoltageAD_Checkbox_8axis);
            Update_Checkbox((val & 4), Error_OvertemperetureAD_Checkbox_8axis);
            Update_Checkbox((val & 8), Error_UndervoltageAD_Checkbox_8axis);
            Update_Checkbox((val & 16), Error_OvercurrentEH_Checkbox_8axis);
            Update_Checkbox((val & 32), Error_OvervoltageEH_Checkbox_8axis);
            Update_Checkbox((val & 64), Error_OvertemperetureEH_Checkbox_8axis);
            Update_Checkbox((val & 128), Error_UndervoltageEH_Checkbox_8axis);
            val = Galil_Programs.PVT_response_reader(ta0_resp_4axis);
            Update_Checkbox((val & 1), Error_OvercurrentAD_Checkbox_4axis);
            Update_Checkbox((val & 2), Error_OvervoltageAD_Checkbox_4axis);
            Update_Checkbox((val & 4), Error_OvertemperetureAD_Checkbox_4axis);
            Update_Checkbox((val & 8), Error_UndervoltageAD_Checkbox_4axis);
        }
        private void Update_Checkbox(int val, CheckBox inp)
        {
            if (val == 0)
            {
                inp.Invoke((MethodInvoker)delegate { inp.Checked = false; });
            }
            else
            {
                inp.Invoke((MethodInvoker)delegate { inp.Checked = true; });
            }

        }
        private void Refresh_Button_Click(object sender, EventArgs e)
        {
            Refresh_Parms();
        }

        private void Reset_All_Button_Click(object sender, EventArgs e)
        {
            // send reset command to 8 axis and 4 axis motor
            string command = "RS \r\n";
            string s = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            if ((s != ":") && (s != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in RS 4axis Motor");
            }
            s = Galil_Interface.Transeive_Galil(command, "8axis");
            if ((s != ":") && (s != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in RS 8axis");
            }

        }

        private void Stop_ALL_Button_Click(object sender, EventArgs e)
        {
            string command = "ST \r\n";
            string s = Galil_Interface.Transeive_Galil(command, "8axis");
            if ((s != ":") && (s != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in ST 8axis");
            }
            s = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            if ((s != ":") && (s != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in ST 4axis Motor");
            }
        }
        #region PVT 
        private int Record_Len = 600;
        private void PVT_Run_Button_Click(object sender, EventArgs e)
        {
            PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = false; });
            // record enable part
            string s;
            if (Record_Enable_Checkbox.Checked)
            {
                if ((!Record_Angle_Radiobutton.Checked) && (!Record_Torque_Radiobutton.Checked) && (!Record_Velocity_Radiobutton.Checked))
                {
                    MessageBox.Show("Please select Angle, Torque or Velociy");
                    PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });
                    return;
                }
                // check if record is not under protect in any devices (sending MG _RC command)
                int record_mode = -1;
                if (Record_Angle_Radiobutton.Checked)
                {
                    record_mode = 1;
                }
                else if (Record_Torque_Radiobutton.Checked)
                {
                    record_mode = 2;
                }
                else if (Record_Velocity_Radiobutton.Checked)
                {
                    record_mode = 3;
                }
                s = Galil_Programs.Check_Record_Status(record_mode);
                if (s != "Succeed")
                {
                    MessageBox.Show(s);
                    PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });
                    return;
                }
                // send record related commands to devices
                s = Galil_Programs.Record_Init(record_mode, 1000);
                if (s != "Succeed")
                {
                    MessageBox.Show(s);
                    PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });
                    return;
                }

                //     Galil_Capture.Clear();

                // send record start commands to the devices
                s = Galil_Programs.Record_Start(record_mode);
                if (s != "Succeed")
                {
                    MessageBox.Show(s);
                    PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });
                    return;
                }
                show_progress_moment = Galil_Programs.Get_Msecond();
                show_progress_flag = true;
                int temp = 0;
                Motion_Progress_Bar.Invoke((MethodInvoker)delegate { Motion_Progress_Bar.Value = temp; });
                Motion_Progress_Label.Invoke((MethodInvoker)delegate { Motion_Progress_Label.Text = temp.ToString() + " %"; });

            }


            // send BT Command to 8 Axis and 4 Axis
            string command = "BTABCDEFGH \r\n";
            //    string command = "BTAE \r\n";
            s = Galil_Interface.Transeive_Galil(command, "8axis");
            if ((s != ":") && (s != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in ST 8axis");
            }

            command = "BTBC \r\n";
            s = Galil_Interface.Transeive_Galil(command, "4axis Motor");
            if ((s != ":") && (s != "\r\n:"))
            {
                MessageBox.Show("fail response from galil Controller in ST 4axis Motor");
            }

            // start capturing
            Capture_start_Momoent_Milisecond = Galil_Programs.Get_Msecond();
            Capture_Enable_flag = true;

        }

        private void PVT_Download_Button_Click(object sender, EventArgs e)
        {
            //check openning file of 4 axis
            int cntr_8 = 0;
            int cntr_2 = 0;
            string[] lines_4axis;
            try
            {
                lines_4axis = File.ReadAllLines(PVT_Filepath_Tbox_4axis.Text);
            }
            catch
            {
                MessageBox.Show("problem in reading 4 axis pvt file, pleas browse it and download again");
                return;
            }
            string[] lines_8axis;
            try
            {
                lines_8axis = File.ReadAllLines(PVT_Filepath_Tbox_8axis.Text);
            }
            catch
            {
                MessageBox.Show("problem in reading 8 axis pvt file, pleas browse it and download again");
                return;
            }

            // send commands to galil
            for (int i = 0; i < lines_4axis.Length; i++)
            {
                string pvt = lines_4axis[i];
                string tt = pvt.Substring(0, 2);
                if (tt == "PV")
                {
                    pvt += "\r\n";
                    string s = Galil_Interface.Transeive_Galil(pvt, "4axis Motor");
                    if ((s != ":") && (s != "\r\n:") && (s != "::"))
                    {
                        MessageBox.Show("fail response from galil Controller in " + lines_4axis[i] + " 4axis");
                        return;
                    }
                    cntr_2++;
                }
            }

            for (int i = 0; i < lines_8axis.Length; i++)
            {
                string pvt = lines_8axis[i];
                pvt += "\r\n";
                string tt = pvt.Substring(0, 2);
                if (tt == "PV")
                {

                    string s = Galil_Interface.Transeive_Galil(pvt, "8axis");
                    if ((s != ":") && (s != "\r\n:") && (s != "::"))
                    {
                        MessageBox.Show("fail response from galil Controller in " + lines_8axis[i] + " 8axis");
                        return;
                    }
                    cntr_8++;
                }
            }

            MessageBox.Show("Download Succeed, " + cntr_8.ToString() + " pvt commands in 8 axis and " + cntr_2.ToString() + " in 4 axis");
            PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });

        }

        private void PVT_Browse_Button_4axis_Click(object sender, EventArgs e)
        {
            OpenFileDialog fpath = new OpenFileDialog();
            fpath.Filter = "DMC files (PVT04_Profile*.DMC)|PVT04_Profile*.DMC|All files (*.*)|*.*";
            if (fpath.ShowDialog() == DialogResult.OK)
            {
                PVT_Filepath_Tbox_4axis.Invoke((MethodInvoker)delegate { PVT_Filepath_Tbox_4axis.Text = fpath.FileName; });
            }
        }

        private void PVT_Browse_Button_8axis_Click(object sender, EventArgs e)
        {
            OpenFileDialog fpath = new OpenFileDialog();
            fpath.Filter = "DMC files (PVT04_Profile*.DMC)|PVT08_Profile*.DMC|All files (*.*)|*.*";
            if (fpath.ShowDialog() == DialogResult.OK)
            {
                PVT_Filepath_Tbox_8axis.Invoke((MethodInvoker)delegate { PVT_Filepath_Tbox_8axis.Text = fpath.FileName; });
            }

        }
        private void PVT_Browse_Button_Start_Position_Click(object sender, EventArgs e)
        {
            OpenFileDialog fpath = new OpenFileDialog();
            fpath.Filter = "Txt files (PVT_start_positions*.txt)|PVT_start_positions*.txt|All files (*.*)|*.*";
            if (fpath.ShowDialog() == DialogResult.OK)
            {
                PVT_Filepath_Tbox_Startposition.Invoke((MethodInvoker)delegate { PVT_Filepath_Tbox_Startposition.Text = fpath.FileName; });
            }
        }

        private void ST_Go_Button_Click(object sender, EventArgs e)
        {
            string st_s = Galil_Programs.Read_PVT_Startposition_file(PVT_Filepath_Tbox_Startposition.Text);
            if (st_s == "Succeed")
            {
                string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Front, Galil_Programs.Exo_Left_Hip_Front.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHF_Tbox.Invoke((MethodInvoker)delegate { ST_LHF_Tbox.Text = Galil_Programs.Exo_Left_Hip_Front.PVT_Start_Angle.ToString(); });
                }
                // left hip center
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Center, Galil_Programs.Exo_Left_Hip_Center.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHC_Tbox.Invoke((MethodInvoker)delegate { ST_LHC_Tbox.Text = Galil_Programs.Exo_Left_Hip_Center.PVT_Start_Angle.ToString(); });
                }
                // left hip rear
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Rear, Galil_Programs.Exo_Left_Hip_Rear.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHR_Tbox.Invoke((MethodInvoker)delegate { ST_LHR_Tbox.Text = Galil_Programs.Exo_Left_Hip_Rear.PVT_Start_Angle.ToString(); });
                }
                // left knee
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Knee, Galil_Programs.Exo_Left_Knee.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LK_Tbox.Invoke((MethodInvoker)delegate { ST_LK_Tbox.Text = Galil_Programs.Exo_Left_Knee.PVT_Start_Angle.ToString(); });
                }
                // left Ankle
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Ankle, Galil_Programs.Exo_Left_Ankle.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LA_Tbox.Invoke((MethodInvoker)delegate { ST_LA_Tbox.Text = Galil_Programs.Exo_Left_Ankle.PVT_Start_Angle.ToString(); });
                }
                //////////////////////
                // right hip front
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Front, Galil_Programs.Exo_Right_Hip_Front.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHF_Tbox.Invoke((MethodInvoker)delegate { ST_RHF_Tbox.Text = Galil_Programs.Exo_Right_Hip_Front.PVT_Start_Angle.ToString(); });
                }
                // right hip center
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Center, Galil_Programs.Exo_Right_Hip_Center.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHC_Tbox.Invoke((MethodInvoker)delegate { ST_RHC_Tbox.Text = Galil_Programs.Exo_Right_Hip_Center.PVT_Start_Angle.ToString(); });
                }
                // right hip rear
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Rear, Galil_Programs.Exo_Right_Hip_Rear.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHR_Tbox.Invoke((MethodInvoker)delegate { ST_RHR_Tbox.Text = Galil_Programs.Exo_Right_Hip_Rear.PVT_Start_Angle.ToString(); });
                }
                // right knee
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Knee, Galil_Programs.Exo_Right_Knee.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RK_Tbox.Invoke((MethodInvoker)delegate { ST_RK_Tbox.Text = Galil_Programs.Exo_Right_Knee.PVT_Start_Angle.ToString(); });
                }
                // right Ankle
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Ankle, Galil_Programs.Exo_Right_Ankle.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RA_Tbox.Invoke((MethodInvoker)delegate { ST_RA_Tbox.Text = Galil_Programs.Exo_Right_Ankle.PVT_Start_Angle.ToString(); });
                }
            }
            else
            {
                MessageBox.Show(st_s);
            }
        }

        private void ST_Check_Button_Click(object sender, EventArgs e)
        {
            Refresh_Parms();
            bool check_result;

            //left hip front
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Left_Hip_Front);
            Tick_LHF_Pbox.Invoke((MethodInvoker)delegate { Tick_LHF_Pbox.Visible = check_result; });
            Warning_LHF_Pbox.Invoke((MethodInvoker)delegate { Warning_LHF_Pbox.Visible = !check_result; });

            //left hip center
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Left_Hip_Center);
            Tick_LHC_Pbox.Invoke((MethodInvoker)delegate { Tick_LHC_Pbox.Visible = check_result; });
            Warning_LHC_Pbox.Invoke((MethodInvoker)delegate { Warning_LHC_Pbox.Visible = !check_result; });

            //left hip rear
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Left_Hip_Rear);
            Tick_LHR_Pbox.Invoke((MethodInvoker)delegate { Tick_LHR_Pbox.Visible = check_result; });
            Warning_LHR_Pbox.Invoke((MethodInvoker)delegate { Warning_LHR_Pbox.Visible = !check_result; });

            //left knee
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Left_Knee);
            Tick_LK_Pbox.Invoke((MethodInvoker)delegate { Tick_LK_Pbox.Visible = check_result; });
            Warning_LK_Pbox.Invoke((MethodInvoker)delegate { Warning_LK_Pbox.Visible = !check_result; });

            //left ankle
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Left_Ankle);
            Tick_LA_Pbox.Invoke((MethodInvoker)delegate { Tick_LA_Pbox.Visible = check_result; });
            Warning_LA_Pbox.Invoke((MethodInvoker)delegate { Warning_LA_Pbox.Visible = !check_result; });

            //right hip front
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Right_Hip_Front);
            Tick_RHF_Pbox.Invoke((MethodInvoker)delegate { Tick_RHF_Pbox.Visible = check_result; });
            Warning_RHF_Pbox.Invoke((MethodInvoker)delegate { Warning_RHF_Pbox.Visible = !check_result; });

            //right hip center
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Right_Hip_Center);
            Tick_RHC_Pbox.Invoke((MethodInvoker)delegate { Tick_RHC_Pbox.Visible = check_result; });
            Warning_RHC_Pbox.Invoke((MethodInvoker)delegate { Warning_RHC_Pbox.Visible = !check_result; });

            //right hip rear
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Right_Hip_Rear);
            Tick_RHR_Pbox.Invoke((MethodInvoker)delegate { Tick_RHR_Pbox.Visible = check_result; });
            Warning_RHR_Pbox.Invoke((MethodInvoker)delegate { Warning_RHR_Pbox.Visible = !check_result; });

            //right knee
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Right_Knee);
            Tick_RK_Pbox.Invoke((MethodInvoker)delegate { Tick_RK_Pbox.Visible = check_result; });
            Warning_RK_Pbox.Invoke((MethodInvoker)delegate { Warning_RK_Pbox.Visible = !check_result; });

            //right ankle
            check_result = Galil_Programs.Check_Start_Position_Error(Galil_Programs.Exo_Right_Ankle);
            Tick_RA_Pbox.Invoke((MethodInvoker)delegate { Tick_RA_Pbox.Visible = check_result; });
            Warning_RA_Pbox.Invoke((MethodInvoker)delegate { Warning_RA_Pbox.Visible = !check_result; });

        }

        private void PVT_Save_Button_Click(object sender, EventArgs e)
        {
            string s;
            int record_mode = -1;
            // clear last capture
            Galil_Capture.Clear();

            // check if record is finished
            if (Record_Angle_Radiobutton.Checked)
                record_mode = 1;
            else if (Record_Torque_Radiobutton.Checked)
                record_mode = 2;
            else if (Record_Velocity_Radiobutton.Checked)
                record_mode = 3;
            else
            {
                MessageBox.Show("please select record mode");
                return;
            }
            s = Galil_Programs.Check_Record_Status(record_mode);
            if (s != "Succeed")
            {
                MessageBox.Show(s);
                return;
            }

            // read recorded data from Galil
            for (int i = 0; i < Record_Len; i++)
            {
                // put readed class to captured list.
                Exo_Logs temp = Galil_Programs.Read_Recorded_Data(record_mode, i);
                if (temp == null)
                {
                    MessageBox.Show("Problem in reading recorded data, record again");
                    return;
                }
                Galil_Capture.Add(temp);
                if ((i % 10) == 0)
                {
                    int tempp = i / 10;
                    Motion_Progress_Bar.Invoke((MethodInvoker)delegate { Motion_Progress_Bar.Value = tempp; });
                    Motion_Progress_Label.Invoke((MethodInvoker)delegate { Motion_Progress_Label.Text = tempp.ToString() + " %"; });
                }
            }
            // write capturd data to the file
            using (var file = File.CreateText("mylog.csv"))
            {
                file.WriteLine(Galil_Programs.Make_CaptureCSV_Title());
                foreach (var arr in Galil_Capture)
                {
                    file.WriteLine(Galil_Programs.Make_CaptureCSV_Line2(arr));
                }
            }
            //terminate record buffer to make sure there is not any duplicated read
            s = Galil_Programs.Record_Terminate(record_mode);
            if (s != "Succeed")
            {
                MessageBox.Show(s);
                return;
            }
            Motion_Progress_Bar.Invoke((MethodInvoker)delegate { Motion_Progress_Bar.Value = 100; });
            Motion_Progress_Label.Invoke((MethodInvoker)delegate { Motion_Progress_Label.Text = "100 %"; });

            MessageBox.Show("write to file Succeed");
        }
        private void Refresh_PVT_Tboxes()
        {
            // pvt part
            //remain
            PVT_Space_Tbox_LHF.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_LHF.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Left_Hip_Front).ToString(); });
            PVT_Space_Tbox_LHC.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_LHC.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Left_Hip_Center).ToString(); });
            PVT_Space_Tbox_LHR.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_LHR.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Left_Hip_Rear).ToString(); });
            PVT_Space_Tbox_LK.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_LK.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Left_Knee).ToString(); });
            PVT_Space_Tbox_LA.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_LA.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Left_Ankle).ToString(); });

            PVT_Space_Tbox_RHF.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_RHF.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Right_Hip_Front).ToString(); });
            PVT_Space_Tbox_RHC.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_RHC.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Right_Hip_Center).ToString(); });
            PVT_Space_Tbox_RHR.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_RHR.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Right_Hip_Rear).ToString(); });
            PVT_Space_Tbox_RK.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_RK.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Right_Knee).ToString(); });
            PVT_Space_Tbox_RA.Invoke((MethodInvoker)delegate { PVT_Space_Tbox_RA.Text = Galil_Programs.Read_PVT_Remain(Galil_Programs.Exo_Right_Ankle).ToString(); });

            //execuate
            PVT_Executed_Tbox_LHF.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_LHF.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Left_Hip_Front).ToString(); });
            PVT_Executed_Tbox_LHC.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_LHC.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Left_Hip_Center).ToString(); });
            PVT_Executed_Tbox_LHR.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_LHR.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Left_Hip_Rear).ToString(); });
            PVT_Executed_Tbox_LK.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_LK.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Left_Knee).ToString(); });
            PVT_Executed_Tbox_LA.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_LA.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Left_Ankle).ToString(); });

            PVT_Executed_Tbox_RHF.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_RHF.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Right_Hip_Front).ToString(); });
            PVT_Executed_Tbox_RHC.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_RHC.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Right_Hip_Center).ToString(); });
            PVT_Executed_Tbox_RHR.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_RHR.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Right_Hip_Rear).ToString(); });
            PVT_Executed_Tbox_RK.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_RK.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Right_Knee).ToString(); });
            PVT_Executed_Tbox_RA.Invoke((MethodInvoker)delegate { PVT_Executed_Tbox_RA.Text = Galil_Programs.Read_PVT_Execuate(Galil_Programs.Exo_Right_Ankle).ToString(); });

        }

        private void PVT_Refresh_Button_Click(object sender, EventArgs e)
        {
            Refresh_PVT_Tboxes();
        }

        #endregion

        private void STB_Button_Click(object sender, EventArgs e)
        {
            OpenFileDialog fpath = new OpenFileDialog();
            fpath.Filter = "Txt files (PVT_start_positions*.txt)|PVT_start_positions*.txt|All files (*.*)|*.*";
            if (fpath.ShowDialog() == DialogResult.OK)
            {
                STB_Tbox.Invoke((MethodInvoker)delegate { STB_Tbox.Text = fpath.FileName; });
            }

        }

        private void STE_Button_Click(object sender, EventArgs e)
        {
            OpenFileDialog fpath = new OpenFileDialog();
            fpath.Filter = "Txt files (PVT_start_positions*.txt)|PVT_start_positions*.txt|All files (*.*)|*.*";
            if (fpath.ShowDialog() == DialogResult.OK)
            {
                STE_Tbox.Invoke((MethodInvoker)delegate { STE_Tbox.Text = fpath.FileName; });
            }

        }

        private void Loop_Button_Click(object sender, EventArgs e)
        {

            string st_s = Galil_Programs.Read_PVT_Startposition_file(STB_Tbox.Text);
            if (st_s == "Succeed")
            {
                string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Front, Galil_Programs.Exo_Left_Hip_Front.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHF_Tbox.Invoke((MethodInvoker)delegate { ST_LHF_Tbox.Text = Galil_Programs.Exo_Left_Hip_Front.PVT_Start_Angle.ToString(); });
                }
                // left hip center
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Center, Galil_Programs.Exo_Left_Hip_Center.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHC_Tbox.Invoke((MethodInvoker)delegate { ST_LHC_Tbox.Text = Galil_Programs.Exo_Left_Hip_Center.PVT_Start_Angle.ToString(); });
                }
                // left hip rear
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Rear, Galil_Programs.Exo_Left_Hip_Rear.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHR_Tbox.Invoke((MethodInvoker)delegate { ST_LHR_Tbox.Text = Galil_Programs.Exo_Left_Hip_Rear.PVT_Start_Angle.ToString(); });
                }
                // left knee
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Knee, Galil_Programs.Exo_Left_Knee.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LK_Tbox.Invoke((MethodInvoker)delegate { ST_LK_Tbox.Text = Galil_Programs.Exo_Left_Knee.PVT_Start_Angle.ToString(); });
                }
                // left Ankle
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Ankle, Galil_Programs.Exo_Left_Ankle.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LA_Tbox.Invoke((MethodInvoker)delegate { ST_LA_Tbox.Text = Galil_Programs.Exo_Left_Ankle.PVT_Start_Angle.ToString(); });
                }
                //////////////////////
                // right hip front
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Front, Galil_Programs.Exo_Right_Hip_Front.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHF_Tbox.Invoke((MethodInvoker)delegate { ST_RHF_Tbox.Text = Galil_Programs.Exo_Right_Hip_Front.PVT_Start_Angle.ToString(); });
                }
                // right hip center
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Center, Galil_Programs.Exo_Right_Hip_Center.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHC_Tbox.Invoke((MethodInvoker)delegate { ST_RHC_Tbox.Text = Galil_Programs.Exo_Right_Hip_Center.PVT_Start_Angle.ToString(); });
                }
                // right hip rear
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Rear, Galil_Programs.Exo_Right_Hip_Rear.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHR_Tbox.Invoke((MethodInvoker)delegate { ST_RHR_Tbox.Text = Galil_Programs.Exo_Right_Hip_Rear.PVT_Start_Angle.ToString(); });
                }
                // right knee
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Knee, Galil_Programs.Exo_Right_Knee.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RK_Tbox.Invoke((MethodInvoker)delegate { ST_RK_Tbox.Text = Galil_Programs.Exo_Right_Knee.PVT_Start_Angle.ToString(); });
                }
                // right Ankle
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Ankle, Galil_Programs.Exo_Right_Ankle.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RA_Tbox.Invoke((MethodInvoker)delegate { ST_RA_Tbox.Text = Galil_Programs.Exo_Right_Ankle.PVT_Start_Angle.ToString(); });
                }
            }
            else
            {
                MessageBox.Show(st_s);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string st_s = Galil_Programs.Read_PVT_Startposition_file(STE_Tbox.Text);
            if (st_s == "Succeed")
            {
                string s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Front, Galil_Programs.Exo_Left_Hip_Front.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHF_Tbox.Invoke((MethodInvoker)delegate { ST_LHF_Tbox.Text = Galil_Programs.Exo_Left_Hip_Front.PVT_Start_Angle.ToString(); });
                }
                // left hip center
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Center, Galil_Programs.Exo_Left_Hip_Center.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHC_Tbox.Invoke((MethodInvoker)delegate { ST_LHC_Tbox.Text = Galil_Programs.Exo_Left_Hip_Center.PVT_Start_Angle.ToString(); });
                }
                // left hip rear
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Hip_Rear, Galil_Programs.Exo_Left_Hip_Rear.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LHR_Tbox.Invoke((MethodInvoker)delegate { ST_LHR_Tbox.Text = Galil_Programs.Exo_Left_Hip_Rear.PVT_Start_Angle.ToString(); });
                }
                // left knee
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Knee, Galil_Programs.Exo_Left_Knee.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LK_Tbox.Invoke((MethodInvoker)delegate { ST_LK_Tbox.Text = Galil_Programs.Exo_Left_Knee.PVT_Start_Angle.ToString(); });
                }
                // left Ankle
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Left_Ankle, Galil_Programs.Exo_Left_Ankle.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_LA_Tbox.Invoke((MethodInvoker)delegate { ST_LA_Tbox.Text = Galil_Programs.Exo_Left_Ankle.PVT_Start_Angle.ToString(); });
                }
                //////////////////////
                // right hip front
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Front, Galil_Programs.Exo_Right_Hip_Front.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHF_Tbox.Invoke((MethodInvoker)delegate { ST_RHF_Tbox.Text = Galil_Programs.Exo_Right_Hip_Front.PVT_Start_Angle.ToString(); });
                }
                // right hip center
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Center, Galil_Programs.Exo_Right_Hip_Center.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHC_Tbox.Invoke((MethodInvoker)delegate { ST_RHC_Tbox.Text = Galil_Programs.Exo_Right_Hip_Center.PVT_Start_Angle.ToString(); });
                }
                // right hip rear
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Hip_Rear, Galil_Programs.Exo_Right_Hip_Rear.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RHR_Tbox.Invoke((MethodInvoker)delegate { ST_RHR_Tbox.Text = Galil_Programs.Exo_Right_Hip_Rear.PVT_Start_Angle.ToString(); });
                }
                // right knee
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Knee, Galil_Programs.Exo_Right_Knee.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RK_Tbox.Invoke((MethodInvoker)delegate { ST_RK_Tbox.Text = Galil_Programs.Exo_Right_Knee.PVT_Start_Angle.ToString(); });
                }
                // right Ankle
                s = Galil_Programs.Motor_Goto_Angle(Galil_Programs.Exo_Right_Ankle, Galil_Programs.Exo_Right_Ankle.PVT_Start_Angle);
                if (s == "Succeed")
                {
                    ST_RA_Tbox.Invoke((MethodInvoker)delegate { ST_RA_Tbox.Text = Galil_Programs.Exo_Right_Ankle.PVT_Start_Angle.ToString(); });
                }
            }
            else
            {
                MessageBox.Show(st_s);
            }

        }

        private void Record_Enable_Checkbox_CheckedChanged(object sender, EventArgs e)
        {
            PVT_Record_Gbox.Invoke((MethodInvoker)delegate { PVT_Record_Gbox.Enabled = Record_Enable_Checkbox.Checked; });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s;
            // check if settings are correct
            if ((!Record_Angle_Radiobutton.Checked) && (!Record_Torque_Radiobutton.Checked) && (!Record_Velocity_Radiobutton.Checked))
            {
                MessageBox.Show("Please select Angle, Torque or Velociy");
                PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });
                return;
            }
            // check if record is not under protect in any devices (sending MG _RC command)
            int record_mode = -1;
            if (Record_Angle_Radiobutton.Checked)
            {
                record_mode = 1;
            }
            else if (Record_Torque_Radiobutton.Checked)
            {
                record_mode = 2;
            }
            else if (Record_Velocity_Radiobutton.Checked)
            {
                record_mode = 3;
            }
            s = Galil_Programs.Check_Record_Status(record_mode);
            if (s != "Succeed")
            {
                MessageBox.Show(s);
                PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });
                return;
            }
            // send record related commands to devices
            s = Galil_Programs.Record_Init(record_mode, 1000);
            if (s != "Succeed")
            {
                MessageBox.Show(s);
                PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });
                return;
            }

            //     Galil_Capture.Clear();

            // send record start commands to the devices
            s = Galil_Programs.Record_Start(record_mode);
            if (s != "Succeed")
            {
                MessageBox.Show(s);
                PVT_Run_Button.Invoke((MethodInvoker)delegate { PVT_Run_Button.Enabled = true; });
                return;
            }
            show_progress_moment = Galil_Programs.Get_Msecond();
            show_progress_flag = true;
            int temp = 0;
            Motion_Progress_Bar.Invoke((MethodInvoker)delegate { Motion_Progress_Bar.Value = temp; });
            Motion_Progress_Label.Invoke((MethodInvoker)delegate { Motion_Progress_Label.Text = temp.ToString() + " %"; });

          
        }
        bool show_progress_flag = false;
        int show_progress_moment;
       private void Reset_Progress()
        {
            show_progress_moment = Galil_Programs.Get_Msecond();
            show_progress_flag = true;
            int temp = 0;
            Motion_Progress_Bar.Invoke((MethodInvoker)delegate { Motion_Progress_Bar.Value = temp; });
            Motion_Progress_Label.Invoke((MethodInvoker)delegate { Motion_Progress_Label.Text = temp.ToString() + " %"; });
        }
    }
}
