﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Galil_Integration
{
    public class Log_Data
    {
        public double Angle { get; set; }
        public double Torque { get; set; }
        public double Velocity { get; set; }
    }
}
